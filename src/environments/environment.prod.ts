import { endPoints } from "./endpoint";

export const environment = {
  production: true,
  pexelRoot: 'https://api.pexels.com/videos/search',
  /* apiRoot: 'https://api.ideaboxventures.com', */
  apiRoot: 'https://api.ideaboxventures.com',
  ...endPoints
};