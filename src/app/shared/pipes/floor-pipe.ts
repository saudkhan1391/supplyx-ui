import { Pipe, PipeTransform } from '@angular/core';
import { Building, ProjectAddress } from '../models/projects/project.model';
import { ICountry, IState } from 'country-state-city/dist/lib/interface';

@Pipe({name: 'floorPipe'})
export class FloorPipe implements PipeTransform {
  transform(buildings: Building[]): number {
      if(buildings && buildings.length > 0) {
        return buildings.reduce( (a, b) => a + b.floors.length, 0)
      }
    return 0;
  }
}