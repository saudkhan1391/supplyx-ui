import { Pipe, PipeTransform } from '@angular/core';
import { Area } from '../models/projects/project.model';

@Pipe({name: 'apartmentTypeArea'})
export class ApartmentTypeAreaPipe implements PipeTransform {
  transform(value: Area[]): string {
      if(value) {
          return value.map( area => `${area.count} ${area.name}`).join(", ");
      }
    return value;
  }
}