import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'initialPipe'})
export class InitialPipe implements PipeTransform {
  transform(value: string): string {
      if(value) {
        const initials: string[] = value.split(' ');
        return initials[0].charAt(0) + (initials.length >= 2 ? initials[1].charAt(0) : '');
      }
    return value;
  }
}