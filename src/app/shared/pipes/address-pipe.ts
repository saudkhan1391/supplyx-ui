import { Pipe, PipeTransform } from '@angular/core';
import { ProjectAddress } from '../models/projects/project.model';
import { ICountry, IState } from 'country-state-city/dist/lib/interface';

@Pipe({name: 'addressPipe'})
export class AddressPipe implements PipeTransform {
  transform(address: ProjectAddress): string {
      if(address) {
        try {
          const state: IState = JSON.parse(address.state);
          const country: ICountry = JSON.parse(address.country);
          return `${address.street}, ${state.name}, ${country.name}, ${address.zipCode}`;
        } catch(err) {
          return address.street + ' ' + address.state + ', ' + address.country + ', ' + address.zipCode;
        }        
      }
    return address;
  }
}