import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InitialPipe } from './pipes/initial-pipe';
import { AddressPipe } from './pipes/address-pipe';
import { ApartmentTypeAreaPipe } from './pipes/apartment-type-area.pipe';
import { EllipsisPipe } from './pipes/ellipsis.pipe';
import { FloorPipe } from './pipes/floor-pipe';

export const COMP = [
  InitialPipe,
  AddressPipe,
  ApartmentTypeAreaPipe,
  EllipsisPipe,
  FloorPipe
]

@NgModule({
  declarations: [...COMP],
  imports: [
    CommonModule
  ],
  exports: [...COMP]
})
export class SharedModule { }
