export interface Scene {
    videoUrl: string,
    text: string
}

export interface Template {
    name: string,
    imageUrl: string,
    animation: string
}