export interface PexelVideoResponse{
    next_page: string,
    page: number,
    per_page: number,
    total_results: number,
    videos: PexelVideo[]
}

export interface PexelVideo{
    id: number,
    width: number,
    height: number,
    duration: number,
    full_res: string,
    tags: string[],
    url: string,
    image: string,
    avg_color: string,
    //user
    video_files: VideoFile[],
    video_pictures: VideoPicture[]
}

export interface VideoFile{
    id: number,
    quality: string,
    file_type: string,
    width: number,
    height: number,
    link: string
}

export interface VideoPicture{
    id: number,
    nr: number,
    picture: string
}