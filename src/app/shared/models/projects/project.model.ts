import { ProjectAttachment, ProjectImage, ProjectVideo } from './../product/image.model';

export interface Project {
    id: number;
    type: string;
    name: string;
    country: string;
    state: string;
    city: string;
    overview: string;
    description: string;
    image: ProjectImage;
    buildings: Building[];
    apartmentTypes: Apartment[];
    address: ProjectAddress;
    status: string;
    takeOffStatus: string;
    specifications: ProjectAttachment[];
    takeOff: TakeOff[];
    createdAt?:string;
    updatedAt?:string;
}

export interface Building {
    id: number;
    project: Project;
    name: string;
    floorCount: number;
    floors: Floor[];
    apartment: Apartment[];
}

export interface Floor {
    id: number;
    name: string;
    floorNumber: number;
    building: Building;
    apartments: Apartment[]
    apartmentTypes: Apartment[];
}

export interface ApartmentInFloor {
    id: number;
    floor: Floor;
    apartmentType: Apartment;
}

export interface Apartment {
    id: number;
    project: Project;
    name: string;
    size: number;
    areas: Area[];
    projectVideo?: ProjectVideo[];
    youtubeLink?: string;
}

export interface ProjectAddress {
    id: number;
    street: string;
    city: string;
    state: string;
    zipCode: string;
    country: string;
}

export interface Area {
    id: number;
    apartmentType: Apartment;
    name: string;
    count: number;
}

export interface TakeOff {
    id: number;
    name: string;
    status: string;
    key: string;
    url: string;
    project: Project;
    projectId: number;
}
