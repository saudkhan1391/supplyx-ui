export interface Catalog {
    id: string,
    userId: number,
    catalogName: string,
    description: string,
    productCount: number
}