
export interface CatalogProductMapping {
    id: number;
    catalogId: number;
    productId: number;
}