export interface ApiResponse<T> {
    data: T,
    message: string
}

export interface ApiPaginationResponse<T> {
    data: T[],
    total: number
}

export interface PaginationOptions {
    page: number,
    take: number,
    status?: string,
    filter?: string,
    filterBy?: string
}