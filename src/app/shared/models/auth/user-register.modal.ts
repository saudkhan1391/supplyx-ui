export interface UserRegister {
    firstName: string,
    lastName: string,
    email: string,
    password: string,
    retypePassword: string,
    companyName: string,
    roleType: string
}