import { UserAttachment } from "../product/image.model"


export interface User {
    companyName: string,
    country: string,
    createdAt: string,
    email: string,
    firstName: string
    id: number,
    lastName: string,
    mobile: string,
    phone: string,
    phoneCode: string,
    roleType: string,
    state: string,
    street: string,
    token: string,
    images: UserAttachment[],
    zipCode: number,
    password:string
}