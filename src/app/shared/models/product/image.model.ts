import { Apartment, Project } from "../projects/project.model";

export interface ProductImage extends UploadedFile { }

export interface ProductSpec extends UploadedFile { }

export interface CompanyImage {
    id: number;
    companyId: number;
    key: string;
    url: string;
}

export interface UploadedFile {
    id: number;
    productId: number;
    key: string;
    url: string;
}

export interface ProjectImage {
    id: number;
    key: string;
    url: string;
}

export interface ProjectAttachment{
    id: number;
    key: string;
    url: string;
    projectId: number;
    status: string;
}

export interface UserAttachment{
    id: number;
    key: string;
    url: string;
    userId: number;
}

/* export interface TakeOffAttachment{
    id: number;
    key: string;
    url: string;
    projectId: number;
} */

export interface ProjectVideo {
    id: number;
    apartmentId: number;
    apartment: Apartment[];
    key: string;
    url: string;
}



