import { Catalog } from "../catalog/catalog.model";
import { Category } from "../category/category.model";
import { ProductImage, ProductSpec } from "./image.model";

export interface Product {
 
    filter(arg0: (item: any) => boolean): unknown;
    productName: string;
    brand: string;
    price: string;
    manufacturer: string;
    partNo: string;
    availableQuantity: number;
    minimumQuantity: number;
    hsCode: string;
    description: string;
    shipinfo: string;
    catalog: Catalog;
    category: Category;
    specifications: ProductSpec[];
    images: ProductImage[];
    id: number;
    status: string;
}