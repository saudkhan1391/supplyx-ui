
import { CompanyImage } from "../product/image.model";

export interface Company{
    id: number;
    sellerId: number,
    userId: number,
    name: string,
    type: string,
    mobile: number,
    email: string,
    phone: number,
    street: string,
    postalCode: string,
    website: string,
    youtubeChannel: string,
    whyChooseUs: string,
    images: CompanyImage[],
    bannerImage: CompanyImage[],
    aboutImage: CompanyImage[],
    chooseusImage: CompanyImage[],
    about: string  
}