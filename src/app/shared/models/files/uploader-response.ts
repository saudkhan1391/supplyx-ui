export interface UploaderResponse {
    key: string,
    path: string
}