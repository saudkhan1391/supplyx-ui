export interface StripeCustomer {
  email: string;
  source: number;
}
