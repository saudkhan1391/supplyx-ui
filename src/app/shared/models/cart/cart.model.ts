import { Product } from "../product/product.model";

export interface Cart {
  id: number;
  userId?: number;
  quantity: number;
  productName: string;
  products?: Product[];
}
