import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { 
  getDocument,
  GlobalWorkerOptions,
  PDFDocumentProxy,
  //PDFRenderParams,
  version,
  //ViewportParameters 
} from 'pdfjs-dist';

@Injectable({
  providedIn: 'root'
})
export class PdfThumbnailService {
  private document: Document;

  constructor(@Inject(DOCUMENT) document: any) {
    this.document = document;
    const pdfWorkerSrc = `https://cdnjs.cloudflare.com/ajax/libs/pdf.js/${version}/pdf.worker.min.js`;
    GlobalWorkerOptions.workerSrc = pdfWorkerSrc;
  }

  // My use case demonstrating strongly typed usage.
  public async pdfToImageDataURLAsync(url: string): Promise<string> {
    //const arrayBuffer = await new Response(url).arrayBuffer();
    const pdfPath: URL = new URL(url);
    const canvas = this.document.createElement('canvas'),
      ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
    const pdf: PDFDocumentProxy = await getDocument(url).promise;
    const page = await pdf.getPage(1);

    const viewPortParams = { scale: 2 };
    const viewport = page.getViewport(viewPortParams);

    canvas.height = viewport.height;
    canvas.width = viewport.width;

    const renderContext = {
      canvasContext: ctx,
      viewport: viewport
    };

    const renderedPage = await page.render(renderContext).promise;
    const res = canvas.toDataURL();
    if (pdf != null) pdf.destroy();
    return res;
  }

}
