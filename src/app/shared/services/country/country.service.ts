import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Country } from '../../models/country/country.model';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private _http: HttpClient) { }

  getCountries(): Observable<Country> {
    return this._http.get<Country>('https://restcountries.com/v3.1/all')
  }
}
