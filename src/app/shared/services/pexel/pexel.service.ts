import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { PexelVideoQuery } from 'src/app/state/apis/pexels/pexel.query';
import { PexelVideoStore } from 'src/app/state/apis/pexels/pexel.store';
import { environment } from 'src/environments/environment';
import { PexelVideo, PexelVideoResponse } from '../../models/pexels/pexel-response-model';

@Injectable({
  providedIn: 'root'
})
export class PexelsService {

  constructor(
    private _store: PexelVideoStore,
    private _query: PexelVideoQuery,
    private _http: HttpClient
  ) { }

  getVideo(): Observable<PexelVideo[]> {
    return this._query.videos$.pipe(
      filter( response => (response && response.videos.length > 0)),
      map( (response: PexelVideoResponse ) => response.videos)
    );
  }

  updateStore( response: PexelVideoResponse) {
    this._store.update( state => ({
      ...state,
      videos: response
    }))
  }

  getVideosForContext(context: string): Observable<PexelVideoResponse>{
    return this._http.get<PexelVideoResponse>(`${environment.pexelRoot}?query=${context}&per_page=40&orientation=landscape`, {
      headers: {
        Authorization: "563492ad6f9170000100000147c6a2ede4db4657b98e96527c215edf"
      }
    });
  }

}