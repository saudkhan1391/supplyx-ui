import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PaginationResponse } from '@datorama/akita';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ApiPaginationResponse, ApiResponse, PaginationOptions } from '../../models/auth/api-response.model';
import { CatalogProductMapping } from '../../models/catalog/catalog-product-mapping.model';
import { Catalog } from '../../models/catalog/catalog.model';
import { Product } from '../../models/product/product.model';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {

  constructor(private _http: HttpClient) { }
  
  addCatalog(catlog: Catalog): Observable<ApiResponse<Catalog>> {
    let t = localStorage.getItem("token");
    return this._http.post<ApiResponse<Catalog>>(`${environment.apiRoot}/${environment.catalog}`, 
    catlog, { headers: { "Authorization": "Bearer " + t} })
  }

  findCatalogbyId(catalogId: number): Observable<ApiResponse<Catalog>> {
    let t = localStorage.getItem("token");
    return this._http.get<ApiResponse<Catalog>>(`${environment.apiRoot}/${environment.catalogById.replace(':id', catalogId.toString())}`, {
      headers: { "Authorization": "Bearer " + t}
    })
  }

  fetchAllCatalog( paginationOptions: PaginationOptions): Observable<PaginationResponse<Catalog>> {
    let t = localStorage.getItem("token");
    return this._http.post<ApiResponse<ApiPaginationResponse<Catalog>>>(`${environment.apiRoot}/${environment.getAllCatalogs}`, {
      page: paginationOptions.page - 1,
      take: paginationOptions.take
    }, { headers: { "Authorization": "Bearer " + t} }).pipe(
      map( ( res: ApiResponse<ApiPaginationResponse<Catalog>>) => {
        return {
          currentPage: paginationOptions.page,
          perPage: paginationOptions.take,
          total: res.data.total,
          lastPage: Math.ceil(res.data.total/paginationOptions.take),
          data: res.data.data
        }
      }) // map closed here
    )
  }

  fetchProductsByCatalogId(catalogId: number, paginationOptions: PaginationOptions): Observable<PaginationResponse<Product>>{
    let t = localStorage.getItem("token");
    return this._http.post<ApiResponse<ApiPaginationResponse<Product>>>(
      `${environment.apiRoot}/${environment.getProductByCatalogId.replace(':id', catalogId.toString())}`, 
      {
        page: paginationOptions.page - 1,
        take: paginationOptions.take,
        status: paginationOptions.status
      },
      { headers: { "Authorization": "Bearer " + t} }
    ).pipe(
      map((res: ApiResponse<ApiPaginationResponse<Product>>) => {
        return {
          currentPage: paginationOptions.page,
          perPage:  paginationOptions.take,
          total: res.data.total,
          lastPage: Math.ceil(res.data.total/paginationOptions.take),
          data: res.data.data
        }})
    )
  }

  addToCatalog( catalogId: string, productIds: number[]): Observable<ApiResponse<CatalogProductMapping>> {
    let t = localStorage.getItem("token");
    return this._http.post<ApiResponse<CatalogProductMapping>>(`${environment.apiRoot}/${environment.addToCatalog}`, {
      id: catalogId,
      productIds: productIds
    }, { headers: { "Authorization": "Bearer " + t} })
  }
}
