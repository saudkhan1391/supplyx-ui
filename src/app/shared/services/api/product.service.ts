
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../../models/product/product.model';
import { ApiPaginationResponse, ApiResponse, PaginationOptions } from '../../models/auth/api-response.model';
import { PaginationResponse } from '@datorama/akita';
import { map } from 'rxjs/operators';
import { Cart } from '../../models/cart/cart.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private _http:HttpClient) { }

  addProduct(product: Product): Observable<ApiResponse<Product>> {
    let t = localStorage.getItem("token");
      return this._http.post<ApiResponse<Product>>(`${environment.apiRoot}/${environment.addProduct}`,
      product, { headers: { "Authorization": "Bearer " + t} })
  }

  addToCart(product: Cart): Observable<ApiResponse<Cart>> {
    let t = localStorage.getItem("token");
      return this._http.post<ApiResponse<Cart>>(`${environment.apiRoot}/${environment.addToCart}`,
      product, { headers: { "Authorization": "Bearer " + t} })
  }

  // getAllCartItems(): Observable<ApiResponse<Cart[]>> {
  //   let t = localStorage.getItem("token");
  //   return this._http.get<ApiResponse<Cart[]>>(`${environment.apiRoot}/${environment.getAllCartItems}`,
  //     { headers: { "Authorization": "Bearer " + t } })
  // }

  getAllCartItems(paginationOptions: PaginationOptions): Observable<PaginationResponse<Cart>>{
    let t = localStorage.getItem("token");
    return this._http.post<ApiResponse<ApiPaginationResponse<Cart>>>(`${environment.apiRoot}/${environment.getAllCartItems}`,
    {
      ...paginationOptions,
      page: paginationOptions.page - 1
    },
     { headers: { "Authorization": "Bearer " + t} }).pipe(
       map((res: ApiResponse<ApiPaginationResponse<Cart>>) => {
        return {
          currentPage: paginationOptions.page,
          perPage:  paginationOptions.take,
          total: res.data.total,
          lastPage: Math.ceil(res.data.total/paginationOptions.take),
          data: res.data.data
        }})
     )
  }
  deleteProductById(id: number): Observable<ApiResponse<Product[]>>{
    let t = localStorage.getItem("token");
      return this._http.delete<ApiResponse<Product[]>>(`${environment.apiRoot}/${environment.cart}/${id}`,
     { headers: { "Authorization": "Bearer " + t} })
  }

  getAllProductsData(): Observable<ApiResponse<Product[]>> {
    let t = localStorage.getItem("token");
    return this._http.get<ApiResponse<Product[]>>(`${environment.apiRoot}/${environment.getAllProductsData}`,
      { headers: { "Authorization": "Bearer " + t } })
  }
  getAllProductsByKey(value:string): Observable<ApiResponse<Product[]>> {
    let t = localStorage.getItem("token");
    return this._http.get<ApiResponse<Product[]>>(`${environment.apiRoot}/${environment.getAllProductsByKey}/${value}`,
      { headers: { "Authorization": "Bearer " + t } })
  }

  addDraftProduct(product: Product): Observable<ApiResponse<Product>> {
    let t = localStorage.getItem("token");
      return this._http.post<ApiResponse<Product>>(`${environment.apiRoot}/${environment.addDraftProduct}`,
      product, { headers: { "Authorization": "Bearer " + t} })
  }

  getProduct(): Observable<ApiResponse<Product[]>>{
    let t = localStorage.getItem("token");
      return this._http.get<ApiResponse<Product[]>>(`${environment.apiRoot}/${environment.fetchProduct}`,
     { headers: { "Authorization": "Bearer " + t} })
  }
  editCartItems(cart: Cart[]): Observable<ApiResponse<Product>> {
    // delete product.;
    // delete product.;
    let t = localStorage.getItem("token");
    return this._http.put<ApiResponse<Product>>(`${environment.apiRoot}/${environment.updateCart}`,
      cart, { headers: { "Authorization": "Bearer " + t} })
  }

  updateProductByStatus(productId: number, status: string ): Observable<ApiResponse<Product>> {
    let t = localStorage.getItem("token");
      return this._http.post<ApiResponse<Product>>(`${environment.apiRoot}/${environment.updateProductByStatus.replace(':id', productId.toString())}`,
      { status: status },
     { headers: { "Authorization": "Bearer " + t} })
  }

  deleteProduct(productId: number ): Observable<ApiResponse<Product>> {
    let t = localStorage.getItem("token");
      return this._http.delete<ApiResponse<Product>>(`${environment.apiRoot}/${environment.updateProductByStatus.replace(':id', productId.toString())}`,
     { headers: { "Authorization": "Bearer " + t} })
  }

  findProductById( productId: number) : Observable<ApiResponse<Product>> {
    let t = localStorage.getItem("token");
      return this._http.get<ApiResponse<Product>>(`${environment.apiRoot}/${environment.getProductById.replace(':id', productId.toString())}`,
     { headers: { "Authorization": "Bearer " + t} })
  }

  fetchProducts(paginationOptions: PaginationOptions): Observable<PaginationResponse<Product>>{
    let t = localStorage.getItem("token");
    return this._http.post<ApiResponse<ApiPaginationResponse<Product>>>(`${environment.apiRoot}/${environment.fetchProduct}`,
    {
      ...paginationOptions,
      page: paginationOptions.page - 1
    },
     { headers: { "Authorization": "Bearer " + t} }).pipe(
       map((res: ApiResponse<ApiPaginationResponse<Product>>) => {
        return {
          currentPage: paginationOptions.page,
          perPage:  paginationOptions.take,
          total: res.data.total,
          lastPage: Math.ceil(res.data.total/paginationOptions.take),
          data: res.data.data
        }})
     )
  }

}
