import { ApiResponse, PaginationOptions } from 'src/app/shared/models/auth/api-response.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Apartment, Building, Project, TakeOff } from '../../models/projects/project.model';
import { ApiPaginationResponse } from '../../models/auth/api-response.model';
import { environment } from 'src/environments/environment';
import { PaginationResponse } from '@datorama/akita';
import { map } from 'rxjs/operators';
import { ProjectAttachment, ProjectImage, ProjectVideo } from '../../models/product/image.model';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  refreshProject$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  refreshProjectsList$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  constructor(private _http: HttpClient) { }

  addProject(project: Project): Observable<ApiResponse<Project>> {
    let t = localStorage.getItem("token");
    return this._http.post<ApiResponse<Project>>(`${environment.apiRoot}/${environment.addProject}`, 
    project, { headers: { "Authorization": "Bearer " + t} })
  }

  editProject(project: Project, id: number): Observable<ApiResponse<Project>> {
    delete project.createdAt;
    delete project.updatedAt;
    let t = localStorage.getItem("token");
    return this._http.post<ApiResponse<Project>>(`${environment.apiRoot}${environment.editProject.replace(':id', id.toString())}`, 
    project, { headers: { "Authorization": "Bearer " + t} })
  }

  updateProjectTakeOffStatus(id: number, status: string,userId:number ): Observable<ApiResponse<Project>> {
    let t = localStorage.getItem("token");
      return this._http.post<ApiResponse<Project>>(`${environment.apiRoot}/${environment.updateprojectstatus.replace(':id', id.toString())}`, 
      { status: status,userId },
     { headers: { "Authorization": "Bearer " + t} })
  }


  // editProject(id: number): Observable<ApiResponse<Project>> {
  //   let t = localStorage.getItem("token");
  //   return this._http.get<ApiResponse<Project>>(`${environment.apiRoot}/${environment.findProjectById.replace(':id', id.toString())}`, { 
  //     headers: { "Authorization": "Bearer " + t} 
  //   });
  // }

  fetchProjects(paginationOptions: PaginationOptions): Observable<PaginationResponse<Project>>{
    let t = localStorage.getItem("token");
    return this._http.post<ApiResponse<ApiPaginationResponse<Project>>>(`${environment.apiRoot}/${environment.fetchProjects}`, 
    {
      ...paginationOptions,
      page: paginationOptions.page - 1
    },
     { headers: { "Authorization": "Bearer " + t} }).pipe(
       map((res: ApiResponse<ApiPaginationResponse<Project>>) => {
        return {
          currentPage: paginationOptions.page,
          perPage:  paginationOptions.take,
          total: res.data.total,
          lastPage: Math.ceil(res.data.total/paginationOptions.take),
          data: res.data.data
        }})
     )
  }

  getProjectById(id: number): Observable<ApiResponse<Project>> {
    let t = localStorage.getItem("token");
    return this._http.get<ApiResponse<Project>>(`${environment.apiRoot}/${environment.findProjectById.replace(':id', id.toString())}`, { 
      headers: { "Authorization": "Bearer " + t} 
    });
  }

  addBuildingToProject(building: Building): Observable<ApiResponse<Building>>{
    let t = localStorage.getItem("token");
    return this._http.post<ApiResponse<Building>>(`${environment.apiRoot}/${environment.addBuilding}`, building, { 
      headers: { "Authorization": "Bearer " + t} 
    });
  }

  addApartmentToProject(apartment: Apartment): Observable<ApiResponse<Apartment>> {
    let t = localStorage.getItem("token");
    return this._http.post<ApiResponse<Apartment>>(`${environment.apiRoot}/${environment.addApartment}`, apartment, { 
      headers: { "Authorization": "Bearer " + t} 
    });
  }

  deleteApartmentInProject(id: Number): Observable<ApiResponse<Number>> {
    let t = localStorage.getItem("token");
    return this._http.delete<ApiResponse<Number>>(`${environment.apiRoot}${environment.deleteApartment}/${id}`, { 
      headers: { "Authorization": "Bearer " + t} 
    });
  }

  addTakeOffToProject(takeoff: TakeOff): Observable<ApiResponse<TakeOff>>{
    let t = localStorage.getItem("token");
    return this._http.post<ApiResponse<TakeOff>>(`${environment.apiRoot}/${environment.takeoff}`,  takeoff, { 
      headers: { "Authorization": "Bearer " + t} 
    });
  }

/*   getTakeOffProject(): Observable<ApiResponse<Project>>{
    let t = localStorage.getItem("token");
    return this._http.get<ApiResponse<Project>>(`${environment.apiRoot}/${environment.takeoffProject}`, { 
      headers: { "Authorization": "Bearer " + t} 
    });
  } */

 /*  addVideoToProject(video: string): Observable<ApiResponse<string>>{
    let t = localStorage.getItem("token");
    return this._http.get<ApiResponse<Project>>(`${environment.apiRoot}/${environment.takeoffProject}`, { 
      headers: { "Authorization": "Bearer " + t} 
    });
  }  */

  addVideoToProject(video: ProjectVideo): Observable<ApiResponse<ProjectVideo>>{
    let t = localStorage.getItem("token");
    return this._http.post<ApiResponse<ProjectVideo>>(`${environment.apiRoot}/${environment.video}`,  video, { 
      headers: { "Authorization": "Bearer " + t} 
    });
  }

  addImageToProject(image: ProjectImage): Observable<ApiResponse<ProjectImage>>{
    let t = localStorage.getItem("token");
    return this._http.post<ApiResponse<ProjectImage>>(`${environment.apiRoot}/${environment.image}`, image, { 
      headers: { "Authorization": "Bearer " + t} 
    });
  }

  addTakeOffToInitiater(takeoff: ProjectAttachment): Observable<ApiResponse<ProjectAttachment>>{
    let t = localStorage.getItem("token");
    return this._http.post<ApiResponse<ProjectAttachment>>(`${environment.apiRoot}/${environment.takeoffinitiated}`,  takeoff, { 
      headers: { "Authorization": "Bearer " + t} 
    });
  }

  fetchTakeOffProjects(paginationOptions: PaginationOptions): Observable<PaginationResponse<Project>>{
    let t = localStorage.getItem("token");
    return this._http.post<ApiResponse<ApiPaginationResponse<Project>>>(`${environment.apiRoot}/${environment.takeoffProject}`, 
    {
      ...paginationOptions,
      page: paginationOptions.page - 1
    },
     { headers: { "Authorization": "Bearer " + t} }).pipe(
       map((res: ApiResponse<ApiPaginationResponse<Project>>) => {
        return {
          currentPage: paginationOptions.page,
          perPage:  paginationOptions.take,
          total: res.data.total,
          lastPage: Math.ceil(res.data.total/paginationOptions.take),
          data: res.data.data
        }})
     )
  }
}
