import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiResponse } from '../../models/auth/api-response.model';
import { UploaderResponse } from '../../models/files/uploader-response';

@Injectable({
  providedIn: 'root'
})
export class FileUploaderService {

  constructor(private _http: HttpClient) { }

  uploadFile(filesFormData: FormData): Observable<ApiResponse<UploaderResponse[]>> {
    return this._http.post<ApiResponse<UploaderResponse[]>>(`${environment.apiRoot}/${environment.upload}`, filesFormData)
  }

  deleteFile(key: string): Observable<string> {
    return this._http.delete<string>(`${environment.apiRoot}/${environment.upload}/${key}`);
  }
}
