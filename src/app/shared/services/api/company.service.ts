
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Company } from '../../models/company/company.model';
import { ApiResponse } from '../../models/auth/api-response.model';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private _http: HttpClient) { }

    addCompany(company: Company): Observable<ApiResponse<Company>> {
      let t = localStorage.getItem("token");
      return this._http.post<ApiResponse<Company>>(`${environment.apiRoot}/${environment.company}`, 
      company, { headers: { "Authorization": "Bearer " + t} })
    }

    getCompany(): Observable<ApiResponse<Company>> {
      let t = localStorage.getItem("token");
      return this._http.get<ApiResponse<Company>>(`${environment.apiRoot}/${environment.company}`,
      { headers: { "Authorization": "Bearer " + t} })
    }

    editCompany(company: Company): Observable<ApiResponse<Company>> {
      let t = localStorage.getItem("token");
      return this._http.post<ApiResponse<Company>>(`${environment.apiRoot}/${environment.company}`,
       company, { headers: { "Authorization": "Bearer " + t} })
    }
    
}
