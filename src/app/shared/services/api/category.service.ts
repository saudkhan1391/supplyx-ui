import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiResponse } from '../../models/auth/api-response.model';
import { Category } from '../../models/category/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor( private _http: HttpClient) { }

  getCategories(): Observable<ApiResponse<Category[]>> {
    let t = localStorage.getItem("token");
    return this._http.get<ApiResponse<Category[]>>(`${environment.apiRoot}/${environment.getAllCategories}`,
      { headers: { "Authorization": "Bearer " + t} })
  }
}
