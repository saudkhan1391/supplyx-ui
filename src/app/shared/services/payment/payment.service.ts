import { Injectable } from '@angular/core';

import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { StripeCustomer } from '../../models/cart/stripe-customer';
import { ApiResponse } from '../../models/auth/api-response.model';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  constructor( private _http:HttpClient) { }

  // makePayment(stripeToken:StripeCustomer):Observable<StripeCustomer>{
  //   const url = "payment"
  //   return this.http.post<StripeCustomer>(url,{token:stripeToken})
  // }

  makePayment(stripeToken: StripeCustomer): Observable<ApiResponse<StripeCustomer>> {
    let t = localStorage.getItem("token");
    return this._http.post<ApiResponse<StripeCustomer>>(`${environment.apiRoot}/${environment.payment}`,
    stripeToken, { headers: { "Authorization": "Bearer " + t} })
  }
}
