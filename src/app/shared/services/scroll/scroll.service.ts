import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScrollService {

  constructor() { }

  scrollToElement(element: string) {
    const ele: Element | null = document.querySelector(element)
    if(ele) {
      ele.scrollIntoView({ behavior: 'smooth', block: 'start', inline: "nearest"})
    } else {
      console.log(`${element} not found to scroll`);
    }
  }
}
