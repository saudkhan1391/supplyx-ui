import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PreparingShipmentComponent } from './preparing-shipment.component';

const routes: Routes = [
  {path:'', component:PreparingShipmentComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PreparingShipmentRoutingModule { }
