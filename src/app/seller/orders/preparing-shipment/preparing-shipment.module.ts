import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PreparingShipmentRoutingModule } from './preparing-shipment-routing.module';
import { PreparingShipmentComponent } from './preparing-shipment.component';


@NgModule({
  declarations: [
    PreparingShipmentComponent
  ],
  imports: [
    CommonModule,
    PreparingShipmentRoutingModule
  ]
})
export class PreparingShipmentModule { }
