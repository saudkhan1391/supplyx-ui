import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparingShipmentComponent } from './preparing-shipment.component';

describe('PreparingShipmentComponent', () => {
  let component: PreparingShipmentComponent;
  let fixture: ComponentFixture<PreparingShipmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreparingShipmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreparingShipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
