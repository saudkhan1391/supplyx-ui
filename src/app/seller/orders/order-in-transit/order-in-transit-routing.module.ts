import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrderInTransitComponent } from './order-in-transit.component';

const routes: Routes = [
  { path:'', component:OrderInTransitComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderInTransitRoutingModule { }
