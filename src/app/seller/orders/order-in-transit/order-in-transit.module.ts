import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderInTransitRoutingModule } from './order-in-transit-routing.module';
import { OrderInTransitComponent } from './order-in-transit.component';


@NgModule({
  declarations: [
    OrderInTransitComponent
  ],
  imports: [
    CommonModule,
    OrderInTransitRoutingModule
  ]
})
export class OrderInTransitModule { }
