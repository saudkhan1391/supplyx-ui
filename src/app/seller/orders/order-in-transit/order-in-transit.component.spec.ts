import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderInTransitComponent } from './order-in-transit.component';

describe('OrderInTransitComponent', () => {
  let component: OrderInTransitComponent;
  let fixture: ComponentFixture<OrderInTransitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderInTransitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderInTransitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
