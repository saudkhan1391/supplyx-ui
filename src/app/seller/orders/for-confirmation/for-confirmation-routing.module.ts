import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForConfirmationComponent } from './for-confirmation.component';

const routes: Routes = [
  { path:'', component:ForConfirmationComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForConfirmationRoutingModule { }
