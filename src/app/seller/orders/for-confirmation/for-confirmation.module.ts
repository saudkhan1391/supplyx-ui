import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForConfirmationRoutingModule } from './for-confirmation-routing.module';
import { ForConfirmationComponent } from './for-confirmation.component';


@NgModule({
  declarations: [
    ForConfirmationComponent
  ],
  imports: [
    CommonModule,
    ForConfirmationRoutingModule
  ]
})
export class ForConfirmationModule { }
