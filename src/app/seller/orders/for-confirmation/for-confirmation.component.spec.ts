import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForConfirmationComponent } from './for-confirmation.component';

describe('ForConfirmationComponent', () => {
  let component: ForConfirmationComponent;
  let fixture: ComponentFixture<ForConfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForConfirmationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
