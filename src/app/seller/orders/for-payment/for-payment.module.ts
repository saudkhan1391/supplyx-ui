import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForPaymentRoutingModule } from './for-payment-routing.module';
import { ForPaymentComponent } from './for-payment.component';
// import { PaymentComponent } from './payment/payment.component';
import { ShippingInformationComponent } from './shipping-information/shipping-information.component';


@NgModule({
  declarations: [
    ForPaymentComponent,
    // PaymentComponent,
    // ShippingInformationComponent
  ],
  imports: [
    CommonModule,
    ForPaymentRoutingModule
  ]
})
export class ForPaymentModule { }
