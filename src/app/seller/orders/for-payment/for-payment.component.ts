import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-for-payment',
  templateUrl: './for-payment.component.html',
  styleUrls: ['./for-payment.component.scss']
})
export class ForPaymentComponent implements OnInit {

  constructor(private _activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
  }

}
