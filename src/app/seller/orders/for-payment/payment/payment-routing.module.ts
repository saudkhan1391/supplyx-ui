import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentComponent } from './payment.component';

const routes: Routes = [
  { path:'', component: PaymentComponent},
  { path: 'shipping-information', loadChildren: () => import('../shipping-information/shipping-information.module').then(m => m.ShippingInformationModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentRoutingModule { }
