import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { render } from 'creditcardpayments/creditCardPayments'
import { PaymentService } from 'src/app/shared/services/payment/payment.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

  constructor(private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private payment: PaymentService) {
    render(
      {
        id: "#myPaypalButtons",
        currency: "USD",
        value: "100.00",
        onApprove: (details) => {
          alert("Transaction Successfull")
        }
      }
    )
  }
  cart: any[] = [];
  paymentHandler: any = null;

  paymentRequest: google.payments.api.PaymentDataRequest = {
    apiVersion: 2,
    apiVersionMinor: 0,
    allowedPaymentMethods: [
      {
        type: 'CARD',
        parameters: {
          allowedAuthMethods: ['PAN_ONLY', 'CRYPTOGRAM_3DS'],
          allowedCardNetworks: ['MASTERCARD', 'VISA'],
        },
        tokenizationSpecification: {
          type: 'PAYMENT_GATEWAY',
          parameters: {
            gateway: 'example',
            gatewayMerchantId: 'exampleGatewayMerchantId',
          },
        },
      },
    ],
    merchantInfo: {
      merchantId: '17613812255336763067',
      merchantName: 'Demo Only (you will not be charged)',
    },
    transactionInfo: {
      totalPriceStatus: 'FINAL',
      totalPriceLabel: 'Total',
      totalPrice: this.cartTotal.toFixed(2),
      currencyCode: 'USD',
      countryCode: 'US',
    },
  };

  get cartSize() {
    return this.cart.reduce((total, item) => total + item.quantity, 0);
  }

  get cartTotal() {
    return this.cart.reduce((total, item) => total + item.quantity * item.item.price, 0);
  }
  async onLoadPaymentData(event: Event) {
    const paymentData = (event as CustomEvent<google.payments.api.PaymentData>).detail;
  }
  userprofileForm = new FormGroup({
    cardNumber: new FormControl(''),
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    expireDate: new FormControl(''),
    CVV: new FormControl(''),
    Street: new FormControl(''),
    stateName: new FormControl(''),
    countryName: new FormControl(''),
    zipCOde: new FormControl(''),
  })
  onSubmit() {
    console.log(this.userprofileForm.value)
  }

  ngOnInit(): void {
    this.invokeStripe();
  }
  makePayment(amount: number) {
    const paymentHandler = (<any>window).StripeCheckout.configure({
      key: 'pk_test_51LT0HlKodSXkSr0FZqtWnThcJLSRopzhNRGZzhefm0RTHSAmmXWyUWPZ8ETQyKr7pVTNXHD3a235Cx0Qd7EM5eoZ000mXOkerp',
      locale: 'auto',
      token: function (stripeToken: any) {
        console.log(stripeToken);
        paymentStripe({ source: stripeToken.id });
        alert('Stripe Payment Done')
        console.log("this is console :", stripeToken)

      },
    });

    const paymentStripe = (stripeToken: any) => {
      this.payment.makePayment(stripeToken).subscribe((data: any) => {
        console.log(data);
      });
    };

    paymentHandler.open({
      name: 'SupplyX',
      description: 'Payment for product at SupplyX Marketplace.',
      amount: amount * 100,
    });
  }

  invokeStripe() {
    if (!window.document.getElementById('stripe-script')) {
      const script = window.document.createElement('script');
      script.id = 'stripe-script';
      script.type = 'text/javascript';
      script.src = 'https://checkout.stripe.com/checkout.js';
      script.onload = () => {
        this.paymentHandler = (<any>window).StripeCheckout.configure({
          key: 'pk_test_51LT0HlKodSXkSr0FZqtWnThcJLSRopzhNRGZzhefm0RTHSAmmXWyUWPZ8ETQyKr7pVTNXHD3a235Cx0Qd7EM5eoZ000mXOkerp',
          locale: 'auto',
          token: function (stripeToken: any) {
            console.log(stripeToken);
          },
        });
      };

      window.document.body.appendChild(script);
    }
  }

}
