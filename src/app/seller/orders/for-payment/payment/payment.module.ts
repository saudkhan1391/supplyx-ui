import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentRoutingModule } from './payment-routing.module';
import { PaymentComponent } from './payment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { GooglePayButtonModule } from '@google-pay/button-angular/button-angular/lib/google-pay-button.module';
import { GooglePayButtonModule } from "@google-pay/button-angular";
import { OrderSummaryMenuModule } from '../../order-summary-menu/order-summary-menu.module';

@NgModule({
  declarations: [
    PaymentComponent,
    
  ],
  imports: [
    CommonModule,
    PaymentRoutingModule,
    GooglePayButtonModule,
    FormsModule,
    ReactiveFormsModule,
    OrderSummaryMenuModule

  ]
  ,
  exports:[
    PaymentComponent
  ]
})
export class PaymentModule { }
