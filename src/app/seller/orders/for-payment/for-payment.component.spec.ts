import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForPaymentComponent } from './for-payment.component';

describe('ForPaymentComponent', () => {
  let component: ForPaymentComponent;
  let fixture: ComponentFixture<ForPaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForPaymentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
