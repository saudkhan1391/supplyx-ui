import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PaymentService } from 'src/app/shared/services/payment/payment.service';

@Component({
  selector: 'app-shipping-information',
  templateUrl: './shipping-information.component.html',
  styleUrls: ['./shipping-information.component.scss'],
})
export class ShippingInformationComponent implements OnInit {
  constructor(
    private _activatedRoute: ActivatedRoute,
    private payment: PaymentService
  ) { }
  ngOnInit() {
  }
  userprofileForm = new FormGroup({
    cardNumber: new FormControl(''),
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    expireDate: new FormControl(''),
    CVV: new FormControl(''),
    Street: new FormControl(''),
    stateName: new FormControl(''),
    countryName: new FormControl(''),
    zipCOde: new FormControl(''),

  })
  onSubmit() {
    console.log(this.userprofileForm.value)
  }
}
