import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForPaymentComponent } from './for-payment.component';

const routes: Routes = [
  { path:'', component: ForPaymentComponent},
  { path: 'payment', loadChildren: () => import('../for-payment/payment/payment.module').then(m => m.PaymentModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForPaymentRoutingModule { }
