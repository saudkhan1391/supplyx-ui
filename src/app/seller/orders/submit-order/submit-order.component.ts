import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from 'src/app/state/products/cart.service';

@Component({
  selector: 'app-submit-order',
  templateUrl: './submit-order.component.html',
  styleUrls: ['./submit-order.component.scss']
})
export class SubmitOrderComponent implements OnInit {
  cartData: any[] = [];

  constructor(private _router: Router,
    private _cartService: CartService) {
    this._cartService.getCartItems().subscribe(res => {
      console.log("cart items : ", res);
      this.cartData=res
      console.log("cartData  :", this.cartData);
    })
  }

  ngOnInit(): void {
  }

}
