import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubmitOrderRoutingModule } from './submit-order-routing.module';
import { SubmitOrderComponent } from './submit-order.component';


@NgModule({
  declarations: [
    SubmitOrderComponent
  ],
  imports: [
    CommonModule,
    SubmitOrderRoutingModule
  ]
})
export class SubmitOrderModule { }
