import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrdersComponent } from './orders.component';
import { OrdersModule } from './orders.module';

const routes: Routes = [
  {path: '', component: OrdersComponent},
  { path: 'for-payment', loadChildren: () => import('../orders/for-payment/for-payment.module').then(m => m.ForPaymentModule) },
  { path: 'for-confirmation', loadChildren: () => import('../orders/for-confirmation/for-confirmation.module').then(m => m.ForConfirmationModule) },
  { path: 'preparing-shipment', loadChildren: () => import('../orders/preparing-shipment/preparing-shipment.module'). then(m => m.PreparingShipmentModule) },
  { path: 'order-in-transit', loadChildren: () => import('../orders/order-in-transit/order-in-transit.module'). then(m => m.OrderInTransitModule) },
  { path: 'completed', loadChildren: () => import('../orders/completed/completed.module'). then(m => m.CompletedModule) },
  { path: 'order-summary-menu', loadChildren: () => import('../orders/order-summary-menu/order-summary-menu.module'). then(m => m.OrderSummaryMenuModule) },
  { path: 'submit-order', loadChildren: () => import('../orders/submit-order/submit-order.module')
  .then(m => m.SubmitOrderModule)},
  {path: 'thankyou-submitted', loadChildren: () => import('../orders/thankyou-submitted/thankyou-submitted.module')
  .then(m => m.ThankyouSubmittedModule)},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
