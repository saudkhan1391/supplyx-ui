import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThankyouSubmittedComponent } from './thankyou-submitted.component';

describe('ThankyouSubmittedComponent', () => {
  let component: ThankyouSubmittedComponent;
  let fixture: ComponentFixture<ThankyouSubmittedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThankyouSubmittedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThankyouSubmittedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
