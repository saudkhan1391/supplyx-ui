import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ThankyouSubmittedComponent } from './thankyou-submitted.component';

const routes: Routes = [
  {path: '', component:ThankyouSubmittedComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThankyouSubmittedRoutingModule { }
