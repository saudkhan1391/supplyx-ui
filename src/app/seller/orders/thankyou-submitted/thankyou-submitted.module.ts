import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThankyouSubmittedRoutingModule } from './thankyou-submitted-routing.module';
import { ThankyouSubmittedComponent } from './thankyou-submitted.component';


@NgModule({
  declarations: [
    ThankyouSubmittedComponent
  ],
  imports: [
    CommonModule,
    ThankyouSubmittedRoutingModule
  ]
})
export class ThankyouSubmittedModule { }
