import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-thankyou-submitted',
  templateUrl: './thankyou-submitted.component.html',
  styleUrls: ['./thankyou-submitted.component.scss']
})
export class ThankyouSubmittedComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
