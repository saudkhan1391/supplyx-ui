import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderSummaryMenuComponent } from './order-summary-menu.component';

describe('OrderSummaryMenuComponent', () => {
  let component: OrderSummaryMenuComponent;
  let fixture: ComponentFixture<OrderSummaryMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderSummaryMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderSummaryMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
