import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderSummaryMenuRoutingModule } from './order-summary-menu-routing.module';
import { OrderSummaryMenuComponent } from './order-summary-menu.component';


@NgModule({
  declarations: [
    OrderSummaryMenuComponent
  ],
  imports: [
    CommonModule,
    OrderSummaryMenuRoutingModule
  ],
  exports: [
    OrderSummaryMenuComponent
  ]
})
export class OrderSummaryMenuModule { }
