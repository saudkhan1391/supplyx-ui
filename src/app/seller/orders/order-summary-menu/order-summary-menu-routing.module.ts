import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrderSummaryMenuComponent } from './order-summary-menu.component';

const routes: Routes = [
  {path:'', component:OrderSummaryMenuComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderSummaryMenuRoutingModule { }
