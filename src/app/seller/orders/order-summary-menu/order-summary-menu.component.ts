import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-order-summary-menu',
  templateUrl: './order-summary-menu.component.html',
  styleUrls: ['./order-summary-menu.component.scss']
})
export class OrderSummaryMenuComponent implements OnInit {

  constructor(private _activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
  }

}
