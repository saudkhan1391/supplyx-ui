import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/models/product/product.model';
import { ProductService } from 'src/app/shared/services/api/product.service';

@Component({
  selector: 'app-seller-dashboard',
  templateUrl: './seller-dashboard.component.html',
  styleUrls: ['./seller-dashboard.component.scss']
})
export class SellerDashboardComponent implements OnInit {

  getAllProductsData: any;
  recentProducts: any;

  constructor(
    private _product: ProductService
  ) {
    _product.getAllProductsData().subscribe((data) => {
      this.getAllProductsData = data.data;
      this.getAllProductsData = this.getAllProductsData.filter((item: Product) => item.status == "active");
      this.recentProducts = this.getAllProductsData.slice(Math.max(this.getAllProductsData.length - 3, 0))
    })
  }


  ngOnInit(): void {
  }

}
