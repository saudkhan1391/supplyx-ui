import { SellerProfileComponent } from './seller-profile.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: SellerProfileComponent},
  { path: 'edit-seller-profile', loadChildren: () => import('../seller-profile/edit-seller-profile/edit-seller-profile.module')
  .then( m => m.EditSellerProfileModule)
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SellerProfileRoutingModule { }
