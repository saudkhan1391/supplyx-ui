    import { Observable, Subject, of, from } from 'rxjs';
import { User } from 'src/app/shared/models/auth/user.model';
import { tap, catchError, takeUntil, pluck, map, switchMap, toArray } from 'rxjs/operators';
import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/api/auth.service';
import { Country, State, City }  from 'country-state-city';
import { CountryModel } from 'src/app/shared/models/country/country.model';
import { ICity, ICountry, IState } from 'country-state-city/dist/lib/interface';
import { getStatesOfCountry } from 'country-state-city/dist/lib/state';
import { UploaderResponse } from 'src/app/shared/models/files/uploader-response';
import { FileUploaderComponent } from 'src/app/lib/file-uploader/file-uploader.component';
import { CompanyImage, } from 'src/app/shared/models/product/image.model';

@Component({
  selector: 'app-edit-seller-profile',
  templateUrl: './edit-seller-profile.component.html',
  styleUrls: ['./edit-seller-profile.component.scss']
})
export class EditSellerProfileComponent implements OnInit, OnDestroy {

  sellerForm!: FormGroup;
  user$! : Observable<User>;
  _user: User = {} as User;
  unSubscribe$: Subject<void> = new Subject<void>()
  countries: ICountry[] = [];
  selectedCountry!: ICountry;
  countryCode!: string;
  states: IState[] = [];
  pCode!: string;
  @ViewChild('imageUploader') imageUploader!: FileUploaderComponent;
  @Output() onDirty: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private fb: FormBuilder,
    private _auth: AuthService,) { }

  ngOnInit(): void {
    from(Country.getAllCountries()).pipe(
      map( res => this.countries.push(res))
    ).subscribe()

    this.createForm();
    this.country?.valueChanges.pipe(
      tap( res => {
        this.selectedCountry = res;
        this.countryCode = this.selectedCountry?.isoCode;
        this.pCode = this.selectedCountry?.phonecode;
        this.sellerForm.patchValue({
          phoneCode: this.selectedCountry?.phonecode
        })
        this.getStates(this.countryCode);
      })
    ).subscribe()

    this._user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')!) : null;
    this.sellerForm.patchValue(this._user);
  }

  getStates(code:string){
    if(this.states.length > 0){
      this.states.splice(0,this.states.length)
    }
      from(State.getStatesOfCountry(code)).pipe(
      map( res => { this.states.push(res)})
    ).subscribe()
  }

  createForm() {
    this.sellerForm = this.fb.group({
      
      firstName: [ this._user.firstName, Validators.required],
      lastName: [ this._user.lastName, Validators.required],
      mobile: [ this._user.mobile, Validators.required],
      phone: [ this._user.phone, Validators.required],
      //phoneCode: [ '' , Validators.required],
      street: [ this._user.street, Validators.required],
      state: [ this._user.street, Validators.required],
      country: [ this._user.country, Validators.required],
      zipCode: [ this._user.zipCode, Validators.required],
    })
  }

  loadBannerImage(uploadedFile: UploaderResponse[]) {
  
  }

  getProfileImages() {
  
   }
 

  submitForm(){
  }

  submitData() {
    if(this.sellerForm.valid) {
      console.log(this.sellerForm.value)
      // cloning object to convert zipCode to numerical value
      let user: User = {
        ...this.sellerForm.value, 
        zipCode: +this.sellerForm.value.zipCode,
        street:  this.sellerForm.value.street,
        country: this.sellerForm.value.country.name,
        state: this.sellerForm.value.state.name,
        phone: this.sellerForm.value.phone,
        mobile: this.sellerForm.value.mobile,
        roleType: this._user.roleType
        //phoneCode: this.selectedCountry.phonecode
      }
    
      this._auth.updateUser(user).pipe(
        catchError( err => {
          throw err
        }),
        takeUntil(this.unSubscribe$)
      ).subscribe();
      console.log(user)
    }
    
  }

  get firstName() {
    return this.sellerForm.get('firstName')
  }

  get lastName() {
    return this.sellerForm.get('lastName')
  }

  get roleType() {
    return this.sellerForm.get('roleType')
  }

  get mobile(){
    return this.sellerForm.get('mobile');
  }

  get phone() {
    return this.sellerForm.get('phone');
  }

  get street() {
    return this.sellerForm.get('street')
  }

  get state() {
    return this.sellerForm.get('state')
  }

  get country() {
    return this.sellerForm.get('country')
  }

  get zipCode() {
    return this.sellerForm.get('zipCode')
  }  

  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }

}
