import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditSellerProfileRoutingModule } from './edit-seller-profile-routing.module';
import { EditSellerProfileComponent } from './edit-seller-profile.component';
import { FileUploaderModule } from 'src/app/lib/file-uploader/file-uploader.module';

@NgModule({
  declarations: [EditSellerProfileComponent],
  imports: [
    CommonModule,
    EditSellerProfileRoutingModule,
    FormsModule,
    ReactiveFormsModule,FileUploaderModule
  ]
})
export class EditSellerProfileModule { }
