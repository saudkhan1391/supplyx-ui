import { User } from './../../shared/models/auth/user.model';
import { Component, OnInit } from '@angular/core';
import { SessionQuery } from 'src/app/state/session.query';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-seller-profile',
  templateUrl: './seller-profile.component.html',
  styleUrls: ['./seller-profile.component.scss']
})
export class SellerProfileComponent implements OnInit {

  user$! : Observable<User>;
  _user: User = {} as User

  constructor(private _query: SessionQuery) { }

  ngOnInit(): void {
    this._user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')!) : null;
    console.log(this._user.id)

  }

}
