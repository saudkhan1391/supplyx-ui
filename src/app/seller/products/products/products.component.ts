import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { PaginationResponse, PaginatorPlugin } from '@datorama/akita';
import { Observable, of, Subject } from 'rxjs';
import { switchMap, takeUntil, tap } from 'rxjs/operators';
import { Product } from 'src/app/shared/models/product/product.model';
import { ProductService } from 'src/app/shared/services/api/product.service';
import { PRODUCT_PAGINATOR_PROVIDER } from 'src/app/state/products/product.paginator';
import { DRAT_PRODUCT_PAGINATOR_PROVIDER } from 'src/app/state/products/draft-product.paginator';
import { INACTIVE_PRODUCT_PAGINATOR_PROVIDER  } from 'src/app/state/products/inactive-product.paginator'
import { DELETED_PRODUCT_PAGINATOR_PROVIDER } from  'src/app/state/products/deleted-product.paginator';
import { DDActionModel } from 'src/app/lib/table/table.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {

  unSubscribe$: Subject<void> = new Subject<void>()
  enableWindow: boolean = true;
  enableList: boolean = false;

  pagination$: Observable<PaginationResponse<Product>> = of({} as PaginationResponse<Product>);
  draftPagination$: Observable<PaginationResponse<Product>> = of({} as PaginationResponse<Product>); 
  inactivePagination$: Observable<PaginationResponse<Product>> = of({} as PaginationResponse<Product>); 
  deletedPagination$: Observable<PaginationResponse<Product>> = of({} as PaginationResponse<Product>); 

  paginationResponse: PaginationResponse<Product> = {} as PaginationResponse<Product>;  
  draftPaginationResponse: PaginationResponse<Product>  = {} as PaginationResponse<Product>; ; 
  inactivePaginationResponse: PaginationResponse<Product>  = {} as PaginationResponse<Product>; ; 
  deletedPaginationResponse: PaginationResponse<Product>  = {} as PaginationResponse<Product>; ; 
  
  perPage: number = 20;
  activeTabIndex: number = 0;

  filter: string | undefined = undefined;

  constructor(@Inject(PRODUCT_PAGINATOR_PROVIDER) public paginatorRef: PaginatorPlugin<Product>,
  @Inject( DRAT_PRODUCT_PAGINATOR_PROVIDER) public draftpaginatorRef: PaginatorPlugin<Product>,
  @Inject( INACTIVE_PRODUCT_PAGINATOR_PROVIDER) public inActivetpaginatorRef: PaginatorPlugin<Product>,
  @Inject( DELETED_PRODUCT_PAGINATOR_PROVIDER) public deletedpaginatorRef: PaginatorPlugin<Product>,
    private _productService: ProductService,
    private _router: Router) { }

    enableCard() {
      this.enableList = true;
      this.enableWindow = false;
    }

    enableTable() {
      this.enableList = false;
      this.enableWindow = true;
    }

  ngOnInit(): void {
    
    //Active Products
    this.pagination$ = this.paginatorRef.pageChanges.pipe(
      switchMap((page) => {
        const req = () => this._productService.fetchProducts({ page: page, take: this.perPage, status: 'active', filter: this.filter} );
        return this.paginatorRef.getPage(req) as Observable<PaginationResponse<Product>>;
      })
    );
    
    //Draft Products
    this.draftPagination$ = this.draftpaginatorRef.pageChanges.pipe(
      switchMap((page) =>{
        const req = () => this._productService.fetchProducts({ page: page, take: this.perPage, status: 'draft', filter: this.filter});
        return this.draftpaginatorRef.getPage(req) as Observable<PaginationResponse<Product>>
     })
    );

    //Inactive Products
    this.inactivePagination$ = this.inActivetpaginatorRef.pageChanges.pipe(
      switchMap((page) => {
        const req = () => this._productService.fetchProducts({ page: page, take: this.perPage, status: 'inactive', filter: this.filter});
        return this.inActivetpaginatorRef.getPage(req) as Observable<PaginationResponse<Product>>
      })
    );

    //Deleted Products
    this.deletedPagination$ = this.deletedpaginatorRef.pageChanges.pipe(
      switchMap((page) => {
        const req = () => this._productService.fetchProducts({ page: page, take: this.perPage, status: 'deleted', filter: this.filter});
        return this.deletedpaginatorRef.getPage(req) as Observable<PaginationResponse<Product>>
      })
    );
  }

  getPaginationResponse( paginationResponse: PaginationResponse<Product>) {
    this.paginationResponse = paginationResponse;
  }

  changePageSize( pageSize: number ) {
    this.perPage = pageSize;
    this.paginatorRef.refreshCurrentPage();
  }

  changeInActivePageSize( pageSize: number ) {
    this.perPage = pageSize;
    this.inActivetpaginatorRef.refreshCurrentPage();
  }

  changedDraftPageSize( pageSize: number ) {
    this.perPage = pageSize;
    this.draftpaginatorRef.refreshCurrentPage();
  }

  changedDeletedPageSize( pageSize: number ) {
    this.perPage = pageSize;
    this.deletedpaginatorRef.refreshCurrentPage();
  }

  changeActiveTab(activatedTabIndex: number) {
    this.activeTabIndex = activatedTabIndex;
  }

  ddAction( ddAction: DDActionModel) {
    if(ddAction && ddAction.action === 'EDIT') {
      this.clearCache();
      this._router.navigateByUrl(`products/edit-product/${ddAction.data.id}`);
    } else if(ddAction && ddAction.action === 'DELETE') {
      this._productService.updateProductByStatus(ddAction.data.id, 'deleted').pipe(
        tap( () => this.clearCache()),
        takeUntil(this.unSubscribe$)
      ).subscribe();
    } else if(ddAction && ddAction.action === 'INACTIVE') {
      this._productService.updateProductByStatus(ddAction.data.id, 'inactive').pipe(
        tap( () => this.clearCache()),
        takeUntil(this.unSubscribe$)
      ).subscribe();
    }
  }

  clearCache() {
    this.paginatorRef.clearCache();
    this.inActivetpaginatorRef.clearCache();
    this.draftpaginatorRef.clearCache();
    this.deletedpaginatorRef.clearCache();
    this.paginatorRef.refreshCurrentPage();
    this.inActivetpaginatorRef.refreshCurrentPage();
    this.draftpaginatorRef.refreshCurrentPage();
    this.deletedpaginatorRef.refreshCurrentPage();
  }

  searchProduct() {
    if(this.activeTabIndex === 0) {
      this.paginatorRef.clearCache();
      this.paginatorRef.refreshCurrentPage();
    } else if(this.activeTabIndex === 1) {
      this.inActivetpaginatorRef.clearCache();
      this.inActivetpaginatorRef.refreshCurrentPage();
    } else if(this.activeTabIndex === 2) {
      this.draftpaginatorRef.clearCache();
      this.draftpaginatorRef.refreshCurrentPage();
    } else if(this.activeTabIndex === 3) {
      this.deletedpaginatorRef.clearCache();
      this.deletedpaginatorRef.refreshCurrentPage();
    }
  }

  clearFilters() {
    this.filter = undefined;
    this.clearCache();
  }

  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }
  
}
