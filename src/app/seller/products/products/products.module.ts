import { FormsModule } from '@angular/forms';
import { ProductCardModule } from 'src/app/lib/product-card/product-card.module';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { TableModule } from '../../../lib/table/table.module';
import { PRODUCT_PAGINATOR_FACTORY, PRODUCT_PAGINATOR_PROVIDER } from 'src/app/state/products/product.paginator';
import { DRAFT_PRODUCT_PAGINATOR_FACTORY, DRAT_PRODUCT_PAGINATOR_PROVIDER  } from 'src/app/state/products/draft-product.paginator';
import { INACTIVE_PRODUCT_PAGINATOR_FACTORY, INACTIVE_PRODUCT_PAGINATOR_PROVIDER  } from 'src/app/state/products/inactive-product.paginator';
import { DELETED_PRODUCT_PAGINATOR_FACTORY, DELETED_PRODUCT_PAGINATOR_PROVIDER } from  'src/app/state/products/deleted-product.paginator';
import { ProductChangesGuardService } from './services/product-changes.gaurd';

@NgModule({
  declarations: [
    ProductsComponent,

  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    FormsModule,
    TableModule,
    ProductCardModule 
  ],
  providers: [
    { provide: PRODUCT_PAGINATOR_PROVIDER, useFactory: PRODUCT_PAGINATOR_FACTORY },
    { provide: DRAT_PRODUCT_PAGINATOR_PROVIDER, useFactory: DRAFT_PRODUCT_PAGINATOR_FACTORY },
    { provide: INACTIVE_PRODUCT_PAGINATOR_PROVIDER, useFactory: INACTIVE_PRODUCT_PAGINATOR_FACTORY },
    { provide: DELETED_PRODUCT_PAGINATOR_PROVIDER, useFactory: DELETED_PRODUCT_PAGINATOR_FACTORY },
    ProductChangesGuardService
  ]
})
export class ProductsModule { }
