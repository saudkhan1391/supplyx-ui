import { Apartment } from 'src/app/shared/models/projects/project.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';
import { filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { Category } from 'src/app/shared/models/category/category.model';
import { Product } from 'src/app/shared/models/product/product.model';
import { CategoryService } from 'src/app/shared/services/api/category.service';
import { ProductService } from 'src/app/shared/services/api/product.service';
import { CategoriesQuery } from 'src/app/state/categories/categories.query';
import { CategoriesStore } from 'src/app/state/categories/categories.store';
import { ProductFormComponent } from '../ui/product-form/product-form.component';



@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {

  unSubscribe$: Subject<void> = new Subject<void>();
  product!: Product;
  categories: Category[] = [];
  isDirty: boolean = false;

  @ViewChild('productForm') productForm!: ProductFormComponent;

  constructor( 
    private _activateRoute: ActivatedRoute,
    private _categoriesStore: CategoriesStore,
    private _categoriesQuery: CategoriesQuery,
    private _categoryService: CategoryService,
    private _productService: ProductService ) { 
      this._activateRoute.params.pipe(
        filter( params => !!params.id),
        switchMap( params => this._productService.findProductById(params.id)),
        tap( apiResponse => this.product = apiResponse.data ),
        takeUntil(this.unSubscribe$)
      ).subscribe();

      this._categoriesQuery.categories$.pipe(
        switchMap( categoryList => categoryList && categoryList.length > 0 ? of(categoryList) : 
          this._categoryService.getCategories().pipe(
            tap( () => this._categoriesStore.update( state => ({
              ...state,
              categories: categoryList
            }))),
            map( apiRes => apiRes.data)
        )),
        tap( categoryList => {
          this.categories = categoryList;
        }),
        takeUntil(this.unSubscribe$)
      ).subscribe();
    }

  ngOnInit(): void { }

  onDirty( isDirty: boolean) {
    this.isDirty = isDirty;
  }

  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }

}
