import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductChangesGuardService } from '../services/product-changes.gaurd';
import { EditProductComponent } from './edit-product.component';

const routes: Routes = [
  { path: '', component: EditProductComponent, canDeactivate: [ProductChangesGuardService] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditProductRoutingModule { }
