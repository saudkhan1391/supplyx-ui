
import { ProductsComponent } from './products.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductChangesGuardService } from './services/product-changes.gaurd';

const routes: Routes = [
  {path: '', component: ProductsComponent},
  { path: 'add-product', loadChildren: () => import('../products/add-product/add-product.module').then(m => m.AddProductModule)},
  { path: 'edit-product/:id', loadChildren: () => import('./edit-product/edit-product.module').then( m => m.EditProductModule )},
  { path: 'product-description/:id', loadChildren: () => import('../products/product-description/product-description.module').then(m => m.ProductDescriptionModule) },  
  { path: 'catalog', loadChildren: () => import('../products/catlog/catlog.module').then( m => m.CatlogModule) },
  { path: 'product-cart', loadChildren: () => import('../products/product-cart/product-cart.module').then(m => m.ProductCartModule)}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
