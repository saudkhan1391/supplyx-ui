import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewCatlogRoutingModule } from './new-catlog-routing.module';
import { NewCatlogComponent } from './new-catlog.component';
import { CatlogProductModule } from '../catlog-product/catlog-product.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'src/app/lib/button/button.module';


@NgModule({
  declarations: [NewCatlogComponent],
  imports: [
    CommonModule,
    NewCatlogRoutingModule,
    CatlogProductModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule
  ]
})
export class NewCatlogModule { }
