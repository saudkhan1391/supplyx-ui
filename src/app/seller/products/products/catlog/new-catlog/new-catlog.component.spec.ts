import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCatlogComponent } from './new-catlog.component';

describe('NewCatlogComponent', () => {
  let component: NewCatlogComponent;
  let fixture: ComponentFixture<NewCatlogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewCatlogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCatlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
