import { catchError, switchMap, takeUntil, tap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CatalogService } from 'src/app/shared/services/api/catalog.service';
import { Router } from '@angular/router';
import { of, Subject } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { ApiResponse } from 'src/app/shared/models/auth/api-response.model';
import { Catalog } from 'src/app/shared/models/catalog/catalog.model';
import { SessionQuery } from 'src/app/state/session.query';

@Component({
  selector: 'app-new-catlog',
  templateUrl: './new-catlog.component.html',
  styleUrls: ['./new-catlog.component.scss']
})
export class NewCatlogComponent implements OnInit {

  catalogForm!: FormGroup;
  unSubscribe$: Subject<void> = new Subject<void>();
  loading: boolean = false;
  disabled: boolean = false;
  apiError: string | undefined = undefined;

  constructor(private _fb: FormBuilder, private _catlogService: CatalogService, 
    private _sessionQuery: SessionQuery,
    private _router: Router) {
  }

  ngOnInit(): void {
    this.catalogForm = this._fb.group({
      id: [ undefined ],
      catalogName: [ '', [Validators.required] ],
      description: [ '', [Validators.required] ]
    });

    this._sessionQuery.editCatalog$.pipe(
      tap( catalog => {
        this.catalogForm.patchValue(catalog);
      }),
      takeUntil(this.unSubscribe$)
    ).subscribe();
  }

  submitData(){
    if(this.catalogForm.valid) {
      of(this.catalogForm.value).pipe(
        tap( () => {this.setButtonStatus(true);}),
        switchMap( catalogData => this._catlogService.addCatalog(catalogData)),
        tap( (apiResponse: ApiResponse<Catalog>) => {
          this.setButtonStatus(false);
          this._router.navigateByUrl('products/catalog');
        }),
        catchError( err => {
          this.setButtonStatus(false);
          this.parseError(err);
          throw err;
        }),
      ).subscribe();
    }
  }

  setButtonStatus( status: boolean){
    this.loading = status;
    this.disabled = status;
  }

  parseError(err: HttpErrorResponse) {
    this.apiError = err.error.message;
  }

  clearError() {
    this.apiError = undefined;
  }

  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
   }

   get catalogName() {
    return this.catalogForm.get('catalogName')
   }

   get description() {
    return this.catalogForm.get('description')
   }

}
