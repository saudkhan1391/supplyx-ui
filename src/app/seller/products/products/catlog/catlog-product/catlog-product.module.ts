import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatlogProductRoutingModule } from './catlog-product-routing.module';
import { CatlogProductComponent } from './catlog-product.component';
import { TableModule } from 'src/app/lib/table/table.module';
import { PRODUCT_PAGINATOR_FACTORY, PRODUCT_PAGINATOR_PROVIDER } from 'src/app/state/products/product.paginator';
import { DRAFT_PRODUCT_PAGINATOR_FACTORY, DRAT_PRODUCT_PAGINATOR_PROVIDER  } from 'src/app/state/products/draft-product.paginator';
import { INACTIVE_PRODUCT_PAGINATOR_FACTORY, INACTIVE_PRODUCT_PAGINATOR_PROVIDER  } from 'src/app/state/products/inactive-product.paginator';
import { DELETED_PRODUCT_PAGINATOR_FACTORY, DELETED_PRODUCT_PAGINATOR_PROVIDER } from  'src/app/state/products/deleted-product.paginator';
import { ProductCardModule } from 'src/app/lib/product-card/product-card.module';


@NgModule({
  declarations: [CatlogProductComponent],
  imports: [
    CommonModule,
    CatlogProductRoutingModule,
    TableModule,
    ProductCardModule 
  ],
  providers: [
    { provide: PRODUCT_PAGINATOR_PROVIDER, useFactory: PRODUCT_PAGINATOR_FACTORY},
    { provide: DRAT_PRODUCT_PAGINATOR_PROVIDER, useFactory: DRAFT_PRODUCT_PAGINATOR_FACTORY},
    { provide: INACTIVE_PRODUCT_PAGINATOR_PROVIDER, useFactory: INACTIVE_PRODUCT_PAGINATOR_FACTORY},
    { provide: DELETED_PRODUCT_PAGINATOR_PROVIDER, useFactory: DELETED_PRODUCT_PAGINATOR_FACTORY},
  ]
})
export class CatlogProductModule { }
