import { Component, Inject, OnInit } from '@angular/core';
import { PaginationResponse, PaginatorPlugin } from '@datorama/akita';
import { Observable, of } from 'rxjs';
import { catchError, filter, map, switchMap, tap } from 'rxjs/operators';
import { Product } from 'src/app/shared/models/product/product.model';
import { PRODUCT_PAGINATOR_PROVIDER } from 'src/app/state/products/product.paginator';
import { DRAT_PRODUCT_PAGINATOR_PROVIDER } from 'src/app/state/products/draft-product.paginator';
import { INACTIVE_PRODUCT_PAGINATOR_PROVIDER  } from 'src/app/state/products/inactive-product.paginator'
import { DELETED_PRODUCT_PAGINATOR_PROVIDER } from  'src/app/state/products/deleted-product.paginator';
import { ActivatedRoute } from '@angular/router';
import { CatalogService } from 'src/app/shared/services/api/catalog.service';
import { Catalog } from 'src/app/shared/models/catalog/catalog.model';
import { DDActionModel } from 'src/app/lib/table/table.component';

@Component({
  selector: 'app-catlog-product',
  templateUrl: './catlog-product.component.html',
  styleUrls: ['./catlog-product.component.scss']
})
export class CatlogProductComponent implements OnInit {

  pagination$: Observable<PaginationResponse<Product>> = of({} as PaginationResponse<Product>);
  draftPagination$: Observable<PaginationResponse<Product>> = of({} as PaginationResponse<Product>); 
  inactivePagination$: Observable<PaginationResponse<Product>> = of({} as PaginationResponse<Product>); 
  deletedPagination$: Observable<PaginationResponse<Product>> = of({} as PaginationResponse<Product>); 
  
  paginationResponse!: PaginationResponse<Product>;  
  draftPaginationResponse!: PaginationResponse<Product>; 
  inactivePaginationResponse!: PaginationResponse<Product>; 
  deletedPaginationResponse!: PaginationResponse<Product>;
  
  perPage = 5;
  activeTabIndex: number = 0;
  catalog: Catalog = {} as Catalog;
  enableWindow: boolean = true;
  enableList: boolean = false;

  constructor(@Inject(PRODUCT_PAGINATOR_PROVIDER) public paginatorRef: PaginatorPlugin<Product>,
    @Inject( DRAT_PRODUCT_PAGINATOR_PROVIDER) public draftpaginatorRef: PaginatorPlugin<Product>,
    @Inject( INACTIVE_PRODUCT_PAGINATOR_PROVIDER) public inActivetpaginatorRef: PaginatorPlugin<Product>,
    @Inject( DELETED_PRODUCT_PAGINATOR_PROVIDER) public deletedpaginatorRef: PaginatorPlugin<Product>,
    private _catalogService: CatalogService,
    private _activatedRoute: ActivatedRoute) { }


    enableCard() {
      this.enableList = true;
      this.enableWindow = false;
    }

    enableTable() {
      this.enableList = false;
      this.enableWindow = true;
    }


   ngOnInit(): void {

    this._activatedRoute.params.pipe(
      filter( params => !!params.catalogId),
      tap( params => this.initTablePagination(params.catalogId)),
      switchMap( params => this._catalogService.findCatalogbyId(params.catalogId).pipe(
        tap( apiResponse  => this.catalog = apiResponse.data )
      )),
      catchError( err => {
        throw err
      })
    ).subscribe();
    
  }

  initTablePagination(catalogId: number) {
    //Active Products
    this.pagination$ = this.paginatorRef.pageChanges.pipe(
      switchMap((page) => {
        const req = () => this._catalogService.fetchProductsByCatalogId(catalogId, { page: page, take: this.perPage, status: 'active'});
        return this.paginatorRef.getPage(req) as Observable<PaginationResponse<Product>>;
      })
    );
    
    //Draft Products
    this.draftPagination$ = this.draftpaginatorRef.pageChanges.pipe(
      switchMap((page) =>{
        const req = () => this._catalogService.fetchProductsByCatalogId(catalogId, { page: page, take: this.perPage, status: 'draft'});
        return this.draftpaginatorRef.getPage(req) as Observable<PaginationResponse<Product>>
     })
    );

    //Inactive Products
    this.inactivePagination$ = this.inActivetpaginatorRef.pageChanges.pipe(
      switchMap((page) => {
        const req = () => this._catalogService.fetchProductsByCatalogId(catalogId, { page: page, take: this.perPage, status: 'inactive'});
        return this.inActivetpaginatorRef.getPage(req) as Observable<PaginationResponse<Product>>
      })
    );

    //Deleted Products
    this.deletedPagination$ = this.deletedpaginatorRef.pageChanges.pipe(
      switchMap((page) => {
        const req = () => this._catalogService.fetchProductsByCatalogId(catalogId, { page: page, take: this.perPage, status: 'deleted'});
        return this.deletedpaginatorRef.getPage(req) as Observable<PaginationResponse<Product>>
      })
    );
  }

  getPaginationResponse( paginationResponse: PaginationResponse<Product>) {
    this.paginationResponse = paginationResponse;
  }

  changePageSize( pageSize: number ) {
    this.perPage = pageSize;
    this.paginatorRef.refreshCurrentPage();
  }

  changeInActivePageSize( pageSize: number ) {
    this.perPage = pageSize;
    this.inActivetpaginatorRef.refreshCurrentPage();
  }

  changedDraftPageSize( pageSize: number ) {
    this.perPage = pageSize;
    this.draftpaginatorRef.refreshCurrentPage();
  }
  
  changedDeletedPageSize( pageSize: number ) {
    this.perPage = pageSize;
    this.deletedpaginatorRef.refreshCurrentPage();
  }

  changeActiveTab(activatedTabIndex: number) {
    this.activeTabIndex = activatedTabIndex;
  }

  ddAction( ddAction: DDActionModel) {
    if(ddAction && ddAction.action === 'CATALOG') {
      this._catalogService.addToCatalog(this.catalog.id, [ddAction.data.id]).pipe(
        tap( () => this.clearCache())
      ).subscribe()
    }
  }

  clearCache() {
    this.paginatorRef.clearCache();
    this.inActivetpaginatorRef.clearCache();
    this.draftpaginatorRef.clearCache();
    this.deletedpaginatorRef.clearCache();
    this.paginatorRef.refreshCurrentPage();
    this.inActivetpaginatorRef.refreshCurrentPage();
    this.draftpaginatorRef.refreshCurrentPage();
    this.deletedpaginatorRef.refreshCurrentPage();
  }

}
