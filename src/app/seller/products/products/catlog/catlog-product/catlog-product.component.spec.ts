import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CatlogProductComponent } from './catlog-product.component';

describe('CatlogProductComponent', () => {
  let component: CatlogProductComponent;
  let fixture: ComponentFixture<CatlogProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CatlogProductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CatlogProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
