import { CatlogProductComponent } from './catlog-product.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: CatlogProductComponent
  },
  {
    path: 'add-products-catlog', loadChildren: () => import('../catlog-product/add-product-catlog/add-product-catlog.module')
      .then( m => m.AddProductCatlogModule)
  }
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatlogProductRoutingModule { }
