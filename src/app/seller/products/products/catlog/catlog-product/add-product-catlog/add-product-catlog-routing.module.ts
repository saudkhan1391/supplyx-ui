import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddProductCatlogComponent } from './add-product-catlog.component';

const routes: Routes = [
  {
    path: '', component:AddProductCatlogComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddProductCatlogRoutingModule { }
