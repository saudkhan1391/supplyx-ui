import { Component, Inject, OnInit } from '@angular/core';
import { PaginationResponse, PaginatorPlugin } from '@datorama/akita';
import { Observable, of } from 'rxjs';
import { catchError, filter, switchMap, tap } from 'rxjs/operators';
import { Product } from 'src/app/shared/models/product/product.model';
import { ProductService } from 'src/app/shared/services/api/product.service';
import { PRODUCT_PAGINATOR_PROVIDER } from 'src/app/state/products/product.paginator';
import { DRAT_PRODUCT_PAGINATOR_PROVIDER } from 'src/app/state/products/draft-product.paginator';
import { INACTIVE_PRODUCT_PAGINATOR_PROVIDER  } from 'src/app/state/products/inactive-product.paginator'
import { DELETED_PRODUCT_PAGINATOR_PROVIDER } from  'src/app/state/products/deleted-product.paginator';
import { Catalog } from 'src/app/shared/models/catalog/catalog.model';
import { ActivatedRoute } from '@angular/router';
import { CatalogService } from 'src/app/shared/services/api/catalog.service';
import { CheckBoxModel, DDActionModel } from 'src/app/lib/table/table.component';

@Component({
  selector: 'app-add-product-catlog',
  templateUrl: './add-product-catlog.component.html',
  styleUrls: ['./add-product-catlog.component.scss']
})
export class AddProductCatlogComponent implements OnInit {

  pagination$: Observable<PaginationResponse<Product>> = of({} as PaginationResponse<Product>);
  draftPagination$: Observable<PaginationResponse<Product>> = of({} as PaginationResponse<Product>); 
  inactivePagination$: Observable<PaginationResponse<Product>> = of({} as PaginationResponse<Product>); 
  deletedPagination$: Observable<PaginationResponse<Product>> = of({} as PaginationResponse<Product>); 
  
  paginationResponse!: PaginationResponse<Product>;  
  draftPaginationResponse!: PaginationResponse<Product>; 
  inactivePaginationResponse!: PaginationResponse<Product>; 
  deletedPaginationResponse!: PaginationResponse<Product>; 

  perPage = 5
  activeTabIndex: number = 0;
  catalog: Catalog = {} as Catalog;
  loading: boolean = false;
  disabled: boolean = false;

  selectedCount: number = 0;
  selectedProducts: CheckBoxModel[] = [];

  constructor(@Inject(PRODUCT_PAGINATOR_PROVIDER) public paginatorRef: PaginatorPlugin<Product>,
    @Inject( DRAT_PRODUCT_PAGINATOR_PROVIDER) public draftpaginatorRef: PaginatorPlugin<Product>,
    @Inject( INACTIVE_PRODUCT_PAGINATOR_PROVIDER) public inActivetpaginatorRef: PaginatorPlugin<Product>,
    @Inject( DELETED_PRODUCT_PAGINATOR_PROVIDER) public deletedpaginatorRef: PaginatorPlugin<Product>,
    private _productService: ProductService,
    private _catalogService: CatalogService,
    private _activatedRoute: ActivatedRoute) { }

   ngOnInit(): void {

    this._activatedRoute.params.pipe(
      filter( params => !!params.catalogId),
      tap( () => this.initTablePagination()),
      switchMap( params => this._catalogService.findCatalogbyId(params.catalogId).pipe(
        tap( apiResponse  => this.catalog = apiResponse.data )
      )),
      catchError( err => {
        throw err
      })
    ).subscribe();
    
  }

  initTablePagination(){
    //Active Products
    this.pagination$ = this.paginatorRef.pageChanges.pipe(
      switchMap((page) => {
        const req = () => this._productService.fetchProducts({ page: page, take: this.perPage});
        return this.paginatorRef.getPage(req) as Observable<PaginationResponse<Product>>;
      })
    );

    //Draft Products
    this.draftPagination$ = this.draftpaginatorRef.pageChanges.pipe(
      switchMap((page) =>{
        const req = () => this._productService.fetchProducts({ page: page, take: this.perPage});
        return this.draftpaginatorRef.getPage(req) as Observable<PaginationResponse<Product>>
     })
    );

    //Inactive Products
    this.inactivePagination$ = this.inActivetpaginatorRef.pageChanges.pipe(
      switchMap((page) => {
        const req = () => this._productService.fetchProducts({ page: page, take: this.perPage});
        return this.inActivetpaginatorRef.getPage(req) as Observable<PaginationResponse<Product>>
      })
    );

    //Deleted Products
    this.deletedPagination$ = this.deletedpaginatorRef.pageChanges.pipe(
      switchMap((page) => {
        const req = () => this._productService.fetchProducts({ page: page, take: this.perPage});
        return this.deletedpaginatorRef.getPage(req) as Observable<PaginationResponse<Product>>
      })
    );
  }

  getPaginationResponse( paginationResponse: PaginationResponse<Product>) {
    this.paginationResponse = paginationResponse;
  }

  changePageSize( pageSize: number ) {
    this.perPage = pageSize;
    this.paginatorRef.refreshCurrentPage();
  }

  changeInActivePageSize( pageSize: number ) {
    this.perPage = pageSize;
    this.inActivetpaginatorRef.refreshCurrentPage();
  }
  changedDraftPageSize( pageSize: number ) {
    this.perPage = pageSize;
    this.draftpaginatorRef.refreshCurrentPage();
  }
  changedDeletedPageSize( pageSize: number ) {
    this.perPage = pageSize;
    this.deletedpaginatorRef.refreshCurrentPage();
  }

  changeActiveTab(activatedTabIndex: number) {
    this.activeTabIndex = activatedTabIndex;
  }


  ddAction( ddAction: DDActionModel) {
    if(ddAction && ddAction.action === 'CATALOG') {
      this._catalogService.addToCatalog(this.catalog.id, [ddAction.data.id]).pipe(
        tap( () => this.clearCache())
      ).subscribe()
    }
  }

  clearCache() {
    this.paginatorRef.clearCache();
    this.inActivetpaginatorRef.clearCache();
    this.draftpaginatorRef.clearCache();
    this.deletedpaginatorRef.clearCache();
    this.paginatorRef.refreshCurrentPage();
    this.inActivetpaginatorRef.refreshCurrentPage();
    this.draftpaginatorRef.refreshCurrentPage();
    this.deletedpaginatorRef.refreshCurrentPage();
  }

  addMultipleToCatalog(){
    if(this.selectedProducts && this.selectedProducts.length > 0 ) {
      let productIds: number[] = this.selectedProducts.filter( product => product.checked)
        .map( product => product.productId);
        this.setbuttonStatus(true);
        this._catalogService.addToCatalog(this.catalog.id, productIds).pipe(
          tap( () => {
            this.setbuttonStatus(false);
            this.clearCache();
          }),
          catchError( err => {
            this.setbuttonStatus(false);
            throw err
          })
        ).subscribe();
    }
    
  }

  onSelection(selectedProducts: CheckBoxModel[]) {
    this.selectedProducts = selectedProducts;
    this.selectedCount = selectedProducts.filter( product => product.checked).length;
  }

  setbuttonStatus(status: boolean) {
    this.loading = status;
    this.disabled = status;
  }

}
