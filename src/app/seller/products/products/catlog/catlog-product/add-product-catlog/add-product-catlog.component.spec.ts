import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProductCatlogComponent } from './add-product-catlog.component';

describe('AddProductCatlogComponent', () => {
  let component: AddProductCatlogComponent;
  let fixture: ComponentFixture<AddProductCatlogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddProductCatlogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProductCatlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
