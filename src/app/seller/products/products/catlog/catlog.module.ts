import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatlogRoutingModule } from './catlog-routing.module';
import { CatlogComponent } from './catlog.component';
import { TableModule } from 'src/app/lib/table/table.module';
import { CatalogTableModule } from 'src/app/lib/catalog-table/catalog-table.module';
import { PRODUCT_PAGINATOR_FACTORY, PRODUCT_PAGINATOR_PROVIDER } from 'src/app/state/products/product.paginator';
import { DRAFT_PRODUCT_PAGINATOR_FACTORY, DRAT_PRODUCT_PAGINATOR_PROVIDER  } from 'src/app/state/products/draft-product.paginator';
import { INACTIVE_PRODUCT_PAGINATOR_FACTORY, INACTIVE_PRODUCT_PAGINATOR_PROVIDER  } from 'src/app/state/products/inactive-product.paginator';
import { DELETED_PRODUCT_PAGINATOR_FACTORY, DELETED_PRODUCT_PAGINATOR_PROVIDER } from  'src/app/state/products/deleted-product.paginator';
import { CATALOG_PAGINATOR_FACTORY, CATALOG_PAGINATOR_PROVIDER } from 'src/app/state/catalog/catalog.paginator';


@NgModule({
  declarations: [CatlogComponent],
  imports: [
    CommonModule,
    CatlogRoutingModule,
    TableModule,
    CatalogTableModule
  ],
  providers: [
    { provide: PRODUCT_PAGINATOR_PROVIDER, useFactory: PRODUCT_PAGINATOR_FACTORY },
    { provide: DRAT_PRODUCT_PAGINATOR_PROVIDER, useFactory: DRAFT_PRODUCT_PAGINATOR_FACTORY },
    { provide: INACTIVE_PRODUCT_PAGINATOR_PROVIDER, useFactory: INACTIVE_PRODUCT_PAGINATOR_FACTORY },
    { provide: DELETED_PRODUCT_PAGINATOR_PROVIDER, useFactory: DELETED_PRODUCT_PAGINATOR_FACTORY },
    { provide: CATALOG_PAGINATOR_PROVIDER, useFactory: CATALOG_PAGINATOR_FACTORY }
  ]
})
export class CatlogModule { }
