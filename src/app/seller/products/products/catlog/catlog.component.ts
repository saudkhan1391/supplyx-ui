import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PaginationResponse, PaginatorPlugin } from '@datorama/akita';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { DDActionModel } from 'src/app/lib/catalog-table/catalog-table.component';
import { Catalog } from 'src/app/shared/models/catalog/catalog.model';
import { CatalogService } from 'src/app/shared/services/api/catalog.service';
import { CATALOG_PAGINATOR_PROVIDER } from 'src/app/state/catalog/catalog.paginator';
import { SessionStore } from 'src/app/state/session.state';

export interface MenuItem{
  id: number,
  catlogName: string,
  productNumber: number
}

@Component({
  selector: 'app-catlog',
  templateUrl: './catlog.component.html',
  styleUrls: ['./catlog.component.scss']
})
export class CatlogComponent implements OnInit, OnDestroy {

  catalogPagination$: Observable<PaginationResponse<Catalog>> = of({} as PaginationResponse<Catalog>);
  paginationResponse: PaginationResponse<Catalog> = {} as PaginationResponse<Catalog>;
  perPage: number = 20;

  constructor(@Inject(CATALOG_PAGINATOR_PROVIDER) public catalogRef: PaginatorPlugin<Catalog>,
    private _catalogService: CatalogService,
    private _sessionStore: SessionStore,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this.catalogPagination$ = this.catalogRef.pageChanges.pipe(
      switchMap( (page) => {
        const req = () => this._catalogService.fetchAllCatalog({ page: page, take: this.perPage });
        return this.catalogRef.getPage(req) as Observable<PaginationResponse<Catalog>>;
      }))
  }

  getPaginationResponse( paginationResponse: PaginationResponse<Catalog>) {
    this.paginationResponse = paginationResponse;
  }

  changePageSize( pageSize: number ) {
    this.perPage = pageSize;
    this.catalogRef.refreshCurrentPage();
  }

  ddAction( ddAction: DDActionModel) {
    if(ddAction && ddAction.action === 'EDIT') {
      this._sessionStore.update( state => ({
        ...state,
        editCatalog: ddAction.data
      }));
      this.catalogRef.clearCache();
      this._router.navigateByUrl('products/catalog/new-catlog');
    } else if(ddAction && ddAction.action === 'DELETE') {

    }
  }

  ngOnDestroy() {
    this.catalogRef.destroy();
  }

}
