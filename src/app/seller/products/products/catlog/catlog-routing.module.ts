import { CatlogComponent } from './catlog.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component:CatlogComponent },
  {
    path: 'new-catlog', loadChildren: () => import('../catlog/new-catlog/new-catlog.module').then( m => m.NewCatlogModule)
  },
  {
    path: 'product-catlog-list/:catalogId', loadChildren: () => import('../catlog/catlog-product/catlog-product.module').then( m => m.CatlogProductModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatlogRoutingModule { }
