import { CanDeactivate } from "@angular/router";
import { Injectable } from "@angular/core";
import { ProductFormComponent } from "../ui/product-form/product-form.component";
import { EditProductComponent } from "../edit-product/edit-product.component";
import { AddProductComponent } from "../add-product/add-product.component";

@Injectable()
export class ProductChangesGuardService implements CanDeactivate<EditProductComponent> {

    canDeactivate(component: EditProductComponent | AddProductComponent): boolean {
        if (component.isDirty) {
            component.productForm.openWarningDialog();
        }
        return !component.isDirty;
    }
}