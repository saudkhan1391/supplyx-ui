import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-description-sidebar',
  templateUrl: './description-sidebar.component.html',
  styleUrls: ['./description-sidebar.component.scss']
})
export class DescriptionSidebarComponent implements OnInit {

  descriptionMenus = [
    { title: 'Product Information', url: 'product-information' },
    { title: 'Product Specification', url: '/' },
    { title: 'Shipping & Delivery', url: '/' },
    { title: 'Reviews', url: '/' },
  ]


  constructor() { }

  ngOnInit(): void {
  }

}
