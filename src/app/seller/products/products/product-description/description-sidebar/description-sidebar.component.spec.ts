import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DescriptionSidebarComponent } from './description-sidebar.component';

describe('DescriptionSidebarComponent', () => {
  let component: DescriptionSidebarComponent;
  let fixture: ComponentFixture<DescriptionSidebarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DescriptionSidebarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DescriptionSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
