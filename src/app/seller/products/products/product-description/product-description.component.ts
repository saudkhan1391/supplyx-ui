import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { catchError, filter, switchMap, takeUntil, tap } from 'rxjs/operators';
import { Cart } from 'src/app/shared/models/cart/cart.model';
import { Product } from 'src/app/shared/models/product/product.model';
import { ProductService } from 'src/app/shared/services/api/product.service';
import { ScrollService } from 'src/app/shared/services/scroll/scroll.service';
import { SessionQuery } from 'src/app/state/session.query';
import { ProductNavItem } from '../ui/product-form/product-form.component';

@Component({
  selector: 'app-product-description',
  templateUrl: './product-description.component.html',
  styleUrls: ['./product-description.component.scss'],
})
export class ProductDescriptionComponent implements OnInit, OnDestroy {
  unSubscribe$: Subject<void> = new Subject<void>();
  product: Product = {} as Product;
  slides: string[] = [];
  role!: string | null;
  showToast:boolean = false;

  navigationList: ProductNavItem[] = [
    { title: 'Product Information', target: '#product-info' },
    { title: 'Product Specification', target: '#product-specs' },
    { title: 'Shipping and Delivery', target: '#shipping' },
    { title: 'Reviews', target: '#reviews' },
  ];

  currentSection: string = this.navigationList[0].title;

  constructor(
    private _productService: ProductService,
    private _activatedRoute: ActivatedRoute,
    private scrollService: ScrollService,
    private _router: Router,
    private _query: SessionQuery
  ) {
    this._activatedRoute.params
      .pipe(
        filter((params) => !!params.id),
        switchMap((params) => this._productService.findProductById(params.id)),
        tap((apiResponse) => {
          this.product = apiResponse.data;
          this.slides = this.product.images.map((image) => image.url);
        }),
        catchError((err) => {
          throw err;
        })
      )
      .subscribe();
  }

  ngOnInit(): void {
    this.getRole();
  }

/*   getRole() {
    this._query.role$.subscribe((res) => {
      this.role = res;
    });
  } */

    getRole(){
      this._query.role$.subscribe(
        res => {
          this.role = res
        }
      )
      console.log('role', this.role);
    }

  changeCurrentSection(i: number) {
    this.currentSection = this.navigationList[i].title;
    this.scrollService.scrollToElement(`${this.navigationList[i].target}`);
  }

  edit() {
    this._router.navigateByUrl(`products/edit-product/${this.product.id}`);
  }

  delete() {
    this._productService
      .updateProductByStatus(this.product.id, 'deleted')
      .pipe(takeUntil(this.unSubscribe$))
      .subscribe();
  }

  markInactive() {
    this._productService
      .updateProductByStatus(this.product.id, 'inactive')
      .pipe(takeUntil(this.unSubscribe$))
      .subscribe();
  }

  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }

  addToCart(product: Product) {
    console.log('product : ', product);
    let itemForCart = { id: product.id, quantity: 0, productName: product.productName };
    this._productService.addToCart(itemForCart).subscribe((res) => {
      console.log('res add to cart :', res);
      this.showToast = true;
    });
  }

  downloadMyFile(url: string, name: string) {
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', url);
    link.setAttribute('download', name);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }
}
