import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductDescriptionRoutingModule } from './product-description-routing.module';
import { ProductDescriptionComponent } from './product-description.component';
import { DescriptionSidebarComponent } from './description-sidebar/description-sidebar.component';
import { ProductSliderModule } from 'src/app/lib/product-slider/product-slider.module';
import { NkHeaderModule } from 'src/app/lib/nk-header/nk-header.module';
import { DocumentPreviewModule } from 'src/app/lib/document-preview/document-preview.module';
import { QuillModule } from 'ngx-quill';
import { NgbToastModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [ProductDescriptionComponent, DescriptionSidebarComponent],
  imports: [
    CommonModule,
    ProductDescriptionRoutingModule,
    ProductSliderModule,
    NkHeaderModule,
    DocumentPreviewModule,
    NgbToastModule,
    QuillModule.forRoot()
  ]
})
export class ProductDescriptionModule { }
