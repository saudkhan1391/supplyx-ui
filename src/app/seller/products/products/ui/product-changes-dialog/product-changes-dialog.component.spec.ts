import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductChangesDialogComponent } from './product-changes-dialog.component';

describe('ProductChangesDialogComponent', () => {
  let component: ProductChangesDialogComponent;
  let fixture: ComponentFixture<ProductChangesDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductChangesDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductChangesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
