import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Product } from 'src/app/shared/models/product/product.model';

@Component({
  selector: 'app-product-changes-dialog',
  templateUrl: './product-changes-dialog.component.html',
  styleUrls: ['./product-changes-dialog.component.scss']
})
export class ProductChangesDialogComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<ProductChangesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: Product) { }

  ngOnInit(): void {
  }

  discardChanges() {
    this.dialogRef.close({
      action: 'DISCARD'
    });
  }

  saveChanges() {
    this.dialogRef.close({
      action: 'SAVE'
    });
  }

  cancel() {
    this.dialogRef.close();
  }
}
