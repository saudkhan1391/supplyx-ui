import { Component, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { catchError, switchMap, takeUntil, tap } from 'rxjs/operators';
import { FileUploaderComponent } from 'src/app/lib/file-uploader/file-uploader.component';
import { ApiResponse } from 'src/app/shared/models/auth/api-response.model';
import { Category } from 'src/app/shared/models/category/category.model';
import { UploaderResponse } from 'src/app/shared/models/files/uploader-response';
import { ProductImage, ProductSpec } from 'src/app/shared/models/product/image.model';
import { Product } from 'src/app/shared/models/product/product.model';
import { FileUploaderService } from 'src/app/shared/services/api/file-uploader.service';
import { ProductService } from 'src/app/shared/services/api/product.service';
import { ScrollService } from 'src/app/shared/services/scroll/scroll.service';
import { SessionQuery } from 'src/app/state/session.query';
import { ProductChangesDialogComponent } from '../product-changes-dialog/product-changes-dialog.component';

export interface ProductNavItem {
  title: string,
  target: string
}

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit, OnDestroy {

  navigationList: ProductNavItem[] = [
    { title: 'Product Information', target: '#product-info' },
    { title: 'Product Specification', target: '#product-specs' },
    { title: 'Pricing & Quantity', target: '#product-pricing' },
    { title: 'Product Image/Videos', target: '#product-images' },
    { title: 'Shipping Information', target: '#product-shipping' }
  ];

  currentSection: string = this.navigationList[0].title;
  unSubscribe$: Subject<void> = new Subject<void>();
  addProductErrors: string | ValidationErrors | null = null;
  loading: boolean = false;
  disabled: boolean = false;

  draftLoading: boolean = false;
  draftDisabled: boolean = false;

  uploadedImages: ProductImage[] = [];
  uploadedSpecs: ProductSpec[] = [];

  _product: Product = {} as Product;
  @Input() set product(product: Product) {
    this._product = product;
  }
  _categories: Category[] = [];
  @Input() set categories(categories: Category[]) {
    this._categories = categories;
  }

  _title: string = 'Add Product';
  @Input() set title(title: string) {
    this._title = title;
  }

  productForm!: FormGroup;
  fileName: string = '';
  role!: string | null;

  @ViewChild('galleryUploader') galleryUploader!: FileUploaderComponent;
  @ViewChild('specUploader') specUploader!: FileUploaderComponent;
  @Output() onDirty: EventEmitter<boolean> = new EventEmitter<boolean>();


  constructor(
    private fb: FormBuilder,
    private _productService: ProductService,
    private _router: Router,
    private _fileUploader: FileUploaderService,
    public dialog: MatDialog,
    private scrollService: ScrollService,
    private _query: SessionQuery
  ) { }

  ngOnInit(): void {
    this.getRole();
    this.createForm();
    // this is required to bring category selected
    this.productForm.patchValue({
      category: this._categories.find(categoryItem => categoryItem.id === this._product.category.id)
    });
  }

  createForm() {
    this.productForm = this.fb.group({
      id: [this._product.id],
      productName: [this._product.productName, [Validators.required, /* Validators.pattern('[a-zA-Z]*') */]],
      category: [this._product.category, Validators.required,],
      brand: [this._product.brand, Validators.required],
      price: [this._product.price, Validators.required],
      minimumQuantity: [this._product.minimumQuantity, Validators.required],
      availableQuantity: [this._product.availableQuantity, Validators.required],
      manufacturer: [this._product.manufacturer, Validators.required],
      partNo: [this._product.partNo, Validators.required],
      hsCode: [this._product.hsCode, Validators.required],
      description: [this._product.description, Validators.required],
      specifications: [this._product.specifications],
      images: [this._product.images],
      /* shipinfo: [this._product.shipinfo, Validators.required], */
      status: [this._product.status]
    })
  }

  submitData() {
    if (this.productForm.valid) {
      this.productForm.patchValue({
        status: 'active'
      });
      this.saveProduct().subscribe();
    } else {
      this.addProductErrors = this.productForm.errors;
    }
  }

  submitDraft() {
    this.productForm.patchValue({
      status: 'draft'
    });
    this.saveProduct().subscribe();
  }

  saveProduct(): Observable<ApiResponse<Product>> {
  /*   this.getFinalImages(); */
   /*  this.getFinalSpecs(); */
    let apiCall$ = this.productForm.value.status === 'draft' ?
      this._productService.addDraftProduct(this.productForm.value) :
      this._productService.addProduct(this.productForm.value);
    this.setButtonLoadingStatus(this.productForm.value.status, true)
    return apiCall$.pipe(
      tap(apiResponse => {
        this.setButtonLoadingStatus(this.productForm.value.status, false)
        this._product = apiResponse.data;
        console.log(apiResponse.data)
        this.clearUploaderData();
      }),
      catchError(err => {
        this.setButtonLoadingStatus(this.productForm.value.status, false)
        throw err
      }),
      takeUntil(this.unSubscribe$)
    );
  }

  setButtonLoadingStatus(status: string, state: boolean) {
    if (status === 'draft') {
      this.draftLoading = state;
      this.draftDisabled = state;
    } else {
      this.loading = state;
      this.disabled = state;
    }
  }

  get productName() {
    return this.productForm.get('productName');
  }

  get productCategory() {
    return this.productForm.get(' productCategory');
  }

  get addCategory() {
    return this.productForm.get(' productCategory');
  }

  get brand() {
    return this.productForm.get('brand');
  }

  get manufacturer() {
    return this.productForm.get('manufacturer');
  }

  get partNo() {
    return this.productForm.get('partNo');
  }

  get hscode() {
    return this.productForm.get('hsCode');
  }

  get description() {
    return this.productForm.get('description');
  }

  get shipinfo() {
    return this.productForm.get('shipinfo');
  }

  onMinimumQuantityChanges(count: number) {
    this.productForm.patchValue({
      minimumQuantity: count
    })
    this._product.minimumQuantity = count;
  }

  onAvailableQuantityChanges(count: number) {
    this.productForm.patchValue({
      availableQuantity: count
    })
    this._product.availableQuantity = count;
  }

  loadImageFile(uploadedFile: UploaderResponse[]) {
    this.uploadedImages = uploadedFile.map(uploadedFile => ({
      key: uploadedFile.key,
      url: uploadedFile.path,
      productId: this._product.id
    } as ProductImage));
    this.getFinalImages();
    this.onDirty.emit(true);
  }

  onRemoveImage(event: any) {
    console.log(event)
    this._product.images = [...this._product.images.filter( res => res.url !== event)];
    this.productForm.patchValue({
      images: this._product.images
    })
    console.log(this._product.images)
  }

  getFinalImages() {
    this._product.images = [/* ...this._product.images, */ ...this.uploadedImages];
    this.productForm.patchValue({
      images: this._product.images
    })
  }

  onGalleryRemoved(removedUploadedFile: UploaderResponse) {
    this._product.images = this._product.images.filter(image => image.key !== removedUploadedFile.key);
    this.uploadedImages = this.uploadedImages.filter(image => image.key !== removedUploadedFile.key);
  }

  loadSpecFiles(uploadedSpecsFile: UploaderResponse[]) {
    this.uploadedSpecs = uploadedSpecsFile.map(uploadedSpecFile => ({
      key: uploadedSpecFile.key,
      url: uploadedSpecFile.path,
      productId: this._product.id
    } as ProductSpec));
    this.getFinalSpecs();
    /*  this.uploadedSpecs.splice(0, this.uploadedSpecs.length) */
    this.onDirty.emit(true);
  }
   
  onRemoveSpec(event: any) {
    console.log(event)
    this._product.specifications = [...this._product.specifications.filter( res => res.url !== event)];
    this.productForm.patchValue({
      specifications: this._product.specifications
    })
    console.log(this._product.specifications)
  }
 
  getFinalSpecs() {
    this._product.specifications = [/* ...this._product.specifications, */ ...this.uploadedSpecs];
    this.productForm.patchValue({
      specifications: this._product.specifications
    })
   }

  onSpecRemoved(removedUploadedFile: UploaderResponse) {
    this._product.specifications = this._product.specifications.filter(attachment => attachment.key !== removedUploadedFile.key);
    this.uploadedSpecs = this.uploadedSpecs.filter(attachment => attachment.key !== removedUploadedFile.key);
  }

  openWarningDialog() {
    const matDialConfig = new MatDialogConfig();
    /* matDialConfig.maxWidth = '100vw';
    matDialConfig.maxHeight = '100vh';
    matDialConfig.height =  '100%';
    matDialConfig.width = '100%'; */
    matDialConfig.data = this._product;
    const dialogRef = this.dialog.open(ProductChangesDialogComponent, matDialConfig);
    dialogRef.afterClosed().pipe(
      switchMap(dialogData => dialogData.action === 'DISCARD' ?
        this.clearUploadedFiles() :
        this.saveProduct()
      ),
      tap(() => this.clearUploaderData()),
      takeUntil(this.unSubscribe$)
    ).subscribe();
  }

  clearUploaderData() {
    this.uploadedImages = [];
    this.uploadedSpecs = [];
    this.galleryUploader.reset();
    this.specUploader.reset();
    this.onDirty.emit(false);
  }

  clearUploadedFiles() {
    return of(this.uploadedImages).pipe(
      // clearing uploaded images
      switchMap(uploadedImages => uploadedImages && uploadedImages.length > 0 ?
        forkJoin(this.uploadedImages.map(uploadedImage => this._fileUploader.deleteFile(uploadedImage.key))) :
        of([])
      ),
      // clearing uploadedSpecs
      switchMap(() => this.uploadedSpecs && this.uploadedSpecs.length > 0 ?
        forkJoin(this.uploadedSpecs.map(uploadedSpec => this._fileUploader.deleteFile(uploadedSpec.key))) :
        of([])
      )
    );
  }

  /* @HostListener("window:beforeunload", ["$event"])
  unloadHandler(event: Event) {
    if( this.uploadedImages && this.uploadedImages.length > 0) {
      this.openWarningDialog();
    }
    event.returnValue = false; // stay on same page
  } */

  changeCurrentSection(i: number) {
    this.currentSection = this.navigationList[i].title;
    this.scrollService.scrollToElement(`${this.navigationList[i].target}`);
  }

  getRole(){
    this._query.role$.subscribe(
      res => {
        this.role = res
      }
    )
    console.log('role', this.role);
  }

  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }

  

}
