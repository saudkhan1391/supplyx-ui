import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductFormComponent } from './product-form/product-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'src/app/lib/button/button.module';
import { FileUploaderModule } from 'src/app/lib/file-uploader/file-uploader.module';
import { RouterModule } from '@angular/router';
import { NumberSpinnerModule } from 'src/app/lib/number-spinner/number-spinner.module';
import { ProductChangesDialogComponent } from './product-changes-dialog/product-changes-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { DocumentPreviewModule } from 'src/app/lib/document-preview/document-preview.module';
import { TextEditorModule } from 'src/app/lib/text-editor/text-editor.module';

const COMPS = [
  ProductFormComponent,
  ProductChangesDialogComponent
]

@NgModule({
  declarations: [...COMPS],
  imports: [
    CommonModule,
    FormsModule, 
    ReactiveFormsModule,
    ButtonModule,
    FileUploaderModule,
    NumberSpinnerModule,
    DocumentPreviewModule,
    RouterModule,
    MatDialogModule,
    TextEditorModule
  ],
  exports: [
    ...COMPS
  ]
})
export class UiModule { }
