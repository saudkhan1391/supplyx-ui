import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PaginationResponse, PaginatorPlugin } from '@datorama/akita';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { SearchModel } from 'src/app/buyer/market-place/market-place.component';
import { CheckBoxModel, DDActionModel } from 'src/app/lib/product-card/product-card.component';
import { Product } from 'src/app/shared/models/product/product.model';
import { ProductService } from 'src/app/shared/services/api/product.service';
import { BrandsService } from 'src/app/state/market-place/brands.service';
import { ProductsService } from 'src/app/state/products/product.service';

@Component({
  selector: 'app-product-cart',
  templateUrl: './product-cart.component.html',
  styleUrls: ['./product-cart.component.scss']
})
export class ProductCartComponent implements OnInit {

  // public isCollapsed = true;
  user: any;
  getAllProductsData: any;
  public isMenuCollapsed = false;
  keyword = 'name';
  data: SearchModel[] = [];


  paginationResponse!: PaginationResponse<Product>;
  @Input() pagination!: Observable<PaginationResponse<Product>>;
  @Input() paginationRef!: PaginatorPlugin<Product>;
  @Input() status: string = '';
  @Input() isSelectable: boolean = false;
  @Input() isInsideCatalog: boolean = false;

  @Output() response: EventEmitter<PaginationResponse<Product>> = new EventEmitter();
  @Output() pageSizeChange: EventEmitter<number> = new EventEmitter();
  @Output() dropdownAction: EventEmitter<DDActionModel> = new EventEmitter();
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onSelected: EventEmitter<CheckBoxModel[]> = new EventEmitter<CheckBoxModel[]>();

  pagination$!: Observable<PaginationResponse<Product>>;
  pageSize: number = 20;
  pageSizes: number[] = [20, 50];
  reservedProducts: any[] = [];
  enableWindow: boolean = false;
  enableList: boolean = true;

  productsCategories: any[] = [];
  brands: any[] = [];
  productsBrand = ['Kajaria', 'Sierra', 'Phillips', 'Fenestrelle', 'JOOFAN', 'eSync', 'Finesta', 'CRAFTSDEC', 'ALTON Store', 'YaeTek'];
  productspartNo = ['MF1-110333', 'FEN 84010', 'B08T1J1N4R', '\tB07PRV869V', '21dfd', '2335454', '21548', '21', 'MF-98697521313', 'MF-12345478587', 'MF1-1238123584', 'MF-198261360', '12345'];
  allCategories: any[] = [];
  productIds: CheckBoxModel[] = [];


  enableCard() {
    this.enableList = true;
    this.enableWindow = false;
  }

  enableTable() {
    this.enableList = false;
    this.enableWindow = true;
  }

  constructor(private _product: ProductService, private _brandsService: BrandsService, private _productsService: ProductsService) {
    _product.getAllProductsData().subscribe((data) => {
      this.reservedProducts = data.data;
      this._productsService.setProducts(data.data);

      this._productsService.getProducts().subscribe(res => {
        this.getAllProductsData = res;

        let categoryNames = data.data.map((item: any) => { if (item.category && item.category.name) return item.category?.name });
        this.brands = [...new Set(data.data.map((item: any) => { if (item.brand) return item.brand }).filter(item => item))];
        // data for search auto
        this.data = this.getAllProductsData.map((item: any) => { return { name: item.productName } });
        this.brands.forEach((item: any) => { this.data.push({ name: item }) });
        categoryNames.forEach((item: any) => { this.data.push({ name: item }) });
        this.data = this.data.filter((item: any) => { if (item.name) return item });

        this._brandsService.setBrands(this.brands);
        categoryNames = categoryNames.filter((item: string) => item);
        this.productsCategories = [...new Set(categoryNames)];
      })

    })
  }
  ngOnInit(): void {

    this.pagination.pipe(
      tap(pagination => {
        this.paginationResponse = pagination;
        this.productIds = pagination.data.map(product => ({
          productId: product.id,
          checked: false
        }))
        this.response.emit(this.paginationResponse)
      })
    ).subscribe();
  }

  selectCategory(product: any) {
    console.log(" getAllProductsData : ", this.getAllProductsData);
    console.log(" product :  : ", product);
    this.getAllProductsData = this.reservedProducts.filter((item: Product) => item.category?.name == product);
  }

  selectpartNo(product: any) {
    console.log(" getAllProductsData : ", this.getAllProductsData);
    console.log(" product :  : ", product);
    this.getAllProductsData = this.reservedProducts.filter((item: Product) => item.partNo == product);
  }
  selectBrand(product: any) {
    console.log(" getAllProductsData : ", this.getAllProductsData);
    console.log(" product :  : ", product);
    this.getAllProductsData = this.reservedProducts.filter((item: Product) => item.brand == product);
  }

  clearFilters() {
    this.getAllProductsData = this.reservedProducts;
  }

  getAllProductsByKey($event: any) {
    let value = $event.name;
    this._product.getAllProductsByKey(value).subscribe((res) => {
      this.getAllProductsData = res.data;
    });
    if (value == "" || value == " ") {
      this.getAllProductsData = this.reservedProducts;
    }
  }

  changePageSize() {
    this.pageSizeChange.emit(this.pageSize);
  }

  ddAction(action: string, data: Product) {
    const actionModel: DDActionModel = {
      action: action,
      data: data
    }
    this.dropdownAction.emit(actionModel);
  }

  onChange(event: any) {
    this.onSelected.emit(this.productIds);
  }

  selectEvent(item: any) {
    this.getAllProductsByKey(item);
  }

  onChangeSearch(val: string) {
  }

  onFocused(e: any) {
  }

  onInputCleared() {
    this.getAllProductsData = this.reservedProducts;
  }
}
