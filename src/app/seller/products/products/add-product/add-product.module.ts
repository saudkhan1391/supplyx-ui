import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddProductRoutingModule } from './add-product-routing.module';
import { AddProductComponent } from './add-product.component';
import { NgWizardModule, NgWizardConfig, THEME } from 'ng-wizard';
import { ButtonModule } from 'src/app/lib/button/button.module';
import { FileUploaderModule } from 'src/app/lib/file-uploader/file-uploader.module';
import { UiModule } from '../ui/ui.module';

const ngWizardConfig: NgWizardConfig = {
  theme: THEME.dots
};


@NgModule({
  declarations: [AddProductComponent],
  imports: [
    NgWizardModule.forRoot(ngWizardConfig),
    CommonModule,
    AddProductRoutingModule,
    UiModule
  ]
})
export class AddProductModule { }
