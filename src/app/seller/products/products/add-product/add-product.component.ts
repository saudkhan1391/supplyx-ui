import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { AddProductStore } from 'src/app/state/add-product/add-product.store';
import { AddProductQuery } from 'src/app/state/add-product/add-product.query';
import { Product } from 'src/app/shared/models/product/product.model';
import { map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { CategoriesStore } from 'src/app/state/categories/categories.store';
import { CategoriesQuery } from 'src/app/state/categories/categories.query';
import { Category } from 'src/app/shared/models/category/category.model';
import { CategoryService } from 'src/app/shared/services/api/category.service';
import { ProductFormComponent } from '../ui/product-form/product-form.component';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit, OnDestroy {

  unSubscribe$: Subject<void> = new Subject<void>();
  addProduct$: Observable<Product>;
  product!: Product;
  categories: Category[] = [];
  isDirty: boolean = false;

  @ViewChild('productForm') productForm!: ProductFormComponent;

  constructor( private _store: AddProductStore,
    private _query: AddProductQuery,
    private _categoriesStore: CategoriesStore,
    private _categoriesQuery: CategoriesQuery,
    private _categoryService: CategoryService ) { 
      this.addProduct$ = this._query.addProduct$.pipe(
        tap( product => this.product =  { ...product}),
        takeUntil(this.unSubscribe$)
      );
      this.addProduct$.subscribe();

      this._categoriesQuery.categories$.pipe(
        switchMap( categoryList => categoryList && categoryList.length > 0 ? of(categoryList) : 
          this._categoryService.getCategories().pipe(
            tap( () => this._categoriesStore.update( state => ({
              ...state,
              categories: categoryList
            }))),
            map( apiRes => apiRes.data)
        )),
        tap( categoryList => {
          this.categories = categoryList;
        }),
        takeUntil(this.unSubscribe$)
      ).subscribe();
    }

  ngOnInit(): void { }

  onDirty( isDirty: boolean) {
    this.isDirty = isDirty;
  }

  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }

}
