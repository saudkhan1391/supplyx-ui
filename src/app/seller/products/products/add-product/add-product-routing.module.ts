import { AddProductComponent } from './add-product.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductChangesGuardService } from '../services/product-changes.gaurd';

const routes: Routes = [
  { path: '', component: AddProductComponent, canDeactivate: [ProductChangesGuardService]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddProductRoutingModule { }
