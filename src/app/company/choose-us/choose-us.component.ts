import { Component, Input, OnInit } from '@angular/core';
import { Company } from 'src/app/shared/models/company/company.model';

@Component({
  selector: 'app-choose-us',
  templateUrl: './choose-us.component.html',
  styleUrls: ['./choose-us.component.scss']
})
export class ChooseUsComponent implements OnInit {

  @Input() chooseUs!: string;
  @Input() company: Company = {} as Company
  constructor() { }

  ngOnInit(): void {
    
  }

 
}
