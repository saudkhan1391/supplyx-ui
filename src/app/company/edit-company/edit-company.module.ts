import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditCompanyRoutingModule } from './edit-company-routing.module';
import { EditCompanyComponent } from './edit-company.component';
import { ButtonModule } from 'src/app/lib/button/button.module';
import { FileUploaderModule } from 'src/app/lib/file-uploader/file-uploader.module';
import { TextEditorModule } from 'src/app/lib/text-editor/text-editor.module';
import { DocumentPreviewModule } from 'src/app/lib/document-preview/document-preview.module';


@NgModule({
  declarations: [EditCompanyComponent],
  imports: [
    CommonModule,
    EditCompanyRoutingModule,
    FormsModule,
    ReactiveFormsModule ,
    FileUploaderModule,
    ButtonModule,
    TextEditorModule,
    DocumentPreviewModule
  ]
})
export class EditCompanyModule { }
