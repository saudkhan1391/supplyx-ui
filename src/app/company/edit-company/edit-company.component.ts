
import { CompanyQuery } from './../../state/company/company.query';
import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, of, Subject } from 'rxjs';
import { User } from 'src/app/shared/models/auth/user.model';
import { switchMap, map, tap, takeUntil, catchError, } from 'rxjs/operators';
import { CompanyService } from 'src/app/shared/services/api/company.service';
import { Company } from 'src/app/shared/models/company/company.model';
import { UploaderResponse } from 'src/app/shared/models/files/uploader-response';
import { EditorChangeContent, EditorChangeSelection } from 'ngx-quill';
import { CompanyImage } from 'src/app/shared/models/product/image.model';
import { FileUploaderComponent } from 'src/app/lib/file-uploader/file-uploader.component';

@Component({
  selector: 'app-edit-company',
  templateUrl: './edit-company.component.html',
  styleUrls: ['./edit-company.component.scss']
})

export class EditCompanyComponent implements OnInit, OnDestroy {

  
  unSubscribe$: Subject<void> = new Subject<void>()
  companyForm!: FormGroup;
  user$: Observable<User> = of({} as User);
  company$?: Observable<Company> ;
  loading: boolean = false;
  disabled: boolean = false;
  fileName: string = '';
  uploadedImages: CompanyImage[] = [];
  aboutImage: CompanyImage[] = [];
  chooseImage: CompanyImage[] = [];
  companyId!: number
  company: Company = {} as Company;
  @Output() onDirty: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('galleryUploader') galleryUploader!: FileUploaderComponent;
  @ViewChild('bannerUploader') bannerUploader!: FileUploaderComponent;
  
  
  constructor(private fb: FormBuilder,
    private _companyService: CompanyService,
    private _query: CompanyQuery) { }

  
  ngOnInit(): void {
    this.createForm();
    this._query.company$.pipe(
      switchMap( company => company ? of(company) : this._companyService.getCompany().pipe(
        map( apiResponse => apiResponse.data)
      )),
      tap( company => {
        this.company = company;
        this.companyForm.patchValue(this.company)
      }),
      catchError( err => {
        throw err;
      }),
      takeUntil(this.unSubscribe$)
    ).subscribe( res => this.companyId = res.id); 
  }  

  createForm() {
    this.companyForm = this.fb.group({
      id: [ null ],
      about: ['', Validators.required ],
      whyChooseUs:  ['', Validators.required],
      website: ['', Validators.required ],
      mobile: ['', Validators.required ],
      email: ['', Validators.required ],
      phone: ['', Validators.required ],
      images: [this.company.images],
      aboutImage: [this.company.aboutImage],
      chooseusImage: [this.company.chooseusImage],
      youtubeChannel: ['']
    })
  }

  submitData(){
    this.getFinalImages();
    this.getAboutImages();
    this.getChooseImages();
    if(this.companyForm.valid) {
      this._companyService.editCompany(this.companyForm.value).pipe(
        tap( apiResponse => console.log(apiResponse)),
        catchError( err => {
          throw err;
        }),
        takeUntil(this.unSubscribe$)
      ).subscribe();
    }
  }

  loadImageFile(uploadedFile: UploaderResponse[]){
    this.uploadedImages = uploadedFile.map( uploadedFile => ({
      key: uploadedFile.key,
      url: uploadedFile.path,
      companyId: this.companyId
    } as CompanyImage));
    this.onDirty.emit(true)
  }

  loadAboutImage(uploadedFile: UploaderResponse[]) {
    if(this.company.aboutImage){
      this.company.aboutImage.splice(0, this.company.aboutImage.length)
    }
    this.aboutImage = uploadedFile.map( uploadedFile => ({
      key: uploadedFile.key,
      url: uploadedFile.path,
      companyId: this.companyId
    } as CompanyImage));
    this.onDirty.emit(true)
  }

  loadChooseImage(uploadedFile: UploaderResponse[]) {
    if( this.company.chooseusImage) {
      this.company.chooseusImage.splice(0, this.company.chooseusImage.length)
    }
    this.chooseImage = uploadedFile.map( uploadedFile => ({
      key: uploadedFile.key,
      url: uploadedFile.path,
      companyId: this.companyId
    } as CompanyImage))
    this.onDirty.emit(true)
  }

  onGalleryRemoved( removedUploadedFile: UploaderResponse) {
    this.company.images = this.company.images.filter( image => image.key !== removedUploadedFile.key);
    this.uploadedImages = this.uploadedImages.filter( image => image.key !== removedUploadedFile.key );
  }

  getFinalImages() {
   this.company.images = [...this.company.images, ...this.uploadedImages];
   this.companyForm.patchValue({
     images: this.company.images,
     bannerImage: this.company.bannerImage
   })
  }

  getAboutImages() {
    if(this.company.aboutImage){
      this.company.aboutImage.splice(0, this.company.aboutImage.length)
    }
    this.company.aboutImage = [...this.company.aboutImage, ...this.aboutImage]
    this.companyForm.patchValue({
      aboutImage: this.company.aboutImage
    })
  }

  getChooseImages() {
    if(this.company.chooseusImage){
      this.company.chooseusImage.splice(0, this.company.aboutImage.length)
    }
    this.company.chooseusImage = [...this.company.chooseusImage, ...this.chooseImage]
    this.companyForm.patchValue({
      chooseusImage: this.company.chooseusImage
    })
  }  

  clearUploaderData() {
    this.uploadedImages = [];
    this.galleryUploader.reset();
    this.onDirty.emit(false);
  }

  get about() {
    return this.companyForm.get('about');
  }
  
  get whyChooseUs(){
    return this.companyForm.get('whyChooseUs');
  }

  get email(){
    return this.companyForm.get('email');
  }

  get website(){
    return this.companyForm.get('website');
  }


  get mobile(){
    return this.companyForm.get('mobile');
  }

  get phone(){
    return this.companyForm.get('phone');
  }

  get postalCode(){
    return this.companyForm.get('postalCode');
  }

  blured = false
  focused = false

  created(event: any) {
    // tslint:disable-next-line:no-console
    console.log('editor-created', event)
  }

  changedEditor(event: EditorChangeContent | EditorChangeSelection) {
    // tslint:disable-next-line:no-console
    console.log('editor-change', event)
  }

  focus($event: any) {
    // tslint:disable-next-line:no-console
    console.log('focus', $event)
    this.focused = true
    this.blured = false
  }

  blur($event: any) {
    // tslint:disable-next-line:no-console
    console.log('blur', $event)
    this.focused = false
    this.blured = true
  }

  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }

}
