import { CompanyComponent } from './company.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'profile/:activeTab', component: CompanyComponent},
  { path: 'edit-company', loadChildren: () => import('../company/edit-company/edit-company.module').then( m => m.EditCompanyModule)}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRoutingModule { }
