import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { PaginationResponse, PaginatorPlugin } from '@datorama/akita';
import { Observable, of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { Product } from 'src/app/shared/models/product/product.model';
import { ProductService } from 'src/app/shared/services/api/product.service';
import { PRODUCT_PAGINATOR_PROVIDER } from 'src/app/state/products/product.paginator';
export interface MenuItem{
  id: number,
  img: string,
  name: string,
  category: string,
  apartmentType: string,
  quantity: string
}

@Component({
  selector: 'app-company-products',
  templateUrl: './company-products.component.html',
  styleUrls: ['./company-products.component.scss']
})
export class CompanyProductsComponent implements OnInit, OnDestroy {

  paginationResponse!: PaginationResponse<Product>;
  pagination$: Observable<PaginationResponse<Product>> = of({} as PaginationResponse<Product>);

  perPage: number = 10;
  pageSizes: number[] = [5, 10, 20, 50];
  
  constructor(
    @Inject(PRODUCT_PAGINATOR_PROVIDER) public paginatorRef: PaginatorPlugin<Product>,
    private _productService: ProductService) { }

  products: MenuItem[] = [
    { 
      id:1,
      img: '../../../../assets/Products/Rectangle 521.jpg',
      name: 'Glass Window Lorem Ipsum Dolor Sit', 
      category: 'Door / Swing Door',
      apartmentType: 'Studio / Standard 2 Bedroom',
      quantity: '100'
    },
    { 
      id: 2,
      img: '../../../../assets/Products/Rectangle 525.jpg',
      name: '10 Lite French Interior Door in Wenge Finish', 
      category: 'Door / Swing Door',
      apartmentType: 'Studio / Deluxe',
      quantity: '100'
    },
    { 
      id: 3,
      img: '../../../../assets/Products/Rectangle 528.jpg',
      name: 'Lights, E26 Base Brushed Nickel Hanging', 
      category: 'Door / Swing Door',
      apartmentType: 'Standard 2 Bedroom',
      quantity: '100'
    },
    { 
      id: 3,
      img: '../../../../assets/Products/Rectangle 528.jpg',
      name: 'Lights, E26 Base Brushed Nickel Hanging', 
      category: 'Door / Swing Door',
      apartmentType: 'Standard 2 Bedroom',
      quantity: '100'
    },
  ]


  ngOnInit(): void {

    //Active Products
    this.pagination$ = this.paginatorRef.pageChanges.pipe(
      switchMap((page) => {
        const req = () => this._productService.fetchProducts({ page: page, take: this.perPage, status: 'active'} );
        return this.paginatorRef.getPage(req) as Observable<PaginationResponse<Product>>;
      })
    );

    this.pagination$.pipe(
      tap( pagination => {
        this.paginationResponse = pagination;
      })
    ).subscribe(); 
    
  }

  ngOnDestroy(): void {
    this.paginatorRef.destroy();
  }

}
