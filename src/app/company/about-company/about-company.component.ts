import { Company } from './../../shared/models/company/company.model';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-company',
  templateUrl: './about-company.component.html',
  styleUrls: ['./about-company.component.scss']
})
export class AboutCompanyComponent implements OnInit {

  @Input() aboutUs!: string;
  @Input() companyName!: string;
  @Input() company: Company = {} as Company
  constructor() { }

  ngOnInit(): void {
    
  }

}
