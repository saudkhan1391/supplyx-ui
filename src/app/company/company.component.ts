import { CompanyStore } from './../state/company/company.state';
import { catchError, takeUntil, tap } from 'rxjs/operators';
import { CompanyService } from 'src/app/shared/services/api/company.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { User } from '../shared/models/auth/user.model';
import { ActivatedRoute } from '@angular/router';
import { Observable, of, Subject } from 'rxjs';
import { Company } from '../shared/models/company/company.model';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit, OnDestroy {

  uid!: number;
  user$: Observable<User | null> = of({} as User);
  unSubscribe$: Subject<void> = new Subject<void>();
  user!: User;
  company: Company = {} as Company;
  activeTab: string = 'about-us';
  tabs: string[] = ['about-us', 'why-choose-us', 'gallery', 'contact-us']

  constructor( 
    private companyService: CompanyService,
    private _companyStore: CompanyStore,
    private _activatedRoute: ActivatedRoute
  ) { 
    this._activatedRoute.params.pipe(
      tap( params => {
        this.activeTab = params.activeTab;
      }),
      takeUntil(this.unSubscribe$)
    ).subscribe();
  }

  ngOnInit(): void {
    this.companyService.getCompany().pipe(
      tap((res) => {
        this.company = res.data
        this.updateCompanyStore();
      }),
      catchError( err => {
        throw err;
      }),
      takeUntil(this.unSubscribe$)
    ).subscribe();
  } 
  
  updateCompanyStore() {
    this._companyStore.update( state => ({
      ...state,
      company: this.company
    }));
  }

  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }    

}
