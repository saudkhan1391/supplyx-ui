import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyRoutingModule } from './company-routing.module';
import { CompanyComponent } from './company.component';
import { AboutCompanyComponent } from './about-company/about-company.component';
import { ChooseUsComponent } from './choose-us/choose-us.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ImagesComponent } from './images/images.component';
import { VideosComponent } from './videos/videos.component';
import { CompanyProductsComponent } from './company-products/company-products.component';
import { PRODUCT_PAGINATOR_FACTORY, PRODUCT_PAGINATOR_PROVIDER } from '../state/products/product.paginator';
import { QuillModule } from 'ngx-quill';



@NgModule({
  declarations: [CompanyComponent, AboutCompanyComponent, ChooseUsComponent, GalleryComponent, ContactUsComponent, ImagesComponent, VideosComponent, CompanyProductsComponent],
  imports: [
    CommonModule,
    CompanyRoutingModule,
    QuillModule.forRoot(),
  ],
  providers: [
    { provide: PRODUCT_PAGINATOR_PROVIDER, useFactory: PRODUCT_PAGINATOR_FACTORY }
  ]
})
export class CompanyModule { }
