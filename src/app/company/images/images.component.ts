import { Component, Input, OnInit } from '@angular/core';
import { Company } from 'src/app/shared/models/company/company.model';
import { CompanyImage } from 'src/app/shared/models/product/image.model';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.scss']
})
export class ImagesComponent implements OnInit {

  constructor() { }

  _company: Company = {} as Company;
  @Input() set company (company:Company) {
    this._company = company;
    this.getImages(this._company); 
  }
  uploadedImages: CompanyImage[] = [];

  ngOnInit(): void {
   
    
  }

  getImages(company: Company) {
    this.uploadedImages = company.images?.filter( res => res.url.includes("jpg"))
  }
}
