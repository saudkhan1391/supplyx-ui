import { Component, Input, OnInit } from '@angular/core';
import { Company } from 'src/app/shared/models/company/company.model';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  constructor() { }

  @Input() company: Company = {} as Company

  ngOnInit(): void {
  }

}
