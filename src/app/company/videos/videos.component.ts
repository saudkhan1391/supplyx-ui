import { Component, Input, OnInit } from '@angular/core';
import { Company } from 'src/app/shared/models/company/company.model';
import { CompanyImage } from 'src/app/shared/models/product/image.model';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit {

  constructor() { }

  _company: Company = {} as Company;
  @Input() set company (company:Company) {
    this._company = company;
    this.getVideos(this._company); 
  }

  uploadedVideos: CompanyImage[] = [];
  
  ngOnInit(): void {
  }

  getVideos(company: Company) {
    this.uploadedVideos = company.images?.filter( res => res.url.includes("mp4"))
  }

}
