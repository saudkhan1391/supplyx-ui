import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {


  @Input() email!: string;
  @Input() phone!: number;
  @Input() mobile!:number;
  @Input() companyWebsite!: string;
  @Input() youtubeChannel!: string;
  constructor() { }

  ngOnInit(): void {
  }

}
