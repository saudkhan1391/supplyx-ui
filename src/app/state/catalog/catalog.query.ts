import { Injectable } from "@angular/core";
import { QueryEntity } from "@datorama/akita";
import { Catalog } from "src/app/shared/models/catalog/catalog.model";
import { CatalogPaginationStore } from "./catalog.store";

@Injectable({
    providedIn: 'root'
})
export class CatalogPaginationQuery extends QueryEntity<Catalog, number> {

    constructor(protected _store:  CatalogPaginationStore) {
        super(_store);
    }

}