import { inject, InjectionToken } from '@angular/core';
import { PaginatorPlugin } from '@datorama/akita';
import { CatalogPaginationQuery } from './catalog.query';

export const CATALOG_PAGINATOR_FACTORY = () => {
  const catalogPaginationQuery = inject(CatalogPaginationQuery);
  return new PaginatorPlugin(catalogPaginationQuery).withControls().withRange();
};

export const CATALOG_PAGINATOR_PROVIDER = new InjectionToken('CATALOG_PAGINATOR');