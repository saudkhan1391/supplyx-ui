import { Injectable } from "@angular/core";
import { EntityState, EntityStore, ID, StoreConfig } from "@datorama/akita";
import { Catalog } from "src/app/shared/models/catalog/catalog.model";

export type ProductPaginationState = EntityState<Catalog, ID>;

@Injectable({
    providedIn: 'root'
})
@StoreConfig({ name: 'catalog-pagination-store' })
export class CatalogPaginationStore extends EntityStore<Catalog> {
    constructor() {
        super();
    }
}