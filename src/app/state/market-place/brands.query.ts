import { Injectable } from "@angular/core";
import { Query } from "@datorama/akita";
import { Observable } from "rxjs";
import { BrandsState, BrandsStore } from "./brands.store";

@Injectable({
    providedIn: 'root'
})
export class BrandsQuery extends Query<BrandsState> {

    brands$: Observable<any[]> = this.select( (state) => state.brands );
    loading$: Observable<boolean> = this.select( (state) => state.loading);

    constructor(protected _store: BrandsStore) {
        super(_store);
    }

}
