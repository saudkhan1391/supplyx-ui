import { Injectable } from "@angular/core";
import { Store, StoreConfig } from "@datorama/akita";


export interface BrandsState {
  brands: any[],
  error: string | null,
  loading: boolean
}

export const initialBrandsState = () => ({
  brands: [] as any[],
  error: null,
  loading: false
})

@Injectable({
  providedIn: 'root'
})
@StoreConfig({ name: 'create-video-store' })
export class BrandsStore extends Store<BrandsState> {
  constructor() {
    super(initialBrandsState());
  }
}
