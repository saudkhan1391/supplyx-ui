import { Injectable } from '@angular/core';
import { forkJoin, Observable, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { BrandsQuery } from './brands.query';
import { BrandsStore } from './brands.store';

@Injectable({
  providedIn: 'root'
})
export class BrandsService {

  constructor(
    private _store: BrandsStore,
    private _query: BrandsQuery,
  ) { }

  isLoading(): Observable<boolean> {
    return this._query.loading$;
  }

  getBrands(): Observable<any[]> {
    return this._query.brands$;
  }

  setBrands(brands: any[]) {
    this._store.update(state => ({
      ...state,
      brands: brands
    }))
  }
}
