import { Injectable } from "@angular/core";
import { Store, StoreConfig } from "@datorama/akita";
import { Project } from "src/app/shared/models/projects/project.model";


export interface SelectedProjectState {
    project: Project,
}

export const initialSelectedProjectState = () => ({
    project: {} as Project
})

@Injectable({
    providedIn: 'root'
})
@StoreConfig({ name: 'selected-project-store' })
export class SelectedProjectStore extends Store<SelectedProjectState> {
    constructor() {
        super(initialSelectedProjectState());
    }
}