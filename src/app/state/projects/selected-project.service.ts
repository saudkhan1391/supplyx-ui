import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from 'src/app/shared/models/projects/project.model';
import { SelectedProjectQuery } from './selected-project.query';
import { SelectedProjectStore } from './selected-project.store';

@Injectable({
  providedIn: 'root'
})
export class SelectedProjectService {

  constructor(
    private _store: SelectedProjectStore,
    private _query: SelectedProjectQuery
  ) { }

  getSelectedProject(): Observable<Project> {
    return this._query.project$;
  }

  updateProject(project: Project) {
    this._store.update( state => ({
      ...state,
      project: project
    }))
  }
}
