import { inject, InjectionToken } from '@angular/core';
import { PaginatorPlugin } from '@datorama/akita';
import { ProjectPaginationQuery } from './project.query';

export const PROJECT_PAGINATOR_FACTORY = () => {
  const projectPaginationQuery = inject(ProjectPaginationQuery);
  return new PaginatorPlugin(projectPaginationQuery).withControls().withRange();
};

export const PROJECT_PAGINATOR_PROVIDER = new InjectionToken('PROJECT_PAGINATOR');