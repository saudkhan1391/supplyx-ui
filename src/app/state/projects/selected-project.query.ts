import { Injectable } from "@angular/core";
import { Query } from "@datorama/akita";
import { Observable } from "rxjs";
import { Project } from "src/app/shared/models/projects/project.model";
import { SelectedProjectState, SelectedProjectStore } from "./selected-project.store";


@Injectable({
    providedIn: 'root'
})
export class SelectedProjectQuery extends Query<SelectedProjectState> {

    project$: Observable<Project> = this.select('project');
   

    constructor(protected _store: SelectedProjectStore) {
        super(_store);
    }

}