import { Injectable } from "@angular/core";
import { EntityState, EntityStore, ID, StoreConfig } from "@datorama/akita";
import { Product } from "src/app/shared/models/product/product.model";

export type ProductPaginationState = EntityState<Product, ID>;

@Injectable({
    providedIn: 'root'
})
@StoreConfig({ name: 'deleted-product-pagination-store' })
export class DeletedProductPaginationStore extends EntityStore<Product> {
    constructor() {
        super();
    }
}