import { inject, InjectionToken } from '@angular/core';
import { PaginatorPlugin } from '@datorama/akita';
import { InactiveProductPaginationQuery } from './inactive-product.query';

export const INACTIVE_PRODUCT_PAGINATOR_FACTORY = () => {
  const productPaginationQuery = inject(InactiveProductPaginationQuery);
  return new PaginatorPlugin(productPaginationQuery).withControls().withRange();
};

export const INACTIVE_PRODUCT_PAGINATOR_PROVIDER = new InjectionToken('INACTIVE_PRODUCT_PAGINATOR');