import { Injectable } from "@angular/core";
import { QueryEntity } from "@datorama/akita";
import { Observable } from "rxjs";
import { Product } from "src/app/shared/models/product/product.model";
import { CartPaginationStore, CartState } from "./cart.store";
import { ProductPaginationStore, ProductsState } from "./product.store";

@Injectable({
    providedIn: 'root'
})

export class CartPaginationQuery extends QueryEntity<CartState, number> {

  cart$: Observable<any[]> = this.select( (state) => state.cart );
  reservedProducts$: Observable<any[]> = this.select( (state) => state.reservedProducts );

    constructor(protected _store: CartPaginationStore) {
        super(_store);
    }

}
