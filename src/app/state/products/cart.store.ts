import { Injectable } from "@angular/core";
import { EntityState, EntityStore, ID, StoreConfig } from "@datorama/akita";
import { Cart } from "src/app/shared/models/cart/cart.model";
import { Product } from "src/app/shared/models/product/product.model";

export type ProductPaginationState = EntityState<Product, ID>;


export interface CartState {
  cart: Cart[],
  reservedProducts: Product[],
  error: string | null,
  loading: boolean
}

export const initialProductsState = () => ({
  cart: [] as Cart[],
  error: null,
  loading: false
})


@Injectable({
    providedIn: 'root'
})
@StoreConfig({ name: 'product-pagination-store' })
export class CartPaginationStore extends EntityStore<CartState> {
    constructor() {
        super();
    }
}
