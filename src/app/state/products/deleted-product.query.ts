import { Injectable } from "@angular/core";
import { QueryEntity } from "@datorama/akita";
import { Product } from "src/app/shared/models/product/product.model";
import { DeletedProductPaginationStore } from "./deleted-product.store";

@Injectable({
    providedIn: 'root'
})
export class DeletedProductPaginationQuery extends QueryEntity<Product, number> {

    constructor(protected _store: DeletedProductPaginationStore) {
        super(_store);
    }

}