import { inject, InjectionToken } from '@angular/core';
import { PaginatorPlugin } from '@datorama/akita';
import { ProductPaginationQuery } from './product.query';

export const PRODUCT_PAGINATOR_FACTORY = () => {
  const productPaginationQuery = inject(ProductPaginationQuery);
  return new PaginatorPlugin(productPaginationQuery).withControls().withRange();
};

export const PRODUCT_PAGINATOR_PROVIDER = new InjectionToken('PRODUCT_PAGINATOR');