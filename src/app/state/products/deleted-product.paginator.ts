import { inject, InjectionToken } from '@angular/core';
import { PaginatorPlugin } from '@datorama/akita';
import { DeletedProductPaginationQuery } from './deleted-product.query';

export const DELETED_PRODUCT_PAGINATOR_FACTORY = () => {
  const productPaginationQuery = inject(DeletedProductPaginationQuery);
  return new PaginatorPlugin(productPaginationQuery).withControls().withRange();
};

export const DELETED_PRODUCT_PAGINATOR_PROVIDER = new InjectionToken('DELETED_PRODUCT_PAGINATOR');
