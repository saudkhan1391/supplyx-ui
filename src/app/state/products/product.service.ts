import { Injectable } from '@angular/core';
import { forkJoin, Observable, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { Product } from 'src/app/shared/models/product/product.model';
import { ProductPaginationQuery } from './product.query';
import { ProductPaginationStore } from './product.store';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(
    private _store: ProductPaginationStore,
    private _query: ProductPaginationQuery,
  ) { }


  getProducts(): Observable<Product[]> {
    return this._query.products$;
  }

  getReservedProducts(): Observable<Product[]>{
    return this._query.reservedProducts$;
  }

  setProducts(products: Product[]) {
    this._store.update(state => ({
      ...state,
      products: products
    }))
    // this._store.update(state => ({
    //   ...state,
    //   reservedProducts: products
    // }))
  }
}
