import { DraftProductPaginationQuery } from './draft-product.query';
import { inject, InjectionToken } from '@angular/core';
import { PaginatorPlugin } from '@datorama/akita';

export const DRAFT_PRODUCT_PAGINATOR_FACTORY = () => {
  const draftproductPaginationQuery = inject(DraftProductPaginationQuery);
  return new PaginatorPlugin(draftproductPaginationQuery).withControls().withRange();
};

export const DRAT_PRODUCT_PAGINATOR_PROVIDER = new InjectionToken('DRAFT_PRODUCT_PAGINATOR');