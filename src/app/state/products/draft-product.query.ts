import { Injectable } from "@angular/core";
import { QueryEntity } from "@datorama/akita";
import { Product } from "src/app/shared/models/product/product.model";
import { DraftProductPaginationStore } from "./draft-product.store";

@Injectable({
    providedIn: 'root'
})
export class DraftProductPaginationQuery extends QueryEntity<Product, number> {

    constructor(protected _store:  DraftProductPaginationStore) {
        super(_store);
    }

}