import { Injectable } from "@angular/core";
import { EntityState, EntityStore, ID, StoreConfig } from "@datorama/akita";
import { Product } from "src/app/shared/models/product/product.model";

export type ProductPaginationState = EntityState<Product, ID>;

@Injectable({
    providedIn: 'root'
})
@StoreConfig({ name: 'draft-product-pagination-store' })
export class DraftProductPaginationStore extends EntityStore<Product> {
    constructor() {
        super();
    }
}