import { Injectable } from '@angular/core';
import { forkJoin, Observable, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { Cart } from 'src/app/shared/models/cart/cart.model';
import { Product } from 'src/app/shared/models/product/product.model';
import { CartPaginationQuery } from './cart.query';
import { CartPaginationStore } from './cart.store';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(
    private _store: CartPaginationStore,
    private _query: CartPaginationQuery,
  ) { }


  getCartItems(): Observable<Cart[]> {
    return this._query.cart$;
  }

  getReservedProducts(): Observable<Product[]>{
    return this._query.reservedProducts$;
  }

  setConfirmOrderItems(cart: Cart[]) {
    this._store.update(state => ({
      ...state,
      cart: cart
    }))
  }
}
