import { Injectable } from "@angular/core";
import { QueryEntity } from "@datorama/akita";
import { Product } from "src/app/shared/models/product/product.model";
import { InactiveProductPaginationStore } from "./inactive-product.store";

@Injectable({
    providedIn: 'root'
})
export class InactiveProductPaginationQuery extends QueryEntity<Product, number> {

    constructor(protected _store:  InactiveProductPaginationStore) {
        super(_store);
    }

}