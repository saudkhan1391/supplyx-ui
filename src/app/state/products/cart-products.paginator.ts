import { inject, InjectionToken } from '@angular/core';
import { PaginatorPlugin } from '@datorama/akita';
import { CartPaginationQuery } from './cart.query';
// import { ProductPaginationQuery } from './product.query';

export const CART_PAGINATOR_FACTORY = () => {
  const productPaginationQuery = inject(CartPaginationQuery);
  return new PaginatorPlugin(productPaginationQuery).withControls().withRange();
};

export const CART_PAGINATOR_PROVIDER = new InjectionToken('CART_PAGINATOR');
