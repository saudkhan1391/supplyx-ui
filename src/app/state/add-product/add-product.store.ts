import { Injectable } from "@angular/core";
import { Store, StoreConfig } from "@datorama/akita";
import { Product } from "src/app/shared/models/product/product.model";


export interface AddProductState {
    product: Product,
}

export const initialAddProductState = () => ({
    product: {
        productName: 'Product Test',
        minimumQuantity: 0,
        availableQuantity: 0,
        images: [],
        specifications: [],
        status: 'draft'
    } as unknown as Product
})

@Injectable({
    providedIn: 'root'
})
@StoreConfig({ name: 'add-product-store' })
export class AddProductStore extends Store<AddProductState> {
    constructor() {
        super(initialAddProductState());
    }
}