import { Injectable } from "@angular/core";
import { Query } from "@datorama/akita";
import { Observable } from "rxjs";
import { Product } from "src/app/shared/models/product/product.model";
import { AddProductState, AddProductStore } from "./add-product.store";

@Injectable({
    providedIn: 'root'
})
export class AddProductQuery extends Query<AddProductState> {

    addProduct$: Observable<Product> = this.select('product');

    constructor(protected _store: AddProductStore) {
        super(_store);
    }

}