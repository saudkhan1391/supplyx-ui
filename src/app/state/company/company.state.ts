import { Injectable } from "@angular/core";
import { Store, StoreConfig } from "@datorama/akita";
import { Company } from "src/app/shared/models/company/company.model";


export interface CompanyState {
    company: Company | null,
}

export const initialSessionState = () => ({
    company: null
})

@Injectable({
    providedIn: 'root'
})
@StoreConfig({ name: 'session-store' })
export class CompanyStore extends Store<CompanyState> {
    constructor() {
        super(initialSessionState());
    }
}