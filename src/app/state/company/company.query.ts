import { Injectable } from "@angular/core";
import { Query } from "@datorama/akita";
import { Observable } from "rxjs";
import { Company } from "src/app/shared/models/company/company.model";
import { CompanyState, CompanyStore } from "./company.state";


@Injectable({
    providedIn: 'root'
})
export class CompanyQuery extends Query<CompanyState> {

    company$: Observable<Company | null> = this.select('company');
   

    constructor(protected _store: CompanyStore) {
        super(_store);
    }

}