import { Injectable } from "@angular/core";
import { Store, StoreConfig } from "@datorama/akita";
import { Category } from "src/app/shared/models/category/category.model";


export interface CatagoriesState {
    categories: Category[],
}

export const initialCategoriesState = () => ({
    categories: []
})

@Injectable({
    providedIn: 'root'
})
@StoreConfig({ name: 'categories-store' })
export class CategoriesStore extends Store<CatagoriesState> {
    constructor() {
        super(initialCategoriesState());
    }
}