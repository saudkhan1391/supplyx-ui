import { Injectable } from "@angular/core";
import { Query } from "@datorama/akita";
import { Observable } from "rxjs";
import { Category } from "src/app/shared/models/category/category.model";
import { CatagoriesState, CategoriesStore } from "./categories.store";

@Injectable({
    providedIn: 'root'
})
export class CategoriesQuery extends Query<CatagoriesState> {

    categories$: Observable<Category[]> = this.select('categories');

    constructor(protected _store: CategoriesStore) {
        super(_store);
    }

}