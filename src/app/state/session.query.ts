import { Injectable } from "@angular/core";
import { Query } from "@datorama/akita";
import { Observable } from "rxjs";
import { User } from "../shared/models/auth/user.model";
import { Catalog } from "../shared/models/catalog/catalog.model";
import { SessionState, SessionStore } from "./session.state";

@Injectable({
    providedIn: 'root'
})
export class SessionQuery extends Query<SessionState> {

    user$: Observable<User | null> = this.select('user');
    role$: Observable<string | null> = this.select('role');
    editCatalog$: Observable<Catalog> = this.select('editCatalog');

    constructor(protected _store: SessionStore) {
        super(_store);
    }

}