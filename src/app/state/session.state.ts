import { Injectable } from "@angular/core";
import { Store, StoreConfig } from "@datorama/akita";
import { User } from "../shared/models/auth/user.model";
import { Catalog } from "../shared/models/catalog/catalog.model";


export interface SessionState {
    isLoggedIn: boolean,
    user: User | null,
    role: string | null,
    editCatalog: Catalog
}

export const initialSessionState = () => ({
    isLoggedIn: false,
    user: null,
    role: null,
    editCatalog: {} as Catalog
})

@Injectable({
    providedIn: 'root'
})
@StoreConfig({ name: 'session-store' })
export class SessionStore extends Store<SessionState> {
    constructor() {
        super(initialSessionState());
    }
}