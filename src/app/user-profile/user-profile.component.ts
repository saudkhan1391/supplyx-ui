import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { User } from '../shared/models/auth/user.model';
import { SessionQuery } from '../state/session.query';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ChangeEmailComponent } from './change-email/change-email.component';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {


  user$!: Observable<User>;
  _user: User = {} as User

  constructor(private _query: SessionQuery, private dialog: MatDialog) { }
  PasswordDialog() {
    this.dialog.open(ChangePasswordComponent).afterClosed().subscribe(res => {
      console.log("Res 1 ", res);
    });
  }
  EmailDialog() {
    this.dialog.open(ChangeEmailComponent).afterClosed().subscribe(res => {
      console.log("Res 2 ", res);
      this._user.email = res;
    });
  }

  ngOnInit(): void {
    this._user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')!) : null;
  }

}
