import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-change-email',
  templateUrl: './change-email.component.html',
  styleUrls: ['./change-email.component.scss']
})
export class ChangeEmailComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<ChangeEmailComponent>, private dialog: MatDialog) { }
  email: string = "";

  CancelAllData() {
    this.dialog.closeAll();
  }
  ngOnInit(): void {
  }

  closeDialog() {
    this.dialogRef.close(this.email);
  }

  emailOnChange($event: any) {
    this.email = $event.target.value;
    console.log("this email : ", this.email);
    // console.log("this email : ", $event.target.value);
  }

}
