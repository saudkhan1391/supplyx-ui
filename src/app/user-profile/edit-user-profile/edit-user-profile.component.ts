import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ICountry, IState } from 'country-state-city/dist/lib/interface';
import { from, Observable, Subject } from 'rxjs';
import { User } from 'src/app/shared/models/auth/user.model';
import { FileUploaderComponent } from 'src/app/lib/file-uploader/file-uploader.component';
import { AuthService } from 'src/app/shared/services/api/auth.service';
import { Country, State } from 'country-state-city';
import { catchError, takeUntil, tap, map } from 'rxjs/operators';
import { UploaderResponse } from 'src/app/shared/models/files/uploader-response';
import { UserAttachment } from 'src/app/shared/models/product/image.model';
import { LocalStorageService } from 'src/app/shared/services/storage/local-storage.service';
import { ChangePasswordComponent } from '../change-password/change-password.component';
import { ChangeEmailComponent } from '../change-email/change-email.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-edit-user-profile',
  templateUrl: './edit-user-profile.component.html',
  styleUrls: ['./edit-user-profile.component.scss']
})
export class EditUserProfileComponent implements OnInit {

  sellerForm!: FormGroup;
  user$!: Observable<User>;
  _user: User = {} as User;
  unSubscribe$: Subject<void> = new Subject<void>()
  countries: ICountry[] = [];
  selectedCountry!: ICountry;
  countryCode!: string;
  states: IState[] = [];
  uploadedImages = new Array<UserAttachment>(1);
  pCode!: string;
  @ViewChild('imageUploader') imageUploader!: FileUploaderComponent;
  @Output() onDirty: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private fb: FormBuilder,
    private dialog: MatDialog,
    private _auth: AuthService, private _storage: LocalStorageService) { }

  ngOnInit(): void {
    from(Country.getAllCountries()).pipe(
      map(res => this.countries.push(res))
    ).subscribe()

    this.createForm();
    this.country?.valueChanges.pipe(
      tap(res => {
        this.selectedCountry = res;
        this.countryCode = this.selectedCountry?.isoCode;
        this.pCode = this.selectedCountry?.phonecode;
        this.sellerForm.patchValue({
          phoneCode: this.selectedCountry?.phonecode
        })
        this.getStates(this.countryCode);
      })
    ).subscribe()

    this._user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')!) : null;
    this.sellerForm.patchValue(this._user);
  }

  getStates(code: string) {
    if (this.states.length > 0) {
      this.states.splice(0, this.states.length)
    }
    from(State.getStatesOfCountry(code)).pipe(
      map(res => { this.states.push(res) })
    ).subscribe()
  }

  createForm() {
    this.sellerForm = this.fb.group({
      id: [this._user.id],
      firstName: [this._user.firstName, Validators.required],
      lastName: [this._user.lastName, Validators.required],
      mobile: [this._user.mobile, Validators.required],
      phone: [this._user.phone, Validators.required],
      //phoneCode: [ '' , Validators.required],
      street: [this._user.street, Validators.required],
      state: [this._user.street, Validators.required],
      country: [this._user.country, Validators.required],
      zipCode: [this._user.zipCode, Validators.required],
      roleType: [this._user.roleType],
      images: [this._user.images],
      email:[this._user.email],
      password:[this._user.password]
    })
    console.log(this.sellerForm.value)
  }

  submitData() {
    if (this.sellerForm.valid) {
      console.log('Before Patch', this.sellerForm.value)
      this.getProfileImages();
      // cloning object to convert zipCode to numerical value
      let user: User = {
        ...this.sellerForm.value,
        zipCode: +this.sellerForm.value.zipCode,
        street: this.sellerForm.value.street,
        country: this.sellerForm.value.country.name,
        state: this.sellerForm.value.state.name,
        phone: this.sellerForm.value.phone,
        mobile: this.sellerForm.value.mobile,
        roleType: this._user.roleType,
        email: this._user.email,
        password: this._user.password,
        //phoneCode: this.selectedCountry.phonecode
      }
      this.sellerForm.patchValue(user)
      this._auth.updateUser(this.sellerForm.value).pipe(
        catchError(err => {
          throw err
        }),
        takeUntil(this.unSubscribe$)
      ).subscribe();
      this._storage.setSession(user);
      console.log('AfterPatch', this.sellerForm.value)
    }
  }

  loadUserImage(uploadedFile: UploaderResponse[]) {
    if (this._user.images) {
      this._user.images.splice(0)
    }
    this.uploadedImages = uploadedFile.map(uploadedFile => ({
      key: uploadedFile.key,
      url: uploadedFile.path,
      userId: this._user.id
    } as UserAttachment));
    this.onDirty.emit(true);
  }

  getProfileImages() {
    console.log(this._user)
    this._user.images = [...this._user.images, ...this.uploadedImages];
    this.sellerForm.patchValue({
      images: this._user.images
    })

  }

  get firstName() {
    return this.sellerForm.get('firstName')
  }

  get lastName() {
    return this.sellerForm.get('lastName')
  }

  get roleType() {
    return this.sellerForm.get('roleType')
  }

  get mobile() {
    return this.sellerForm.get('mobile');
  }

  get phone() {
    return this.sellerForm.get('phone');
  }

  get street() {
    return this.sellerForm.get('street')
  }

  get state() {
    return this.sellerForm.get('state')
  }

  get country() {
    return this.sellerForm.get('country')
  }

  get zipCode() {
    return this.sellerForm.get('zipCode')
  }

  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }

  EmailDialog() {
    this.dialog.open(ChangeEmailComponent).afterClosed().subscribe((res: any) => {
      console.log("Res 2 ", res);
      this._user.email = res;
      this.sellerForm.patchValue({
        email: res
      })
    });
  }

  PasswordDialog() {
    this.dialog.open(ChangePasswordComponent).afterClosed().subscribe((res: any) => {
      console.log("Res 1 ", res);
      this._user.password = res;
      this.sellerForm.patchValue({
        password: res
      })
    });
  }

}
