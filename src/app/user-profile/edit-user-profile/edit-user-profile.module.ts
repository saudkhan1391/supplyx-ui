import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditUserProfileRoutingModule } from './edit-user-profile-routing.module';
import { EditUserProfileComponent } from './edit-user-profile.component';
import { FileUploaderModule } from 'src/app/lib/file-uploader/file-uploader.module';


@NgModule({
  declarations: [
    EditUserProfileComponent
  ],
  imports: [
    CommonModule,
    EditUserProfileRoutingModule,
    FormsModule,
    ReactiveFormsModule,FileUploaderModule
  ]
})
export class EditUserProfileModule { }
