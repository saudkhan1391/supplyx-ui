import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  constructor(private dialog: MatDialog, private dialogRef: MatDialogRef<ChangePasswordComponent>) { }
  password: string = "";
  
  CancelAllData() {
    this.dialog.closeAll();
  }

  ngOnInit(): void {
  }

  closeDialog() {
    this.dialogRef.close(this.password);
  }

  passwordOnChange($event: any) {
    this.password = $event.target.value;
  }
}
