import { EditUserProfileModule } from './edit-user-profile/edit-user-profile.module';
import { UserProfileComponent } from './user-profile.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: UserProfileComponent
  },
  {
    path: 'edit-profile', loadChildren: () => import('../user-profile//edit-user-profile/edit-user-profile.module').then( m => m. EditUserProfileModule)
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserProfileRoutingModule { }
