import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatDialogModule} from '@angular/material/dialog';


import { UserProfileRoutingModule } from './user-profile-routing.module';
import { UserProfileComponent } from './user-profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ChangeEmailComponent } from './change-email/change-email.component';


@NgModule({
  declarations: [
    UserProfileComponent,
    ChangePasswordComponent,
    ChangeEmailComponent
  ],
  imports: [
    CommonModule,
    UserProfileRoutingModule,
    MatDialogModule
  ]
})
export class UserProfileModule { }
