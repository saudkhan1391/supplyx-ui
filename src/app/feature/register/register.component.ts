
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators,} from '@angular/forms';
import {  PasswordValidator} from '../../../app/shared/Custom-Validation/custom-validation'
import { of, Subject } from 'rxjs';
import { AuthService } from 'src/app/shared/services/api/auth.service';
import { catchError, switchMap, takeUntil, takeWhile, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { CompanyService } from 'src/app/shared/services/api/company.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {

  unSubscribe$: Subject<void> = new Subject<void>()
  registerForm: any;
  loading: boolean = false;
  disabled: boolean = false;
  apiError: string | undefined = undefined;
  showForm: boolean = true;
  cName: any;


  constructor(private fb: FormBuilder, 
    private _auth: AuthService,
    private _router: Router,
    private company:CompanyService,) { }

  createForm() {
    this.registerForm = this.fb.group({
      firstName: ['',[Validators.required]],
      lastName: ['',[Validators.required]],
      email: ['',[Validators.required]],
      password: ['', [Validators.required]],
      confirmPassword: ['',[Validators.required,PasswordValidator]],
      companyName: ['',[Validators.required]],
      roleType: ['2', [Validators.required]]
    }, { validators: PasswordValidator})
  }

  submitData(){
    this.clearError();
    if(this.registerForm.valid) {
      this.setButtonStatus(true);
      this._auth.signup(this.registerForm.value).pipe(
        tap( () => {
          this.setButtonStatus(false)
          this.showForm = false;
        }),
        catchError( err => {
          this.setButtonStatus(false)
          throw err;
        }),
        takeUntil(this.unSubscribe$)
      ).subscribe();
    } else {
      console.log("Form is invalid !!!!!");
    }
  }

  ngOnInit(): void {
    this.createForm();
  }

  get firstName() {
    return this.registerForm.get('firstName')
  }

  get lastName() {
    return this.registerForm.get('lastName')
  }

  get email() {
    return this.registerForm.get('email')
  }

  get password() {
    return this.registerForm.get('password')
  }

  get  confirmPassword() {
    return this.registerForm.get('confirmPassword')
  }

  get  companyName() {
    return this.registerForm.get('companyName')
  }

  setButtonStatus( status: boolean) {
    this.loading = status;
    this.disabled = status;
  }

  parseError(err: HttpErrorResponse) {
    this.apiError = err.error.message;
  }

  clearError() {
    this.apiError = undefined;
  }

  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }


}
