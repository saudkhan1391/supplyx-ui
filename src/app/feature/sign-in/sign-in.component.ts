import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { of, Subject } from 'rxjs';
import { catchError, filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { ApiResponse } from 'src/app/shared/models/auth/api-response.model';
import { User } from 'src/app/shared/models/auth/user.model';
import { AuthService } from 'src/app/shared/services/api/auth.service';
import { LocalStorageService } from 'src/app/shared/services/storage/local-storage.service';
import { SessionQuery } from 'src/app/state/session.query';
import { SessionStore } from 'src/app/state/session.state';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit, OnDestroy {

  signinForm: any;
  unSubscribe$: Subject<void> = new Subject<void>();
  loading: boolean = false;
  disabled: boolean = false;
  apiError: string | undefined = undefined;

  constructor( private fb:FormBuilder, 
    private _router: Router, private _auth: AuthService,
    private _store: SessionStore, private _query: SessionQuery,
    private _storage: LocalStorageService) { }

  createForm() {
    this.signinForm = this.fb.group({
      email: [undefined, [Validators.required]],
      password: [undefined, [Validators.required]]
    })
  }

  submitData(){
    of(this.signinForm.valid).pipe(
      tap( () => this.setButtonStatus(true)),
      switchMap( () => this._auth.login(this.signinForm.value)),
      tap( (apiResponse: ApiResponse<User>) => {
        this.setButtonStatus(false);
        this.updateUserDetails(apiResponse.data);
        /* let url = apiResponse.data.roleType === "1" ? 'buyer-dashboard' : 'seller-dashboard'; */
        let url;
        if(apiResponse.data.roleType === "1"){
          url = 'buyer-dashboard'
        }else if(apiResponse.data.roleType === "2") {
          url = 'seller-dashboard'
        }else {
          url = 'take-off'
        }
        this._router.navigateByUrl(url);
      }),
      catchError( err => {
        this.setButtonStatus(false);
        this.parseError(err);
        throw err;
      }),
      takeUntil(this.unSubscribe$)
    ).subscribe();
  }
  

  ngOnInit(): void {
    this.createForm();
  }

  get email(){
    return this.signinForm.get('email');
  }

  get password(){
    return this.signinForm.get('password');
  }

  updateUserDetails( user: User) {
    this._store.update( state => ({
      ...state,
      user: user,
      role: user.roleType,
      isLoggedIn: true
    }));
    this._storage.setSession( user );
  }

  setButtonStatus( status: boolean){
    this.loading = status;
    this.disabled = status;
  }

  parseError(err: HttpErrorResponse) {
    this.apiError = err.error.message;
  }

  clearError() {
    this.apiError = undefined;
  }

  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }

}
