import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { UserLogin } from '../shared/models/auth/user-login.model';
import { User } from '../shared/models/auth/user.model';
import { AuthService } from '../shared/services/api/auth.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private _appService: AuthService) { }
  user$: Observable<User> = of({} as User);
  _user: User = {} as User

  ngOnInit(): void {
    this.user$ = this._appService.getUserFromLocalStorage();
    this._user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')!) : null;
  }

}
