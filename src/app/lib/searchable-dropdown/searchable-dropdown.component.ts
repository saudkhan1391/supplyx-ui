import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-searchable-dropdown',
  templateUrl: './searchable-dropdown.component.html',
  styleUrls: ['./searchable-dropdown.component.scss']
})
export class SearchableDropdownComponent implements OnInit {

  getAllProductsData: any[] = [];
  // @Input setProductsData set(data: any[]) {
  //   getAllProductsData = data;
  // }

  constructor() { }

  ngOnInit(): void {
    console.log("getAllProductsData :", this.getAllProductsData);
  }

  selectedCar: any = 0;

  cars = [
    { id: 1, name: 'Volvo' },
    { id: 2, name: 'Saab' },
    { id: 3, name: 'Opel' },
    { id: 4, name: 'Audi' },
  ];

}
