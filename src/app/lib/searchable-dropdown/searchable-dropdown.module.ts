import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { SearchableDropdownComponent } from './searchable-dropdown.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [SearchableDropdownComponent],
  imports: [
    CommonModule,
    NgSelectModule,
    FormsModule,
    RouterModule,
  ],
  exports: [ SearchableDropdownComponent,NgSelectModule,FormsModule]
})
export class SearchableDropdownModule { }
