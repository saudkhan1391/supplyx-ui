import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PdfThumbnailService } from 'src/app/shared/services/pdf/pdf-thumbnail.service';
import { SessionQuery } from 'src/app/state/session.query';

export type ATTACHMENT_TYPE = 'IMG' | 'VIDEO' | 'PDF' | 'EXCEL'

@Component({
  selector: 'app-document-preview',
  templateUrl: './document-preview.component.html',
  styleUrls: ['./document-preview.component.scss']
})
export class DocumentPreviewComponent implements OnInit {

  _src!: string;
  _pdfPreview: any;
  type: ATTACHMENT_TYPE = 'IMG';
  url: string = "";
  @Output() removeDrawing: EventEmitter<any> = new EventEmitter();
  @Output() removeImage: EventEmitter<any> = new EventEmitter();
  @Input() role!: string  | null;

  @Input() set spec(spec: any) {
    this.spec = spec;
  }
  @Input() set src(src: string) {
    this._src = src;
    this.url = src;
  }
  constructor(private pdfThumbnailService: PdfThumbnailService,
    private _query: SessionQuery)
   { }

  ngOnInit(): void {
    this.checkType(this._src);
  }


  onRemoveImage() {
    this.removeImage.emit(this.url);
  }

  onRemoveDrawing() {
    this.removeDrawing.emit(this.spec)
  }

  downloadMyFile() {
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', this.url);
    link.setAttribute('download', "file");
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  checkType(url: string): string {
    let attachmentType: string = url.substring(url.lastIndexOf(".") + 1, url.length);
    switch (attachmentType) {
      case 'mp4':
        this.type = 'VIDEO';
        break;
      case 'jpg':
        this.type = 'IMG';
        break;
      case 'xlsx':
        this.type = 'EXCEL';
        break;
      case 'pdf':
        this.type = 'PDF';
        this.pdfThumbnailService.pdfToImageDataURLAsync(this._src).then(res => {
          this._pdfPreview = res;
          // console.log(res, "this is console log");
        });
    }
    return attachmentType;
  }
}
