import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentPreviewComponent } from './document-preview.component';



@NgModule({
  declarations: [
    DocumentPreviewComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DocumentPreviewComponent
  ]
})
export class DocumentPreviewModule { }
