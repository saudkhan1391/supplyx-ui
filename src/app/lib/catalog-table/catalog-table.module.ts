import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogTableRoutingModule } from './catalog-table-routing.module';
import { CatalogTableComponent } from './catalog-table.component';
import { FormsModule } from '@angular/forms';
import { SpinnerColoredModule } from '../spinner-colored/spinner-colored.module';


@NgModule({
  declarations: [CatalogTableComponent],
  imports: [
    CommonModule,
    CatalogTableRoutingModule,
    SpinnerColoredModule,
    FormsModule
  ],
  exports: [
    CatalogTableComponent
  ]
})
export class CatalogTableModule { }
