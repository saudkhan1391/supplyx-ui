import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PaginationResponse, PaginatorPlugin } from '@datorama/akita';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Catalog } from 'src/app/shared/models/catalog/catalog.model';

export interface DDActionModel{
  action: string,
  data: Catalog
}

@Component({
  selector: 'app-catalog-table',
  templateUrl: './catalog-table.component.html',
  styleUrls: ['./catalog-table.component.scss']
})
export class CatalogTableComponent implements OnInit {

  paginationResponse!: PaginationResponse<Catalog>;
  @Input() pagination!: Observable<PaginationResponse<Catalog>>;
  @Input() paginationRef!: PaginatorPlugin<Catalog>;

  @Output() response: EventEmitter<PaginationResponse<Catalog>> = new EventEmitter();
  @Output() pageSizeChange: EventEmitter<number> = new EventEmitter();
  @Output() dropdownAction: EventEmitter<DDActionModel> = new EventEmitter();

  //wrapper to pipe the input pagination
  pagination$!: Observable<PaginationResponse<Catalog>>;

  pageSize: number = 20;
  pageSizes: number[] = [5, 10, 20, 50]

  constructor() { }

  ngOnInit(): void {
    this.pagination.pipe(
      tap( pagination => {
        this.paginationResponse = pagination;
        this.response.emit(this.paginationResponse)
      })
    ).subscribe();
  }

  changePageSize() {
    this.pageSizeChange.emit(this.pageSize);
  }

  ddAction( action: string, data: Catalog ) {
    const actionModel: DDActionModel = {
      action: action,
      data: data
    }
    this.dropdownAction.emit(actionModel);
  }

}
