import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SpinnerColoredModule } from '../spinner-colored/spinner-colored.module';

@NgModule({
  declarations: [TableComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    SpinnerColoredModule
  ],
  exports: [
    TableComponent
  ]
 
})
export class TableModule { }
