import { PaginationResponse, PaginatorPlugin } from '@datorama/akita';
import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { Product } from 'src/app/shared/models/product/product.model';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

export interface DDActionModel{
  action: string,
  data: Product
}

export interface CheckBoxModel {
  productId: number;
  checked: boolean;
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  paginationResponse!: PaginationResponse<Product>;
  @Input() pagination!: Observable<PaginationResponse<Product>>;
  @Input() paginationRef!: PaginatorPlugin<Product>;
  @Input() status: string = '';
  @Input() isSelectable: boolean = false;
  @Input() isInsideCatalog: boolean = false;

  @Output() response: EventEmitter<PaginationResponse<Product>> = new EventEmitter();
  @Output() pageSizeChange: EventEmitter<number> = new EventEmitter();
  @Output() dropdownAction: EventEmitter<DDActionModel> = new EventEmitter();
  @Output() onSelected: EventEmitter<CheckBoxModel[]> = new EventEmitter<CheckBoxModel[]>();

  pagination$!: Observable<PaginationResponse<Product>>;
  pageSize: number = 20;
  pageSizes: number[] = [20, 50];

  productIds: CheckBoxModel[] = [];
  
  constructor() { }

  ngOnInit(): void {
   this.pagination.pipe(
      tap( pagination => {
        this.paginationResponse = pagination;
        this.productIds = pagination.data.map( product => ({
          productId: product.id,
          checked: false
        }))
        this.response.emit(this.paginationResponse)
      })
    ).subscribe();  
  }

  changePageSize() {
    this.pageSizeChange.emit(this.pageSize);
  }

  ddAction( action: string, data: Product ) {
    const actionModel: DDActionModel = {
      action: action,
      data: data
    }
    this.dropdownAction.emit(actionModel);
  }

  onChange(event: any) {
    this.onSelected.emit(this.productIds);
  }

}

