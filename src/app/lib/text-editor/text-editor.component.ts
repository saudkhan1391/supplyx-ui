import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { EditorChangeContent, EditorChangeSelection } from 'ngx-quill';

@Component({
  selector: 'app-text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi:true,
      useExisting: TextEditorComponent
    }
  ]
})
export class TextEditorComponent implements OnInit, ControlValueAccessor  {

  blured = false;
  focused = false;
  touched = false;
  disabled = false;

  _text: any = '';
  @Input() set text( text: string) {
    this._text = text;
  }

  modules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],
      //['blockquote', 'code-block'],
      [{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
      [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
      //[{ 'direction': 'rtl' }],                         // text direction

      //[{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      //[{ 'header': [1, 2, 3, 4, 5, 6, false] }],

      //[{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      //[{ 'font': [] }],
      [{ 'align': [] }],

      ['clean'],                                         // remove formatting button
      //['link', 'image', 'video'],
      ['link']  
    ]
  }

  onChange = (text: any) => {};
  onTouched = () => {};

  constructor() { }

  ngOnInit(): void {
  }

  created(event: any) { }

  changedEditor(event: EditorChangeContent | EditorChangeSelection) {
    this.markAsTouched();
    if (!this.disabled) {
      this.onChange(this._text);
    }
  }

  focus($event: any) {
    this.focused = true
    this.blured = false
  }

  blur($event: any) {
    this.focused = false
    this.blured = true
  }

  writeValue(text: string): void {
    this._text = text;
  }

  registerOnChange(onChange: any) {
    this.onChange = onChange;
  }

  registerOnTouched(onTouched: any) {
    this.onTouched = onTouched;
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched();
      this.touched = true;
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }

}
