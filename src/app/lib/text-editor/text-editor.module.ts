import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextEditorComponent } from './text-editor.component';
import { QuillModule } from 'ngx-quill';
import { FormsModule } from '@angular/forms';

export const COMPS = [
  TextEditorComponent
]

@NgModule({
  declarations: [
    ...COMPS
  ],
  imports: [
    CommonModule,
    FormsModule,
    QuillModule.forRoot()
  ],
  exports: [
    ...COMPS
  ]
})
export class TextEditorModule { }
