import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NkHeaderComponent } from './nk-header.component';

describe('NkHeaderComponent', () => {
  let component: NkHeaderComponent;
  let fixture: ComponentFixture<NkHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NkHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NkHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
