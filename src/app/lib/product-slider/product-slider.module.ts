import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductSliderComponent } from './product-slider.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';

const COMPS = [ProductSliderComponent]

@NgModule({
  declarations: [...COMPS],
  imports: [
    CommonModule,
    SlickCarouselModule
  ],
  exports: [
    ...COMPS
  ]
})
export class ProductSliderModule { }
