import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { param } from 'jquery';
import { catchError, filter, switchMap, tap } from 'rxjs/operators';
import { Product } from 'src/app/shared/models/product/product.model';
import { ProductService } from 'src/app/shared/services/api/product.service';

@Component({
  selector: 'app-product-slider',
  templateUrl: './product-slider.component.html',
  styleUrls: ['./product-slider.component.scss']
})
export class ProductSliderComponent implements OnInit {

  @Input() slides: string[] = [];
  
  slideConfig = {
    "slidesToShow": 1, 
    "slidesToScroll": 1,
    "arrows": false,
    "fade": true,
    "navAsThumbnails": true
  };

  navSlidesConfig = {
    "arrows": false, 
    "slidesToShow": 5, 
    "slidesToScroll": 1, 
    "asNavFor":"#sliderFor", 
    "centerMode":true, 
    "focusOnSelect": true, 
    "responsive":[ 
      { "breakpoint": 1539, "settings":{"slidesToShow": 4}}, 
      {"breakpoint": 768,"settings":{"slidesToShow": 3}},
      {"breakpoint": 420,"settings":{"slidesToShow": 2}} 
    ]
  }

  constructor( ) { }

  ngOnInit(): void {
  }

  removeSlide() {
    this.slides.length = this.slides.length - 1;
  }
  
  slickInit(event: any) { }
  
  breakpoint(event: any) { }
  
  afterChange(event: any) { }
  
  beforeChange(event: any) { }

}
