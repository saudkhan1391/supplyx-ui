import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerColoredComponent } from './spinner-colored.component';

export const COMPS = [SpinnerColoredComponent]

@NgModule({
  declarations: [...COMPS],
  imports: [
    CommonModule
  ],
  exports: [...COMPS]
})
export class SpinnerColoredModule { }
