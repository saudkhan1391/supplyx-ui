import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpinnerColoredComponent } from './spinner-colored.component';

describe('SpinnerColoredComponent', () => {
  let component: SpinnerColoredComponent;
  let fixture: ComponentFixture<SpinnerColoredComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpinnerColoredComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpinnerColoredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
