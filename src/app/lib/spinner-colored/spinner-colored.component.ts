import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-spinner-colored',
  templateUrl: './spinner-colored.component.html',
  styleUrls: ['./spinner-colored.component.scss']
})
export class SpinnerColoredComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
