import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

export type BUTTON_TYPES = 'button' | 'submit';
export type BUTTON_COLOR = 'primary' | 'secondary' | 'success' | 'warning' | 'danger' | 'info' | 'light' | 'dark';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Output() onClick: EventEmitter<any> = new EventEmitter<any>();
  @Input() disabled: boolean = false;
  @Input() loading: boolean = false;
  @Input() isLarge: boolean = true;
  @Input() isBlock: boolean = false;
  @Input() type: BUTTON_TYPES = 'button';
  @Input() color: BUTTON_COLOR = 'primary';

  constructor() { }

  ngOnInit(): void {
  }

  btnClick() {
    this.onClick.emit();
  }

}
