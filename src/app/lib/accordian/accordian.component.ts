import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-accordian',
  templateUrl: './accordian.component.html',
  styleUrls: ['./accordian.component.scss']
})
export class AccordianComponent implements OnInit {

  constructor() { }

  toggleBuilding() {
    /* document.querySelector(".ni-forward-ios")?.classList.toggle("rotate")
    console.log("rotate") */
    const arrow = document.getElementById("B1")?.firstElementChild;
    arrow?.classList.toggle("rotate");
    if(arrow?.classList.contains("rotate")) {
      const floor = document.getElementById("F1")
      floor?.classList.add("show")
    }
    else if(!arrow?.classList.contains("rotate")) {
      const floor = document.getElementById("F1")
      floor?.classList.remove("show")
      const floorArrow = document.getElementById("F1")?.firstElementChild;
      const apartments = document.getElementById("A1")
      if(!floor?.classList.contains("show") && floorArrow?.classList.contains("rotate")) {
        floorArrow?.classList.remove("rotate")
        apartments?.classList.toggle("show")
      }
    }
  }

  toggleFloor() {
    const arrow = document.getElementById("F1")?.firstElementChild
    arrow?.classList.toggle("rotate")
    if(arrow?.classList.contains("rotate")) {
     const apartments = document.getElementById("A1")
     apartments?.classList.add("show")
    }
    else if(!arrow?.classList.contains("rotate")) {
      const apartments = document.getElementById("A1")
      apartments?.classList.remove("show")
    }
  }

  ngOnInit(): void {
  }

}
