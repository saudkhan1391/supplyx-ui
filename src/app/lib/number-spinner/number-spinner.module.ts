import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumberSpinnerComponent } from './number-spinner.component';
import { FormsModule } from '@angular/forms';

const COMPS = [NumberSpinnerComponent]

@NgModule({
  declarations: [...COMPS],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    ...COMPS
  ]
})
export class NumberSpinnerModule { }
