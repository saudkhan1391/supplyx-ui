import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime, tap } from 'rxjs/operators';

@Component({
  selector: 'app-number-spinner',
  templateUrl: './number-spinner.component.html',
  styleUrls: ['./number-spinner.component.scss']
})
export class NumberSpinnerComponent implements OnInit {

  @Input() count: number = 0;
  @Output() spinnerChange: EventEmitter<number> = new EventEmitter<number>();
  @ViewChild('input', { static: true }) inputField!: ElementRef;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    fromEvent(this.inputField.nativeElement, 'keyup').pipe(
      debounceTime(500),
      tap( () => this.spinnerChange.emit(this.count))
    ).subscribe();
  }

  spinner(direction: number) {
    if(direction === -1) {
      this.count > 0 ? this.count-- : this.count;
    } else {
      this.count++;
    }
    this.spinnerChange.emit(this.count);
  }

}
