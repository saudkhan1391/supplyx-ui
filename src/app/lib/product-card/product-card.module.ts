import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductCardComponent } from './product-card.component';
import { SpinnerColoredModule } from '../spinner-colored/spinner-colored.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [ProductCardComponent],
  imports: [
    CommonModule,
    SpinnerColoredModule,
    RouterModule,
  ],
  exports: [
    ProductCardComponent
  ]
})
export class ProductCardModule { }
