import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PaginationResponse, PaginatorPlugin } from '@datorama/akita';
import { each } from 'jquery';
import { from, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Product } from 'src/app/shared/models/product/product.model';

export interface DDActionModel{
  action: string,
  data: Product
}

export interface CheckBoxModel {
  productId: number;
  checked: boolean;
}

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  paginationResponse!: PaginationResponse<Product>;
  @Input() pagination!: Observable<PaginationResponse<Product>>;
  @Input() paginationRef!: PaginatorPlugin<Product>;
  @Input() status: string = '';
  @Input() isSelectable: boolean = false;
  @Input() isInsideCatalog: boolean = false;

  @Output() response: EventEmitter<PaginationResponse<Product>> = new EventEmitter();
  @Output() pageSizeChange: EventEmitter<number> = new EventEmitter();
  @Output() dropdownAction: EventEmitter<DDActionModel> = new EventEmitter();
  @Output() onSelected: EventEmitter<CheckBoxModel[]> = new EventEmitter<CheckBoxModel[]>();

  pagination$!: Observable<PaginationResponse<Product>>;
  productIds: CheckBoxModel[] = [];

  constructor() { }

  ngOnInit(): void {
    
    this.pagination.pipe(
      tap( pagination => {
        this.paginationResponse = pagination;
        this.productIds = pagination.data.map( product => ({
          productId: product.id,
          checked: false
        }))
        this.response.emit(this.paginationResponse)
      })
    ).subscribe();  
  }
  
}
