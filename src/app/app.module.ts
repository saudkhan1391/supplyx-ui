import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgxSpinnerModule } from "ngx-spinner";


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FullContentLayoutComponent } from './layout/full-content-layout/full-content-layout.component';
import { ContentLayoutComponent } from './layout/content-layout/content-layout.component';
import { PrimarySidebarComponent } from './sidebar/primary-sidebar/primary-sidebar.component';
import { SecondarySidebarComponent } from './sidebar/secondary-sidebar/secondary-sidebar.component';
import { HeaderComponent } from './header/header.component';
import { environment } from 'src/environments/environment';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AkitaNgRouterStoreModule } from '@datorama/akita-ng-router-store';
import { HttpRequestInterceptor } from './shared/middleware/http-request.interceptor';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { ChatComponent } from './chat/chat.component';
import { PaymentModule } from './seller/orders/for-payment/payment/payment.module';



const akitaDevTools = environment.production ? AkitaNgDevtools.forRoot() : AkitaNgDevtools.forRoot();

@NgModule({
  declarations: [
    AppComponent,
    FullContentLayoutComponent,
    ContentLayoutComponent,
    PrimarySidebarComponent,
    SecondarySidebarComponent,
    HeaderComponent,
    ChatComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    AkitaNgRouterStoreModule,
    akitaDevTools,
    PaymentModule,
    NgbModule,
    AutocompleteLibModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
