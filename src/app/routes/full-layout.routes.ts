import { UserProfileModule } from './../user-profile/user-profile.module';
import { Routes } from '@angular/router';
import { EditProfileModule } from '../buyer/buyer-profile/edit-profile/edit-profile.module';
//Route for content layout with sidebar, navbar and footer.

export const Full_ROUTES: Routes = [

    { path: 'company', loadChildren: () => import('../company/company.module').then( m => m.CompanyModule) },
    { path: 'chat', loadChildren: () => import('../chat/chat.module').then( m => m.ChatModule) },
    { path: 'projects', loadChildren: () => import('../buyer/projects/projects.module').then( m => m.ProjectsModule) },
    { path: 'buyer-dashboard', loadChildren: () => import('../buyer/buyer-dashboard/buyer-dashboard.module').then(m => m.BuyerDashboardModule)},
    { path: 'seller-dashboard', loadChildren: () => import('../seller/seller-dashboard/seller-dashboard.module').then(m => m.SellerDashboardModule)},
    { path: 'orders', loadChildren: () => import('../seller/orders/orders.module').then(m => m.OrdersModule)},
    { path: 'products', loadChildren: () => import('../seller/products/products/products.module').then(m => m.ProductsModule)},
    { path: 'company', loadChildren: () => import('../company/company.module').then( m => m.CompanyModule)},
   /*  { path: 'seller-profile', loadChildren: () => import('../seller/seller-profile/seller-profile.module').then( m => m.SellerProfileModule)}, */
    { path: 'order', loadChildren: () => import('../seller/orders/orders.module').then(m => m.OrdersModule)},
    { path: 'user-profile', loadChildren: () => import('../user-profile/user-profile.module').then( m => m.UserProfileModule)},

    // Added later
    { path: 'buyer-bid', loadChildren: () => import('../buyer/bid/bid.module').then(m => m.BidModule) },
    { path: 'for-payment', loadChildren: () => import('../seller/orders/for-payment/for-payment.module').then(m => m.ForPaymentModule) },
    { path: 'payment', loadChildren: () => import('../seller/orders/for-payment/payment/payment.module').then(m => m.PaymentModule) },
    { path: 'shipping-information', loadChildren: () => import('../seller/orders/for-payment/shipping-information/shipping-information.module').then(m => m.ShippingInformationModule) },
    { path: 'for-confirmation', loadChildren: () => import('../seller/orders/for-confirmation/for-confirmation.module').then(m => m.ForConfirmationModule) },
    { path: 'preparing-shipment', loadChildren: () => import('../seller/orders/preparing-shipment/preparing-shipment.module'). then(m => m.PreparingShipmentModule) },
    { path: 'order-transit', loadChildren: () => import('../seller/orders/order-in-transit/order-in-transit.module'). then(m => m.OrderInTransitModule) },
    { path: 'market-place', loadChildren: () => import('../buyer/market-place/market-place.module'). then(m => m.MarketPlaceModule) },
    { path: 'take-off', loadChildren: () => import('../buyer/take-off/take-off.module').then( m => m.TakeOffModule)},
    { path: 'product-cart', loadChildren: () => import('../buyer/product-cart/product-cart.module').then(m => m.ProductCartModule)},
    { path: 'payment', loadChildren: () => import('../buyer/payments/payments.module').then( m => m.PaymentsModule)},
    { path: 'order-summary-menu', loadChildren: () => import('../seller/orders/order-summary-menu/order-summary-menu.module').then(m => m.OrderSummaryMenuModule)}
]
 