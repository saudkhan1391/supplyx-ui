import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BuyerDashboardRoutingModule } from './buyer-dashboard-routing.module';
import { BuyerDashboardComponent } from './buyer-dashboard.component';
import { PROJECT_PAGINATOR_FACTORY, PROJECT_PAGINATOR_PROVIDER } from 'src/app/state/projects/project.paginator';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [BuyerDashboardComponent],
  imports: [
    CommonModule,
    BuyerDashboardRoutingModule,
    FormsModule,
    SharedModule
  ],
  providers: [
    { provide: PROJECT_PAGINATOR_PROVIDER, useFactory: PROJECT_PAGINATOR_FACTORY }
  ]
})
export class BuyerDashboardModule { }
