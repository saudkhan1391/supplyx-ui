import { Component, Inject, OnInit } from '@angular/core';
import { PaginationResponse, PaginatorPlugin } from '@datorama/akita';
import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { User } from 'src/app/shared/models/auth/user.model';
import { Product } from 'src/app/shared/models/product/product.model';
import { Project } from 'src/app/shared/models/projects/project.model';
import { ProductService } from 'src/app/shared/services/api/product.service';
import { ProjectsService } from 'src/app/shared/services/api/projects.service';
import { PROJECT_PAGINATOR_PROVIDER } from 'src/app/state/projects/project.paginator';

@Component({
  selector: 'app-buyer-dashboard',
  templateUrl: './buyer-dashboard.component.html',
  styleUrls: ['./buyer-dashboard.component.scss']
})
export class BuyerDashboardComponent implements OnInit {

  projects: Project[] = [];
  _user: User = {} as User;
  perPage: number = 20;
  pagination$: Observable<PaginationResponse<Project>> = of({} as PaginationResponse<Project>);
  buildings: any;
  totalFloors: number = 0;
  products$: Observable<Product[]> = of([]);

  selectedProject: Project | undefined = undefined;

  constructor(
    @Inject(PROJECT_PAGINATOR_PROVIDER) public projectPaginatorRef: PaginatorPlugin<Project>,
    private _projectsService: ProjectsService,
    private _product: ProductService) {
      this.products$ = this._product.getAllProductsData().pipe(
        map( apiResponse => apiResponse.data)
      );
  }

  ngOnInit(): void {
    this._user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')!) : null;
    this.getProjects();
  }

  getProjects() {
    this.pagination$ = this.projectPaginatorRef.pageChanges.pipe(
      switchMap((page) => {
        const req = () => this._projectsService.fetchProjects({
          page: page,
          take: this.perPage,
          status: 'active',
          filter: undefined
        });
        return this.projectPaginatorRef.getPage(req) as Observable<PaginationResponse<Project>>;
      })
    );

    this.pagination$.subscribe(res => {
      this.projects = res.data;
      this.selectedProject = this.projects[0];
    });

  }

}
