
import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { Route, Router } from '@angular/router';
import { PaginationResponse, PaginatorPlugin } from '@datorama/akita';
import { Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { SearchModel } from 'src/app/buyer/market-place/market-place.component';
import { CheckBoxModel, DDActionModel } from 'src/app/lib/product-card/product-card.component';
import { Cart } from 'src/app/shared/models/cart/cart.model';
import { Product } from 'src/app/shared/models/product/product.model';
import { ProductService } from 'src/app/shared/services/api/product.service';
import { BrandsService } from 'src/app/state/market-place/brands.service';
import { CART_PAGINATOR_PROVIDER } from 'src/app/state/products/cart-products.paginator';
import { CartService } from 'src/app/state/products/cart.service';
import { ProductsService } from 'src/app/state/products/product.service';

@Component({
  selector: 'app-product-cart',
  templateUrl: './product-cart.component.html',
  styleUrls: ['./product-cart.component.scss']
})
export class ProductCartComponent implements OnInit {

  user: any;
  getAllProductsData: any;
  public isMenuCollapsed = false;
  keyword = 'name';
  data: SearchModel[] = [];


  perPage: number = 20;
  activeTabIndex: number = 0;
  filter: string | undefined = undefined;


  paginationResponse!: PaginationResponse<Product>;
  @Input() pagination!: Observable<PaginationResponse<Product>>;
  @Input() paginationRef!: PaginatorPlugin<Product>;
  @Input() status: string = '';
  @Input() isSelectable: boolean = false;
  @Input() isInsideCatalog: boolean = false;

  @Output() response: EventEmitter<PaginationResponse<Product>> = new EventEmitter();
  @Output() pageSizeChange: EventEmitter<number> = new EventEmitter();
  @Output() dropdownAction: EventEmitter<DDActionModel> = new EventEmitter();
  @Output() onSelected: EventEmitter<CheckBoxModel[]> = new EventEmitter<CheckBoxModel[]>();

  pagination$!: Observable<PaginationResponse<Product>>;
  pageSize: number = 20;
  pageSizes: number[] = [20, 50];
  reservedProducts: any[] = [];
  enableWindow: boolean = false;
  enableList: boolean = true;
  deleteProductById: any[] = [];
  productsCategories: any[] = [];
  brands: any[] = [];
  selected: any[] = [];
  productsBrand = ['Kajaria', 'Sierra', 'Phillips', 'Fenestrelle', 'JOOFAN', 'eSync', 'Finesta', 'CRAFTSDEC', 'ALTON Store', 'YaeTek'];
  productspartNo = ['MF1-110333', 'FEN 84010', 'B08T1J1N4R', '\tB07PRV869V', '21dfd', '2335454', '21548', '21', 'MF-98697521313', 'MF-12345478587', 'MF1-1238123584', 'MF-198261360', '12345'];
  allCategories: any[] = [];
  productIds: CheckBoxModel[] = [];
  productCartItems: any[] = [];
  confirmOrderItems: any[] = [];
  OneItem: any[] = [];
  item: any[] = [];

  enableCard() {
    this.enableList = true;
    this.enableWindow = false;
  }

  enableTable() {
    this.enableList = false;
    this.enableWindow = true;
  }

  constructor(@Inject(CART_PAGINATOR_PROVIDER) public paginatorRef: PaginatorPlugin<Cart>,
    private _product: ProductService, private _brandsService: BrandsService,
    private _productsService: ProductsService, private _router: Router,
    private _cartService: CartService
  ) {
    this.getAllCartItems();
  }

  editCartItem() {
    this._product.editCartItems(this.productCartItems).subscribe(res => {
      console.log("res : ", res);
    })
  }

  getAllCartItems() {
    this.pagination$ = this.paginatorRef.pageChanges.pipe(
      switchMap((page) => {
        const req = () => this._product.getAllCartItems({ page: page, take: this.perPage, status: 'active', filter: this.filter });
        return this.paginatorRef.getPage(req) as Observable<PaginationResponse<Product>>;
      })
    );

    this._product.getAllCartItems({ page: 1, take: this.perPage, status: 'draft', filter: this.filter }).subscribe((data) => {
      console.log("res : ", data);
      this.productCartItems = data.data

    });
  }
  ngOnInit(): void {

    this.pagination?.pipe(
      tap(pagination => {
        this.paginationResponse = pagination;
        this.productIds = pagination.data.map(product => ({
          productId: product.id,
          checked: false
        }))
        this.response.emit(this.paginationResponse)
      })
    ).subscribe();
  }

  changePageSize() {
    this.pageSizeChange.emit(this.pageSize);
  }

  deleteProductRow(product: number) {
    this._product.deleteProductById(product).subscribe((data) => {
      this.getAllCartItems();
    }
    );
  }
  selectAllItems($event: any) {
    const isChecked = $event.target.checked;
    this.productCartItems.map((item: any) => item.selected = isChecked);
    this.confirmOrderItems = this.productCartItems.filter((item: any) => item.selected);
  }
  onChange(event: any) {
    this.onSelected.emit(this.productIds);
  }

  onChangeSearch(val: string) {
  }

  onFocused(e: any) {
  }

  onInputCleared() {
  }

  OnChangeItem($event: any, item: any, index: number) {
    const isChecked = $event.target.checked;
    const id = $event.target.value;
    item = { ...item, selected: isChecked };
    this.productCartItems[index] = item;
  }
  AddQuantity($event: any, item: any, index: number) {
    const isChecked = $event.target.checked;
    const id = $event.target.value;
    item.quantity = item.quantity += 1;
    item = { ...item, selected: isChecked };
    this.productCartItems[index] = item;
  }
  SubQuantity($event: any, item: any, index: number) {
    const isChecked = $event.target.checked;
    const id = $event.target.value;
    item.quantity = item.quantity -= 1;
    item = { ...item, selected: isChecked };
    this.productCartItems[index] = item;


  }

  checkout() {

    this.confirmOrderItems = this.productCartItems.filter((item: any) => item.selected);
    this._cartService.setConfirmOrderItems(this.confirmOrderItems);
    this._router.navigateByUrl('/orders/submit-order');
  }
}
