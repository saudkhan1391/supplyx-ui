import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductCartRoutingModule } from './product-cart-routing.module';
import { ProductCartComponent } from './product-cart.component';
import { PRODUCT_PAGINATOR_FACTORY, PRODUCT_PAGINATOR_PROVIDER } from 'src/app/state/products/product.paginator';
import { ProductChangesGuardService } from 'src/app/seller/products/products/services/product-changes.gaurd';
import { CART_PAGINATOR_FACTORY, CART_PAGINATOR_PROVIDER } from 'src/app/state/products/cart-products.paginator';


@NgModule({
  declarations: [
    ProductCartComponent
  ],
  imports: [
    CommonModule,
    ProductCartRoutingModule
  ],
  providers: [
    { provide: CART_PAGINATOR_PROVIDER, useFactory: CART_PAGINATOR_FACTORY },
    // ProductChangesGuardService
  ]
})
export class ProductCartModule { }
