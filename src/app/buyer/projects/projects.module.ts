import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddProjectModule } from './add-project/add-project.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectsComponent } from './projects.component';
import { FileUploaderModule } from 'src/app/lib/file-uploader/file-uploader.module';
import { ProjectCardsComponent } from './ui/project-cards/project-cards.component';
import { PROJECT_PAGINATOR_FACTORY, PROJECT_PAGINATOR_PROVIDER } from 'src/app/state/projects/project.paginator';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ProjectsComponent, ProjectCardsComponent],
  imports: [
    CommonModule,
    ProjectsRoutingModule,
    FileUploaderModule,
    AddProjectModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
    { provide: PROJECT_PAGINATOR_PROVIDER, useFactory: PROJECT_PAGINATOR_FACTORY }
  ]
})
export class ProjectsModule { }
