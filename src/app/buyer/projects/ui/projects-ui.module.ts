import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectCardsComponent } from './project-cards/project-cards.component';
import { SharedModule } from 'src/app/shared/shared.module';

const COMPONENTS = [
  ProjectCardsComponent
]

@NgModule({
  declarations: [
    ...COMPONENTS
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    ...COMPONENTS
  ]
})
export class ProjectsUiModule { }
