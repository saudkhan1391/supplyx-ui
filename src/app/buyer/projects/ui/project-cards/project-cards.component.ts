import { Component, Input, OnInit } from '@angular/core';
import { Project } from 'src/app/shared/models/projects/project.model';

@Component({
  selector: 'app-project-cards',
  templateUrl: './project-cards.component.html',
  styleUrls: ['./project-cards.component.scss']
})
export class ProjectCardsComponent implements OnInit {

  _project: Project = {} as Project;
  @Input() set project( project: Project) {
    this._project = project;
  }
  constructor() { }

  ngOnInit(): void {
  }

}
