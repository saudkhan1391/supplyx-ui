import { Project } from './../../../shared/models/projects/project.model';
import { ProjectAttachment, ProjectImage } from './../../../shared/models/product/image.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { UploaderResponse } from 'src/app/shared/models/files/uploader-response';
import { from, Observable, Subject } from 'rxjs';
import { ProjectsService } from 'src/app/shared/services/api/projects.service';
import { catchError, map, takeUntil, tap } from 'rxjs/operators';
import { ICountry, IState } from 'country-state-city/dist/lib/interface';
import { Country, State } from 'country-state-city';
import { ApiResponse } from 'src/app/shared/models/auth/api-response.model';
import { ActivatedRoute } from '@angular/router';

export interface ProjectType {
  icon: string;
  title: string;
  desc: string;
}
@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.scss']
})
export class AddProjectComponent implements OnInit, OnDestroy {

  unSubscribe$: Subject<void> = new Subject<void>()
  projectForm!: FormGroup;
  uploadedSpecs: ProjectImage[] = [];
  finalSpecs: ProjectImage[] = [];
  project: Project = {
    status: 'draft',
    type: 'Residential'
  } as Project;
  countries: ICountry[] = [];
  selectedCountry!: ICountry;
  selectedState!: IState;
  countryCode!: string;
  states: IState[] = [];
  loading: boolean = false;
  disabled: boolean = false;
  @Output() onDirty: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('addProject') openForm!: ElementRef<HTMLElement>;

  isProjectInfoNextClicked: boolean = true;

  activeTab: string = "PROJECT_TYPE";
  projectTypes: ProjectType[] = [
    { icon: 'Residential', title: 'Residential', desc: 'Housing and Apartments' },
    { icon: 'Commercial', title: 'Commercial', desc: 'Offices and Commercial Buildings' },
    { icon: 'Hospitality', title: 'Hospitality', desc: 'Hotels and Resorts' },
    { icon: 'Retail', title: 'Retails', desc: 'Shops and Dine' }
  ]

  constructor(
    private fb: FormBuilder,
    private _project: ProjectsService,
    private _activedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    from(Country.getAllCountries()).pipe(
      map(res => this.countries.push(res)),
      takeUntil(this.unSubscribe$)
    ).subscribe();
    this.createForm();
    this.country?.valueChanges.pipe(
      tap(res => {
        this.selectedCountry = res;
        this.countryCode = this.selectedCountry?.isoCode;
        this.getStates(this.countryCode);
      }),
      takeUntil(this.unSubscribe$)
    ).subscribe();
  }

  ngAfterViewInit() {
    this._activedRoute.params.pipe(
      tap(params => {
        this.handleModal(params.type)
      }),
      takeUntil(this.unSubscribe$)
    ).subscribe();
  }

  handleModal(param: string) {
    if (param === 'add') {
      //this.openForm.nativeElement.click();
    }
  }

  createForm() {
    this.projectForm = this.fb.group({
      id: [this.project.id],
      type: [this.project.type],
      name: ['', Validators.required],
      address: this.fb.group({
        street: ['', Validators.required],
        zipCode: ['', Validators.required],
        country: [''],
        state: [''],
        city: [''],
      }),
      image: [],
      description: [''],
      status: ['draft'],
      specifications: [],
    
    })
  }

  setProjectType(type: string) {
    this.project.type = type;
    this.projectForm.patchValue({
      type: this.project.type
    });
    this.activeTab = 'PROJECT_INFO';
  }


  loadFile(uploadedFile: UploaderResponse[]) {
    this.uploadedSpecs = uploadedFile.map(uploadedFile => ({
      key: uploadedFile.key,
      url: uploadedFile.path,
      status: 'projectDoc'
    } as ProjectAttachment));
    this.onDirty.emit(true)
    console.log(this.uploadedSpecs[this.uploadedSpecs.length - 1].url)
  }

  getFinalImages() {
    this.finalSpecs = [...this.uploadedSpecs];
    this.projectForm.patchValue({
      specifications: this.finalSpecs
    })
  }

  onRemoveImage(event: any) {
    /* this.projectToUpdate = {
      ...this._project,
      specifications: [...this._project.specifications.filter( res => res.url !== event)]
    }
    this._selectedProjectService.updateProject(this.projectToUpdate) */
    this.uploadedSpecs = this.uploadedSpecs.filter(res => res.url !== event)
    console.log(event)
  }

  submitData() {
    this.getFinalImages();
    this.getCountryState();
    if (this.projectForm.valid) {
      this.projectForm.patchValue({
        status: 'active'
      })
      this.saveProject(this.projectForm.value).subscribe();
    }
  }

  saveProject(project: Project): Observable<ApiResponse<Project>> {
    this.setButtonStatus(true);
    return this._project.addProject(project).pipe(
      tap(apiResponse => {
        this.setButtonStatus(false);
        this.project = apiResponse.data;
        this.projectForm.patchValue(apiResponse.data);
        // refresh project list in the projects section
        this._project.refreshProjectsList$.next(true);
      }),
      catchError(err => {
        this.setButtonStatus(false);
        throw err;
      }),
      takeUntil(this.unSubscribe$)
    );
  }

  getCountryState() {
    let address: Project = {
      ...this.projectForm.value,
      country: this.projectForm.value.address.country.name,
      state: this.projectForm.value.address.state.name,
    }
    this.projectForm.patchValue({
      address: address
    })
  }

  getStates(code: string) {
    if (this.states.length > 0) {
      this.states.splice(0, this.states.length)
    }
    from(State.getStatesOfCountry(code)).pipe(
      map(res => {
        this.states.push(res)
      })
    ).subscribe(res => {
      console.log('State', res)
    })
  }

  get name() {
    return this.projectForm.get('name');
  }

  get street(): any {
    return this.projectForm.get('address.street');
  }

  get zipCode() {
    return this.projectForm.get('address.zipCode')
  }

  get country() {
    return this.projectForm.get('address.country')
  }

  changeActiveTab(selectedTab: string) {
    this.activeTab = selectedTab;
    if (this.activeTab === 'PROJECT_DOCS' && this.isProjectInfoNextClicked) {
      this.saveProject(this.projectForm.value).subscribe(() => {
        this.isProjectInfoNextClicked = false;
      });
    }
  }

  setButtonStatus(status: boolean) {
    this.loading = status;
    this.disabled = status;
  }

  

  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }


}


