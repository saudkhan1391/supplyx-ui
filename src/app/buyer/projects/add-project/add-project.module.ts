import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddProjectRoutingModule } from './add-project-routing.module';
import { AddProjectComponent } from './add-project.component';
import { FileUploaderModule } from 'src/app/lib/file-uploader/file-uploader.module';
import { ButtonModule } from 'src/app/lib/button/button.module';
import { DocumentPreviewModule } from 'src/app/lib/document-preview/document-preview.module';


@NgModule({
  declarations: [AddProjectComponent],
  imports: [
    CommonModule,
    AddProjectRoutingModule,
    FileUploaderModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    DocumentPreviewModule,
  ],
  exports: [
    AddProjectComponent
  ]
})
export class AddProjectModule { }
