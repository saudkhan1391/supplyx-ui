import { ProjectsComponent } from './projects.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', component:ProjectsComponent
  },
  {
    path: 'add-project', loadChildren: () => import('../projects/add-project/add-project.module').then( m => m.AddProjectModule)
  },
  {
    path: ':projectId', loadChildren: () => import('../projects/project-description/project-description.module').then( m => m.ProjectDescriptionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsRoutingModule { }
