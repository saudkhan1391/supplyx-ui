import { FormBuilder, FormGroup } from '@angular/forms';

import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Loader, LoaderOptions } from 'google-maps';
import { Subject } from 'rxjs';
import { filter, switchMap, takeUntil, tap } from 'rxjs/operators';
import { UploaderResponse } from 'src/app/shared/models/files/uploader-response';
import { ProjectImage } from 'src/app/shared/models/product/image.model';
import { Apartment, Project } from 'src/app/shared/models/projects/project.model';
import { ProjectsService } from 'src/app/shared/services/api/projects.service';
import { SelectedProjectService } from 'src/app/state/projects/selected-project.service';
import { AddApartmentComponent } from './project-information/add-apartment/add-apartment.component';

@Component({
  selector: 'app-project-description',
  templateUrl: './project-description.component.html',
  styleUrls: ['./project-description.component.scss']
})
export class ProjectDescriptionComponent implements OnInit {

  unSubscribe$: Subject<void> = new Subject<void>();
  uploadedImage: ProjectImage | undefined = undefined;
  showApartment: boolean = false;
  showProjectInfo: boolean = true;
  project: Project = {} as Project;
  showEditApartment: boolean = false;
  enableImage: boolean = false;
  @ViewChild('addApartment') addApartment!: AddApartmentComponent

  constructor( 
    private _activatedRoute: ActivatedRoute,
    private _projectsService: ProjectsService,
    private _selectedProjectService: SelectedProjectService,
    private _fb: FormBuilder
  ) {
    _projectsService.refreshProject$.asObservable().pipe(
      filter( event =>  event),
      tap( () => this.loadProject()),
      takeUntil(this.unSubscribe$)
    ).subscribe();

    this._selectedProjectService.getSelectedProject().pipe(
      tap( ), 
    ).subscribe(res => {
      this.project = res
    });
    this.loadProject();
  }

  loadProject() {
    this._activatedRoute.params.pipe(
      filter( params => !!params.projectId),
      switchMap( params => this._projectsService.getProjectById(params.projectId)),
      tap( apiResponse => {
        this.project = apiResponse.data;
        this._selectedProjectService.updateProject(this.project);
      }),
      takeUntil(this.unSubscribe$)
    ).subscribe(res => {
      this.project = res.data
    });
  }

  enableApartment(show: boolean) {
    this.showApartment = show;
    this.showProjectInfo = false;
    this.addApartment.addApartmentForm.reset()
  }

  hideAddApartment(back: boolean) {
    this.showApartment = false;
    this.showProjectInfo = true;
  }

  ngOnInit(): void {
  }

  showEditProject(show: boolean) {
    this.showEditApartment = show
  }

  loadProjectImage(uploadedFile: UploaderResponse[]) {
    this.uploadedImage = uploadedFile  && uploadedFile.length > 0 ? ({
      key: uploadedFile[0].key,
      url: uploadedFile[0].path
    } as ProjectImage) : undefined;
    this.submitImage();
  }

  submitImage() {
    if(this.uploadedImage) {
      const projectToUpdate: Project = {
        ...this.project,
        image: {
          ...this.project.image,
          key: this.uploadedImage.key,
          url: this.uploadedImage.url
        }
      }
      delete projectToUpdate.createdAt;
      delete projectToUpdate.updatedAt;
      this._projectsService.editProject(projectToUpdate, this.project.id)
        .pipe( tap( () => this.loadProject()) ).subscribe();
    }
    /* this._projectsService.editProject()
    this._projectsService.addImageToProject({...this.projectIamgeForm.value, 
      project: this.project}).pipe( tap( () => this.loadProject())).subscribe(); */
  }

  onSaveApartment(apartment: Apartment) {
    this._projectsService.addApartmentToProject({
      ...apartment,
      size: +apartment.size, // converting froms string to number
      project: this.project
    }).pipe(
      tap( () => {
        this._projectsService.refreshProject$.next(true)
        this.hideAddApartment(true);
      }),
      takeUntil(this.unSubscribe$)
    ).subscribe();
  }

  editApartment(apartment: Apartment){
    this.enableApartment(true)
    this.addApartment.addApartmentForm.patchValue(apartment)
  }

}
