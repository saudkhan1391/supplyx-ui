import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import 'jstree';
import { Subject } from 'rxjs';
import { filter, skip, takeUntil, tap } from 'rxjs/operators';
import { Project } from 'src/app/shared/models/projects/project.model';
import { ProjectsService } from 'src/app/shared/services/api/projects.service';
import { SelectedProjectService } from 'src/app/state/projects/selected-project.service';

@Component({
  selector: 'app-buildings',
  templateUrl: './buildings.component.html',
  styleUrls: ['./buildings.component.scss']
})
export class BuildingsComponent implements OnInit {
 
  unSubscribe$: Subject<void> = new Subject<void>();
  jsTreeInstance: any;
  treeData = [
    { "text" : "Building 1", "type": "root", "children" : [
        { "text" : "Child node 1" },
        { "text" : "Child node 2" }
      ]
    },
    { "text" : "Building 2", "children" : [
        { "text" : "Child node 1" },
        { "text" : "Child node 2" }
      ]
    }
  ]
  _project!: Project;
  @Input() set project(project: Project) {
    this._project = project;
  }
  get project() {
    return this._project;
  }
  constructor( private element: ElementRef,
    private _selectedProjectService: SelectedProjectService ) { }

  toggleArrow() {
    document.querySelector(".accordion-header")?.addEventListener("click", () =>{
      document.querySelector(".ni-forward-ios")?.classList.toggle("rotatearrow")
    })
  }  

  ngOnInit(): void {
    this._selectedProjectService.getSelectedProject().pipe(
      tap((project) => {
        this.project = project;
        this.initializeJsTree();
      }),
      takeUntil(this.unSubscribe$)
    ).subscribe();
  }

  ngAfterViewInit() {
    
  }

  initializeJsTree() {
    $('.tree-container').jstree({
      "plugins": ["checkbox", "wholerow", "themes", "types"],
      "types" : {
        "default" : {
            "icon" : "fa fa-folder text-warning"
        },
        "root": {
          "icon" : "icon ni ni-chevron-right"
        }
      },
      "core": {
        "data": this.treeData
      }
    });
    this.jsTreeInstance = $('.tree-container').jstree(true);
  }

}
