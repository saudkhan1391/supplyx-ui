import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FloorBuilderRoutingModule } from './floor-builder-routing.module';
import { FloorBuilderComponent } from './floor-builder.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    FloorBuilderComponent
  ],
  imports: [
    CommonModule,
    FloorBuilderRoutingModule,
    DragDropModule,
    FormsModule
  ],
  exports: [
    FloorBuilderComponent
  ]
})
export class FloorBuilderModule { }
