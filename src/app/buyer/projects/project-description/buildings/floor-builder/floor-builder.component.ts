import { Apartment } from './../../../../../shared/models/projects/project.model';
import { CdkDragDrop, CdkDragEnter, CdkDragExit, copyArrayItem, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, Input, OnInit } from '@angular/core';
import { Project } from 'src/app/shared/models/projects/project.model';
import { ProjectsService } from 'src/app/shared/services/api/projects.service';

@Component({
  selector: 'app-floor-builder',
  templateUrl: './floor-builder.component.html',
  styleUrls: ['./floor-builder.component.scss']
})
export class FloorBuilderComponent implements OnInit {

  apartments: Apartment[] = [];
  floorApartment: Apartment[] = [];
  dragDisabled: boolean = true;
  currentIndex!: number;
  _project!: Project;
  _currentProject!: Project;
  numberOfApartmentToClone: number = 1;

  @Input() set project(project: Project) {
    this._project = project;
    this.apartments = this._project.apartmentTypes;
  }
  get project() {
    return this._project;
  }

  constructor(private _projectService: ProjectsService) { }

  focused() {
    console.log('FocusedWork')
  }

  drop(event: CdkDragDrop<Apartment[]>) {
    console.log(" Dropped => ", event);
    this.currentIndex = event.currentIndex;
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      copyArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      );
    }
  }

  deleteApartment(buildingIndex: number, floorIndex: number, apartmentIndex: number) {
     this._project.buildings[buildingIndex].floors[floorIndex].apartments.splice(apartmentIndex, 1);
    // endpoint needs to be called from here
  }

  cloneApartment(buildingIndex: number, floorIndex: number, apartmentIndex: number, apartment: Apartment) {
    let apartments: Apartment[] = Array.from({ length: this.numberOfApartmentToClone }, (_, i) => apartment);
    this.project.buildings[buildingIndex].floors[floorIndex].apartments = [
      ...this._project.buildings[buildingIndex].floors[floorIndex].apartments.splice(0, apartmentIndex),
      ...apartments,
      ...this._project.buildings[buildingIndex].floors[floorIndex].apartments.splice(apartmentIndex)
    ];
  }

  editProject(){
    // remove key createdAt from _project
    delete this._project.createdAt;
    delete this._project.updatedAt;
    this._projectService.editProject(this._project, this._project.id).subscribe(res => {
      console.log('res => ', res);
    });
  }

  ngOnInit(): void {
  }

}
