import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FloorBuilderComponent } from './floor-builder.component';

describe('FloorBuilderComponent', () => {
  let component: FloorBuilderComponent;
  let fixture: ComponentFixture<FloorBuilderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FloorBuilderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FloorBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
