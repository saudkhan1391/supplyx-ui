import { FloorBuilderModule } from './floor-builder/floor-builder.module';
import { AccordianModule } from './../../../../lib/accordian/accordian.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BuildingsRoutingModule } from './buildings-routing.module';
import { BuildingsComponent } from './buildings.component';


@NgModule({
  declarations: [BuildingsComponent],
  imports: [
    CommonModule,
    BuildingsRoutingModule,
    AccordianModule,
    FloorBuilderModule
  ],
  exports: [
    BuildingsComponent
  ]
})
export class BuildingsModule { }
