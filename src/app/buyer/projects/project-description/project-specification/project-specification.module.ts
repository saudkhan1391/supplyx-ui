import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectSpecificationRoutingModule } from './project-specification-routing.module';
import { ProjectSpecificationComponent } from './project-specification.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';


@NgModule({
  declarations: [
    ProjectSpecificationComponent
  ],
  imports: [
    CommonModule,
    ProjectSpecificationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AutocompleteLibModule
  ],
  exports: [
    ProjectSpecificationComponent
  ]
})
export class ProjectSpecificationModule { }
