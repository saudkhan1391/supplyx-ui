import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { UploaderResponse } from 'src/app/shared/models/files/uploader-response';
import { ProjectAttachment } from 'src/app/shared/models/product/image.model';
import { Project, TakeOff } from 'src/app/shared/models/projects/project.model';
import { ProjectsService } from 'src/app/shared/services/api/projects.service';
import { SelectedProjectService } from 'src/app/state/projects/selected-project.service';
import { SessionQuery } from 'src/app/state/session.query';

@Component({
  selector: 'app-project-specification',
  templateUrl: './project-specification.component.html',
  styleUrls: ['./project-specification.component.scss']
})
export class ProjectSpecificationComponent implements OnInit {


  unSubscribe$: Subject<void> = new Subject<void>();
  _project: Project = {} as Project;
  @Input() set project(project: Project) {
    this._project = project;
    this.specDocs = this._project.specifications?.filter(spec => spec.status === 'projectDoc')
  }
  specDocs: ProjectAttachment[] = [];
  projectToUpdate!: Project;
  role!: string | null;

  constructor(
    private _selectedProjectService: SelectedProjectService,
     private _projectService: ProjectsService,
     private _query: SessionQuery
     ) {

  }

  ngOnInit(): void {
    this.getRole();
   }

  getRole(){
    this._query.role$.subscribe(
      res => {
        this.role = res
      }
    )
    console.log('role', this.role);
  }

  onRemoveImage(event: any) {
    console.log(event);
    this.projectToUpdate = {
      ...this._project,
      specifications: [...this._project.specifications.filter(res => res.url !== event)]
    }
    this._selectedProjectService.updateProject(this.projectToUpdate)
  }

  loadSpecifications(uploadedFiles: UploaderResponse[]) {
    this.projectToUpdate = {
      ...this._project,
      specifications: [
        ...this._project.specifications, ...uploadedFiles.map(uploadFile => ({
          key: uploadFile.key,
          url: uploadFile.path,
          status: 'projectDoc'
        } as ProjectAttachment))
      ]
    }
    console.log(this.projectToUpdate.specifications)
    this._selectedProjectService.updateProject(this.projectToUpdate)
    uploadedFiles.splice(0, uploadedFiles.length)
  }

  saveProject() {
    this._projectService.editProject(this.projectToUpdate, this._project.id).subscribe();
  }

  selectEvent(item: any) {
    // do something with selected item
  }
 
  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }
  
  onFocused(e: any){
    // do something when input is focused
  }

}
