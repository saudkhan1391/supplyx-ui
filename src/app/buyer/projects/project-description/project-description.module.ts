import { TakeoffInitiaterModule } from './takeoff-initiater/takeoff-initiater.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccordianModule } from './../../../lib/accordian/accordian.module';
import { AddApartmentModule } from './project-information/add-apartment/add-apartment.module';
import { OurProductsModule } from './our-products/our-products.module';

import { ProjectInformationModule } from './project-information/project-information.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectDescriptionRoutingModule } from './project-description-routing.module';
import { ProjectDescriptionComponent } from './project-description.component';
import { BuildingsModule } from './buildings/buildings.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProjectSpecificationComponent } from './project-specification/project-specification.component';
import { DocumentPreviewModule } from 'src/app/lib/document-preview/document-preview.module';
import { EditApartmentModule } from './project-information/edit-apartment/edit-apartment.module';
import { FileUploaderModule } from 'src/app/lib/file-uploader/file-uploader.module';


@NgModule({
  declarations: [ProjectDescriptionComponent, ProjectSpecificationComponent],
  imports: [
    CommonModule,
    ProjectDescriptionRoutingModule,
    ProjectInformationModule,
    AddApartmentModule,
    BuildingsModule,
    OurProductsModule,
    SharedModule,
    AccordianModule,
    DocumentPreviewModule,
    FormsModule,
    ReactiveFormsModule,
    EditApartmentModule,
    TakeoffInitiaterModule,
    FileUploaderModule,
  ]
})
export class ProjectDescriptionModule { }
