import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TakeoffInitiaterComponent } from './takeoff-initiater.component';

describe('TakeoffInitiaterComponent', () => {
  let component: TakeoffInitiaterComponent;
  let fixture: ComponentFixture<TakeoffInitiaterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TakeoffInitiaterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TakeoffInitiaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
