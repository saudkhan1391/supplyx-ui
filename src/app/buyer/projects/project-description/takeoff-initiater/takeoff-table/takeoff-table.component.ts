import { TakeOff } from './../../../../../shared/models/projects/project.model';
import { Component, Input, OnInit } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Project } from 'src/app/shared/models/projects/project.model';
import { ProjectsService } from 'src/app/shared/services/api/projects.service';
import { SelectedProjectService } from 'src/app/state/projects/selected-project.service';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

@Component({
  selector: 'app-takeoff-table',
  templateUrl: './takeoff-table.component.html',
  styleUrls: ['./takeoff-table.component.scss']
})
export class TakeoffTableComponent implements OnInit {

  unSubscribe$: Subject<void> = new Subject<void>();
  
  project!: Observable<Project>
  _project: Project = {} as Project;

  takeOffs: TakeOff[] = [];

  constructor(private _projectService: ProjectsService, private _selectedProjectService: SelectedProjectService) { }

  ngOnInit(): void {
   
    this.getTakeOff();
  }


  getTakeOff() {
     this._selectedProjectService.getSelectedProject().subscribe( res => {
      this._project = res;
      this.takeOffs = this._project.takeOff;
     })
  }

  downloadMyFile(url: string, name: string) {
    console.log(url, name)
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', url);
    link.setAttribute('download', name);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

}
