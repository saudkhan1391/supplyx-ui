import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TakeoffTableComponent } from './takeoff-table.component';

describe('TakeoffTableComponent', () => {
  let component: TakeoffTableComponent;
  let fixture: ComponentFixture<TakeoffTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TakeoffTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TakeoffTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
