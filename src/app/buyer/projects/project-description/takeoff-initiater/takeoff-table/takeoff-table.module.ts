import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TakeoffTableRoutingModule } from './takeoff-table-routing.module';
import { TakeoffTableComponent } from './takeoff-table.component';


@NgModule({
  declarations: [
    TakeoffTableComponent
  ],
  imports: [
    CommonModule,
    TakeoffTableRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    TakeoffTableComponent
  ]
})
export class TakeoffTableModule { }
