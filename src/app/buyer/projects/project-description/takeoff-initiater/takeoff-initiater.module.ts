import { FileUploaderModule } from 'src/app/lib/file-uploader/file-uploader.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TakeoffInitiaterRoutingModule } from './takeoff-initiater-routing.module';
import { TakeoffInitiaterComponent } from './takeoff-initiater.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TakeoffTableModule } from './takeoff-table/takeoff-table.module';
import { DocumentPreviewModule } from 'src/app/lib/document-preview/document-preview.module';
import { NgbToastModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    TakeoffInitiaterComponent
  ],
  imports: [
    CommonModule,
    TakeoffInitiaterRoutingModule,
    FileUploaderModule,
    FormsModule,
    ReactiveFormsModule,
    TakeoffTableModule,
    DocumentPreviewModule,
    NgbToastModule
  ],
  exports: [
    TakeoffInitiaterComponent
  ]

})
export class TakeoffInitiaterModule { }
