import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { UploaderResponse } from 'src/app/shared/models/files/uploader-response';
import { ProjectAttachment, ProjectVideo } from 'src/app/shared/models/product/image.model';
import { Project } from 'src/app/shared/models/projects/project.model';
import { ProjectsService } from 'src/app/shared/services/api/projects.service';
import { tap, takeUntil } from 'rxjs/operators';
import { SelectedProjectService } from 'src/app/state/projects/selected-project.service';
import { User } from 'src/app/shared/models/auth/user.model';
import { SessionQuery } from 'src/app/state/session.query';

@Component({
  selector: 'app-takeoff-initiater',
  templateUrl: './takeoff-initiater.component.html',
  styleUrls: ['./takeoff-initiater.component.scss']
})
export class TakeoffInitiaterComponent implements OnInit {


  @Output() onDirty: EventEmitter<boolean> = new EventEmitter<boolean>();
  takeOffDocs: ProjectAttachment[] = [];
  unSubscribe$: Subject<void> = new Subject<void>();
  projectToUpdate!: Project;
  takeOffStatus!: string;
  projectId!: number;
  _user: User = {} as User
  showToast:boolean = false;
  role!: string | null;

  _project: Project = {} as Project;
  @Input() set project(project: Project) {
    this._project = project;
    this.takeOffDocs = this._project.specifications?.filter(spec => spec.status === 'takeoff')
  }

  constructor(private _fb: FormBuilder, private _projectService: ProjectsService,
    private _selectedProjectService: SelectedProjectService,
    private _query: SessionQuery
    ) { }

  ngOnInit(): void {
    
  }

  loadDocument(uploadedFiles: UploaderResponse[]) {
    this.projectToUpdate = {
      ...this._project,
      specifications: [
        ...this._project.specifications, ...uploadedFiles.map(uploadedFile => ({
          key: uploadedFile.key,
          url: uploadedFile.path,
          status: 'takeoff'
        } as ProjectAttachment))
      ]
    }
    this._selectedProjectService.updateProject(this.projectToUpdate)
    uploadedFiles.splice(0, uploadedFiles.length)
  }

  onRemoveImage(event: any) {
    this.projectToUpdate = {
      ...this._project,
      specifications: [...this._project.specifications.filter( res => res.url !== event)]
    }
    this._selectedProjectService.updateProject(this.projectToUpdate)
  }


  checkImage() {
    if (!this.projectToUpdate?.specifications) {
      // alert("Image must be uploaded");
      this.showToast = true;
    }
  }

  initiateTakeOff() {
    if (this.projectToUpdate?.specifications) {
      this._projectService.editProject({
        ...this._project,
        takeOffStatus: 'INITIATED'
      }, this._project.id).pipe(
        tap((apiResponse) => this._selectedProjectService.updateProject(apiResponse.data))
      ).subscribe();
    }
    else {
      console.log("projectToUpdate image", this.projectToUpdate);
      alert("Image must be uploaded");
      return;
    }
  }

}
