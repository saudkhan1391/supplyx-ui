import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import {Loader, LoaderOptions} from 'google-maps';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { Building, Project, Apartment } from 'src/app/shared/models/projects/project.model';
import { ProjectsService } from 'src/app/shared/services/api/projects.service';
import {} from 'googlemaps';

@Component({
  selector: 'app-project-information',
  templateUrl: './project-information.component.html',
  styleUrls: ['./project-information.component.scss']
})
export class ProjectInformationComponent implements OnInit {

  unSubscribe$: Subject<void> = new Subject<void>();
  _project: Project = {} as Project;
  @Input() set project(project: Project) {
    this._project = project;
  }
  @Output() addAppartment = new EventEmitter<boolean>();
  @Output() onEdit: EventEmitter<Apartment> = new EventEmitter<Apartment>();

  @ViewChild('map') mapElement: any;
  map!: google.maps.Map;
  
  constructor(
    private _projectService: ProjectsService
  ) { }

  enableAddApartment() {
    this.addAppartment.emit(true);
  }

  addBuilding(building: Building) {
    this._projectService.addBuildingToProject({
      ...building,
      project: this._project
    }).pipe(
      tap( () => this._projectService.refreshProject$.next(true)),
      takeUntil(this.unSubscribe$)
    ).subscribe();
  }

 
  
  ngOnInit(): void {
    
  }

  ngAfterViewInit() {
    const mapProperties = {
      center: new google.maps.LatLng(35.2271, -80.8431),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
  }

  editApartment(apartment: Apartment) {
    this.onEdit.emit(apartment)
  }

}
