
import { BuildingsModule } from './../buildings/buildings.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectInformationRoutingModule } from './project-information-routing.module';
import { ProjectInformationComponent } from './project-information.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AddBuildingDialogComponent } from './add-building-dialog/add-building-dialog.component';
import { NumberSpinnerModule } from 'src/app/lib/number-spinner/number-spinner.module';
import { ReactiveFormsModule } from '@angular/forms';
import { BuildingTableComponent } from './building-table/building-table.component';
import { ApartmentTableComponent } from './apartment-table/apartment-table.component';

@NgModule({
  declarations: [
    ProjectInformationComponent, 
    AddBuildingDialogComponent, 
    BuildingTableComponent, 
    ApartmentTableComponent
  ],
  imports: [
    CommonModule,
    ProjectInformationRoutingModule,
    BuildingsModule,
    SharedModule,
    NumberSpinnerModule,
    ReactiveFormsModule
  ],
  exports: [
    ProjectInformationComponent
  ]
})
export class ProjectInformationModule { }
