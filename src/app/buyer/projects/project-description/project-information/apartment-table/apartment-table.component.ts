import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Apartment } from 'src/app/shared/models/projects/project.model';
import { ProjectsService } from 'src/app/shared/services/api/projects.service';

export interface EditModel {
  show: boolean,
  data: Apartment
}

@Component({
  selector: 'app-apartment-table',
  templateUrl: './apartment-table.component.html',
  styleUrls: ['./apartment-table.component.scss']
})
export class ApartmentTableComponent implements OnInit {

  _apartmentTypes: Apartment[] = [];
  @Input() set apartmentTypes(apartmentTypes: Apartment[]) {
    this._apartmentTypes = apartmentTypes;
  }

  @Output() onEdit: EventEmitter<Apartment> = new EventEmitter<Apartment>();

  get apartmentTypes() {
    return this._apartmentTypes;
  }
  constructor(private _router: Router,
    private _projectService: ProjectsService) { }

  editApartment(apartment: Apartment) {
    console.log('I got hit')

    this.onEdit.emit(apartment)
  }

  deleteApartmennt(id: number) {
    this._projectService.deleteApartmentInProject(id).subscribe(
      (res: any) => {
        console.log("res delete ", res);
      }
    )
  }

  ngOnInit(): void {
  }

}
