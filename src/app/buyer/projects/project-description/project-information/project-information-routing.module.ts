import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectInformationComponent } from './project-information.component';

const routes: Routes = [
  {
    path: '', component:ProjectInformationComponent
  },
  {
    path: 'add-apartment', loadChildren: () => import('../project-information/add-apartment/add-apartment.module')
      .then( m => m.AddApartmentModule)
  },
  {
    path: 'edit-apartment', loadChildren: () => import('../project-information/edit-apartment/edit-apartment.module')
      .then( m => m.EditApartmentModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectInformationRoutingModule { }
