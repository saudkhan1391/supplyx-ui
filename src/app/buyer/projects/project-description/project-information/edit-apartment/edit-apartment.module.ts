import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditApartmentRoutingModule } from './edit-apartment-routing.module';
import { EditApartmentComponent } from './edit-apartment.component';


@NgModule({
  declarations: [
    EditApartmentComponent
  ],
  imports: [
    CommonModule,
    EditApartmentRoutingModule
  ],
  exports: [
    EditApartmentComponent  
  ]
})
export class EditApartmentModule { }
