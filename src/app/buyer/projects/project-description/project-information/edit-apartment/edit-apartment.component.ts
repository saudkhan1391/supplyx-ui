import { Component, Input, OnInit } from '@angular/core';
import { Apartment } from 'src/app/shared/models/projects/project.model';

@Component({
  selector: 'app-edit-apartment',
  templateUrl: './edit-apartment.component.html',
  styleUrls: ['./edit-apartment.component.scss']
})
export class EditApartmentComponent implements OnInit {

  _apartment: Apartment = {} as Apartment;
  @Input() set apartment(apartment: Apartment) {
    this._apartment = apartment
  }

  constructor() { }

  ngOnInit(): void {
  }

}
