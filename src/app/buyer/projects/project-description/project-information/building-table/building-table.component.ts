import { Component, Input, OnInit } from '@angular/core';
import { Building } from 'src/app/shared/models/projects/project.model';

@Component({
  selector: 'app-building-table',
  templateUrl: './building-table.component.html',
  styleUrls: ['./building-table.component.scss']
})
export class BuildingTableComponent implements OnInit {

  _buildings: Building[] = [];
  @Input() set buildings( buildings: Building[]) {
    this._buildings = buildings;
  }
  
  constructor() { }

  ngOnInit(): void {
  }

}
