 import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Building, Floor } from 'src/app/shared/models/projects/project.model';

@Component({
  selector: 'app-add-building-dialog',
  templateUrl: './add-building-dialog.component.html',
  styleUrls: ['./add-building-dialog.component.scss']
})
export class AddBuildingDialogComponent implements OnInit {

  building: Building = {
    name: '',
    floorCount: 0
  } as Building;
  addBuildingForm!: FormGroup;
  @Output() save: EventEmitter<Building> = new EventEmitter<Building>();
  @ViewChild('close') closeDialog!: HTMLElement;

  constructor(
    private _fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.addBuildingForm = this._fb.group({
      name: [this.building.name],
      floorCount: [this.building.floorCount]
    });
  }

  onFloorCountChange( count: number) {
    this.addBuildingForm.patchValue({
      floorCount: count
    });
    this.building.floorCount = count;
  }

  saveBuilding() {
    const building: Building = {
      ...this.addBuildingForm.value,
      floors: this.getFloors(this.addBuildingForm.value.floorCount)
    } as Building;
    this.save.emit(building);
    document.getElementById('close-dialog')?.click();
  }

  getFloors( buildingCount: number) {
    return [...Array(+buildingCount).keys()].map( floorNumber => ({
      name: `Floor ${floorNumber + 1}`
    } as Floor));
  }

}
