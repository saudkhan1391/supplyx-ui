import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Area } from 'src/app/shared/models/projects/project.model';

@Component({
  selector: 'app-add-area-dialog',
  templateUrl: './add-area-dialog.component.html',
  styleUrls: ['./add-area-dialog.component.scss']
})
export class AddAreaDialogComponent implements OnInit {

  addAreaForm!: FormGroup;
  @Output() onAdd: EventEmitter<Area> = new EventEmitter<Area>();

  constructor( private _fb: FormBuilder) { }

  ngOnInit(): void {
    this.addAreaForm = this._fb.group({
      name: [],
      count: [0]
    });
  }

  addArea() {
    this.onAdd.emit(this.addAreaForm.value);
    document.getElementById('add-area-close')?.click();
  }

}
