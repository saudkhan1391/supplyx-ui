import { ProductImage, ProjectVideo } from './../../../../../shared/models/product/image.model';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { UploaderResponse } from 'src/app/shared/models/files/uploader-response';
import { Apartment, Area, Project } from 'src/app/shared/models/projects/project.model';
import { ProjectsService } from 'src/app/shared/services/api/projects.service';
import { tap, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-add-apartment',
  templateUrl: './add-apartment.component.html',
  styleUrls: ['./add-apartment.component.scss']
})
export class AddApartmentComponent implements OnInit {

  @Output() onBack = new EventEmitter<boolean>();
  @Output() onSave = new EventEmitter<Apartment>();
  @Output() onDirty: EventEmitter<boolean> = new EventEmitter<boolean>();
  addApartmentForm!: FormGroup;

  _project: Project = {} as Project;
  @Input() set project(project: Project) {
    this._project = project;
  }

  apartmentType: Apartment[] = [];
  finalVideo: ProjectVideo[] = []
  uploadedVideo: ProjectVideo[] = [];
  videoForm!: FormGroup;
  youtubeLink!: string;
  videoPreviewuRL!: string;
  unSubscribe$: Subject<void> = new Subject<void>();

  constructor(private _fb: FormBuilder, private projectService: ProjectsService,) { }

  ngOnInit(): void {
    this.creatForm();
    this.addApartmentForm = this._fb.group({
      name: [],
      size: [],
      areas: this.addDefaultAreas(),
      video: [],
      youtubeLink: []
    })
    console.log('AREAS',this.getAreas)
  }

  creatForm() {
    this.videoForm = this._fb.group({
      key: [],
      url: [],
    })
    console.log(this._project)
  }

  uploadVideo() {
    this.getFinalVideo();
    /* this.addVideo(this.videoForm.value) */
  }

  loadProjectVideo(uploadedFile: UploaderResponse[]) {
    this.uploadedVideo = uploadedFile.map(uploadedFile => ({
      key: uploadedFile.key,
      url: uploadedFile.path
    } as ProjectVideo));
    this.onDirty.emit(true)
  }

  getFinalVideo() {
    this.finalVideo = [...this.uploadedVideo]
    this.videoForm.patchValue({
      key: this.finalVideo[0].key,
      url: this.finalVideo[0].url
    })
    this.videoPreviewuRL = this.finalVideo[this.finalVideo.length - 1].url;
    this.addApartmentForm.patchValue({
      video: this.finalVideo,
      youtubeLink: this.youtubeLink
    })
    console.log(this.finalVideo)
   
  }

  addDefaultAreas(): FormArray {
    return this._fb.array([
      this.createArea('Bed Room', 0),
      this.createArea('Living Room', 0),
      this.createArea('Bathroom', 0),
      this.createArea('Kitchen', 0),
      this.createArea('Balcony', 0),
      this.createArea('Laundry Area', 0),
      this.createArea('Garage', 0),
      this.createArea('Stock Room', 0)
    ])
  }

  get getAreas(): FormArray {
    return <FormArray>this.addApartmentForm.controls['areas'];
  }

  createArea(name: string, count: number): FormGroup {
    return this._fb.group({
      name: name,
      count: count
    })
  }

  addArea(area: Area) {
    return this.getAreas.push(this.createArea(area.name, area.count));
  }

  backToProject() {
    this.onBack.emit(true);
  }

  saveApartmentType() {
    this.addVideo(this.videoForm.value)
    this.onSave.emit(this.addApartmentForm.value);
    console.log(this.addApartmentForm.value)
  }

  addVideo(video: ProjectVideo) {
    this.projectService.addVideoToProject(video).pipe(
      tap(() => this.projectService.refreshProject$.next(true)),
      takeUntil(this.unSubscribe$)
    ).subscribe();
  }

  setArea($event: any) {
    console.log("area data  : ", $event.target.value);
  }

}
