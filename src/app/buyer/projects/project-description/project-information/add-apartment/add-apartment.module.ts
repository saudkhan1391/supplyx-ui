import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddApartmentRoutingModule } from './add-apartment-routing.module';
import { AddApartmentComponent } from './add-apartment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'src/app/lib/button/button.module';
import { AddAreaDialogComponent } from './add-area-dialog/add-area-dialog.component';
import { FileUploaderModule } from 'src/app/lib/file-uploader/file-uploader.module';
import { DocumentPreviewModule } from 'src/app/lib/document-preview/document-preview.module';


@NgModule({
  declarations: [AddApartmentComponent, AddAreaDialogComponent],
  imports: [
    CommonModule,
    AddApartmentRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    FileUploaderModule,
    DocumentPreviewModule,
  ],
  exports: [
    AddApartmentComponent
  ]
})
export class AddApartmentModule { }
