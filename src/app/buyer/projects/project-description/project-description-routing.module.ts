
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectDescriptionComponent } from './project-description.component';

const routes: Routes = [
  {
    path: '', component: ProjectDescriptionComponent
  },
  {
    path: 'project-information', loadChildren: () => import('../project-description/project-information/project-information.module')
      .then(m => m.ProjectInformationModule)
  },
  {
    path: 'buildings', loadChildren: () => import('../project-description/buildings/buildings.module')
      .then(m => m.BuildingsModule)
  },
  {
    path: 'our-products', loadChildren: () => import('../project-description/our-products/our-products.module')
      .then( m => m.OurProductsModule)
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectDescriptionRoutingModule { }
