import { Component, OnInit } from '@angular/core';
export interface MenuItem{
  id: number,
  img: string,
  name: string,
  category: string,
  apartmentType: string,
  quantity: string
}

@Component({
  selector: 'app-our-products',
  templateUrl: './our-products.component.html',
  styleUrls: ['./our-products.component.scss']
})
export class OurProductsComponent implements OnInit {

  constructor() { }

  products: MenuItem[] = [
    { 
      id:1,
      img: '../../../../assets/Products/Rectangle 521.jpg',
      name: 'Glass Window Lorem Ipsum Dolor Sit', 
      category: 'Door / Swing Door',
      apartmentType: 'Studio / Standard 2 Bedroom',
      quantity: '100'
    },
    { 
      id: 2,
      img: '../../../../assets/Products/Rectangle 525.jpg',
      name: '10 Lite French Interior Door in Wenge Finish', 
      category: 'Door / Swing Door',
      apartmentType: 'Studio / Deluxe',
      quantity: '100'
    },
    { 
      id: 3,
      img: '../../../../assets/Products/Rectangle 528.jpg',
      name: 'Lights, E26 Base Brushed Nickel Hanging', 
      category: 'Door / Swing Door',
      apartmentType: 'Standard 2 Bedroom',
      quantity: '100'
    },
    { 
      id: 3,
      img: '../../../../assets/Products/Rectangle 528.jpg',
      name: 'Lights, E26 Base Brushed Nickel Hanging', 
      category: 'Door / Swing Door',
      apartmentType: 'Standard 2 Bedroom',
      quantity: '100'
    },
  ]

  ngOnInit(): void {
  }

}
