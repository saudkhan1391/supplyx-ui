import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OurProductsRoutingModule } from './our-products-routing.module';
import { OurProductsComponent } from './our-products.component';


@NgModule({
  declarations: [OurProductsComponent],
  imports: [
    CommonModule,
    OurProductsRoutingModule
  ],
  exports: [
    OurProductsComponent
  ]
})
export class OurProductsModule { }
