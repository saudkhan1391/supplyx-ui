import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ICountry, IState } from 'country-state-city/dist/lib/interface';
import { Country, State } from 'country-state-city';
import { from, Subject } from 'rxjs';

import { map, takeUntil, tap } from 'rxjs/operators';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {

  cardForm!: FormGroup;
  unSubscribe$: Subject<void> = new Subject<void>()
  countries: ICountry[] = [];
  selectedCountry!: ICountry;
  selectedState!: IState;
  countryCode!: string;
  states: IState[] = [];

  constructor(private fb: FormBuilder,) { }

  ngOnInit(): void {
    from(Country.getAllCountries()).pipe(
      map(res => this.countries.push(res)),
      takeUntil(this.unSubscribe$)
    ).subscribe();
    this.createForm();
    this.country?.valueChanges.pipe(
      tap(res => {
        this.selectedCountry = res;
        this.countryCode = this.selectedCountry?.isoCode;
        this.getStates(this.countryCode);
      }),
      takeUntil(this.unSubscribe$)
    ).subscribe();
  }

  createForm() {
    this. cardForm = this.fb.group({
      country: [''],
      state: ['']
    })
  }

  get country() {
    return this.cardForm.get('country')
  }

  getStates(code: string) {
    if (this.states.length > 0) {
      this.states.splice(0, this.states.length)
    }
    from(State.getStatesOfCountry(code)).pipe(
      map(res => {
        this.states.push(res)
      })
    ).subscribe(res => {
      console.log('State', res)
    })
  }

}
