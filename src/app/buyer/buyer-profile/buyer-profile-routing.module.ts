import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BuyerProfileComponent } from './buyer-profile.component';

const routes: Routes = [
  { path: 'edit-buyer-profile', loadChildren: () => import('./edit-profile/edit-profile.module').then( m => m.EditProfileModule)},
  { path:'', component:BuyerProfileComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyerProfileRoutingModule { }
