import { Component, Inject, OnInit } from '@angular/core';
import { PaginationResponse, PaginatorPlugin } from '@datorama/akita';
import { Observable, of, Subject } from 'rxjs';
import { switchMap, filter, tap, takeUntil } from 'rxjs/operators';
import { Project } from 'src/app/shared/models/projects/project.model';
import { ProjectsService } from 'src/app/shared/services/api/projects.service';
import { PROJECT_PAGINATOR_PROVIDER } from 'src/app/state/projects/project.paginator';

@Component({
  selector: 'app-take-off',
  templateUrl: './take-off.component.html',
  styleUrls: ['./take-off.component.scss']
})
export class TakeOffComponent implements OnInit {
  
  unSubscribe$: Subject<void> = new Subject<void>();
  pagination$: Observable<PaginationResponse<Project>> = of({} as PaginationResponse<Project>);
  filter: string | undefined = undefined;
  perPage: number = 20;
  
  constructor(
    @Inject(PROJECT_PAGINATOR_PROVIDER) public projectPaginatorRef: PaginatorPlugin<Project>,
    private _projectsService: ProjectsService
  ) { }

  ngOnInit(): void {
    this.pagination$ = this.projectPaginatorRef.pageChanges.pipe(
      switchMap( (page) => {
        const req = () => this._projectsService.fetchProjects({ 
          page: page, 
          take: this.perPage, 
          status: 'active', 
          filter: this.filter
        });
        return this.projectPaginatorRef.getPage(req) as Observable<PaginationResponse<Project>>;
      })
    );

    this._projectsService.refreshProjectsList$.pipe(
      filter( evt => evt),
      tap( () => this.refresh()),
      takeUntil(this.unSubscribe$)
    ).subscribe();
  }

  refresh() {
    this.projectPaginatorRef.clearCache();
    this.projectPaginatorRef.refreshCurrentPage();
  }


}
