import { TakeOffTableModule } from './take-off-table/take-off-table.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TakeOffRoutingModule } from './take-off-routing.module';
import { TakeOffComponent } from './take-off.component';


@NgModule({
  declarations: [
    TakeOffComponent,
  ],
  imports: [
    CommonModule,
    TakeOffRoutingModule,
    TakeOffTableModule
  ]
})
export class TakeOffModule { }
