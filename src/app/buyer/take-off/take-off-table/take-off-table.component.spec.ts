import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TakeOffTableComponent } from './take-off-table.component';

describe('TakeOffTableComponent', () => {
  let component: TakeOffTableComponent;
  let fixture: ComponentFixture<TakeOffTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TakeOffTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TakeOffTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
