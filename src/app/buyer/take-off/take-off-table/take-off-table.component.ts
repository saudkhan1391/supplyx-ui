import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { PaginationResponse, PaginatorPlugin } from '@datorama/akita';
import { Subject, Observable, of } from 'rxjs';
import { filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { UploaderResponse } from 'src/app/shared/models/files/uploader-response';
import { Project, TakeOff } from 'src/app/shared/models/projects/project.model';
import { ProjectsService } from 'src/app/shared/services/api/projects.service';
import { PROJECT_PAGINATOR_PROVIDER } from 'src/app/state/projects/project.paginator';

@Component({
  selector: 'app-take-off-table',
  templateUrl: './take-off-table.component.html',
  styleUrls: ['./take-off-table.component.scss']
})
export class TakeOffTableComponent implements OnInit {

  project: Project = {} as Project;
  currentProject: Project | undefined = undefined;
  projectId!: number;
  uploadedTakeOffs: TakeOff[] = [];
  @Output() onDirty: EventEmitter<boolean> = new EventEmitter<boolean>();
  unSubscribe$: Subject<void> = new Subject<void>();
  pagination$: Observable<PaginationResponse<Project>> = of({} as PaginationResponse<Project>);
  takeoffProjects: Project[] = [];
  filter: string | undefined = undefined;
  perPage: number = 20;
  takeOffForm!: FormGroup;
  takeOffName: string = "";
  // _user: User = {} as User

  constructor(
    @Inject(PROJECT_PAGINATOR_PROVIDER) public projectPaginatorRef: PaginatorPlugin<Project>,
    private _projectsService: ProjectsService, private fb: FormBuilder, private _project: ProjectsService,
  ) { }

  ngOnInit(): void {
    this.createForm();
    this.pagination$ = this.projectPaginatorRef.pageChanges.pipe(
      switchMap( (page) => {
        const req = () => this._projectsService.fetchTakeOffProjects({
          page: page,
          take: this.perPage,
          status: 'active',
          filter: this.filter
        });
        return this.projectPaginatorRef.getPage(req) as Observable<PaginationResponse<Project>>;
      }),
      map( pagination => ({
        ...pagination,
        data: pagination.data.filter( project => project.takeOffStatus === 'INITIATED')
      }))
    );

    this._projectsService.refreshProjectsList$.pipe(
      filter(evt => evt),
      tap(() => this.refresh()),
      takeUntil(this.unSubscribe$)
    ).subscribe();

  }

  createForm() {
    this.takeOffForm = this.fb.group({
      status: [],
      key: [],
      url: [],
      name: []
    })
  }

  setProject(project: Project) {
    this.currentProject = project;
  }

  submitData() {
    if(this.currentProject) {

    }
  }

  uploadTakeOffDocuments(uploadedFiles: UploaderResponse[]) {
    this.uploadedTakeOffs = uploadedFiles.map( uploadedFile => ({
      status: 'intiated',
      key: uploadedFile.key,
      url: uploadedFile.path
    } as TakeOff));
  }

  downloadMyFile(url: string, name: string) {
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', url);
    link.setAttribute('download', name);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  downloadDrawing(url: string) {
    console.log(url)
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', url);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  refresh() {
    this.projectPaginatorRef.clearCache();
    this.projectPaginatorRef.refreshCurrentPage();
    this._projectsService.refreshProject$.next(true)
  }

  saveTakeOff() {
    /* this._projectsService.updateProjectTakeOffStatus(projectId,'takeOffCompleted').pipe(
      tap( () => this._projectsService.refreshProject$.next(true)),
      takeUntil(this.unSubscribe$)
    ).subscribe(); */
    const projectToUpdate: Project = {
      ...this.currentProject!,
      takeOff: [
          ...this.currentProject!.takeOff, ...this.uploadedTakeOffs
        ]
    }
    this._projectsService.editProject(projectToUpdate, this.currentProject!.id).pipe(
      tap( () => {
        this.uploadedTakeOffs = [];
        this.refresh();
      })
    ).subscribe();
  }

  closeTakeOff(project: Project) {
    this._projectsService.editProject({
      ...project,
      takeOffStatus: 'COMPLETED'
    }, project.id).pipe(
      tap( () => this.refresh())
    ).subscribe();
  }

}
