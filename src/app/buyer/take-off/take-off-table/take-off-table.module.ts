import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TakeOffTableComponent } from './take-off-table.component';
import { PROJECT_PAGINATOR_FACTORY, PROJECT_PAGINATOR_PROVIDER } from 'src/app/state/projects/project.paginator';
import { FileUploaderModule } from 'src/app/lib/file-uploader/file-uploader.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [
    TakeOffTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    FileUploaderModule,
    ReactiveFormsModule,
    NgbModule
  ],
  exports: [
    TakeOffTableComponent,
  ],
  providers: [
    { provide: PROJECT_PAGINATOR_PROVIDER, useFactory: PROJECT_PAGINATOR_FACTORY }
  ]
})
export class TakeOffTableModule { }
