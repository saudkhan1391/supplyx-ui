import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarketPlaceRoutingModule } from './market-place-routing.module';
import { MarketPlaceComponent } from './market-place.component';
import { NgbModule, NgbToastModule } from '@ng-bootstrap/ng-bootstrap';
import { SearchableDropdownModule } from 'src/app/lib/searchable-dropdown/searchable-dropdown.module';
import { FormsModule } from '@angular/forms';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
// import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [
    MarketPlaceComponent
  ],
  imports: [
    CommonModule,
    MarketPlaceRoutingModule,
    NgbModule,
    SearchableDropdownModule,
    AutocompleteLibModule,
    FormsModule,
    NgbToastModule
  ]
})
export class MarketPlaceModule { }
