
import { Component, OnDestroy, OnInit } from '@angular/core';
import { from, iif, Observable, of, Subject } from 'rxjs';
import { filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { User } from 'src/app/shared/models/auth/user.model';
import { SessionQuery } from 'src/app/state/session.query';
import { createPopper } from '@popperjs/core';


export interface MenuItem{
  title: string,
  icon: string,
  url: string,
  disabled?: boolean
}

@Component({
  selector: 'app-primary-sidebar',
  templateUrl: './primary-sidebar.component.html',
  styleUrls: ['./primary-sidebar.component.scss']
})
export class PrimarySidebarComponent implements OnInit, OnDestroy {

  user$: Observable<User | null> = of({} as User);

  unSubscribe$: Subject<void> = new Subject<void>();
  primaryMenu$: Observable<MenuItem[]> = of([]);
  activeLinkIndex: number = 0;
  dashBoard: MenuItem [] = []
  uri: MenuItem[] = []; 

  constructor(private _query: SessionQuery) {  }


  takeOffMenus: MenuItem[] = [
    { title: 'Take Off', icon: 'navigate', url: 'take-off' },
    { title: 'Profile', icon: 'user-circle', url: 'user-profile' },
    { title: 'Sign out', icon: 'signout', url: '' },
  ]

  buyerMenus: MenuItem[] = [
    { title: 'Dashboard', icon: 'dashboard', url: 'buyer-dashboard' },
    { title: 'Projects', icon: 'building', url: 'projects' },
    { title: 'Market Place', icon: 'bag', url: 'market-place'},
    { title: 'Orders', icon: 'cart', url: 'orders' },
    { title: 'Profile', icon: 'user-circle', url: 'user-profile' },
    { title: 'Sign out', icon: 'signout', url: '' },
  ]

  sellerMenus: MenuItem[] = [
    { title: 'Dashboard', icon: 'dashboard', url: 'seller-dashboard'},
    { title: 'Products', icon: 'package-fill', url: 'products' },
    { title: 'Company', icon: 'building', url: 'company/profile/about-us' },
    { title: 'Bids', icon: 'archived', url: 'buyer-bid' },
    { title: 'Profile', icon: 'user-circle', url: 'user-profile' },
    { title: 'Sign out', icon: 'signout', url: '' },
  ]

  ngOnInit(): void {
    this.primaryMenu$ = this._query.role$.pipe(
      switchMap( role => !!role ? of(role) : of(localStorage.getItem('roleType'))),
      filter( role => !!role ),
      map( (role) => 
      {
        return role === "1" ? this.buyerMenus : (role === "2") ? this.sellerMenus : this.takeOffMenus 
      }
      ),
      takeUntil(this.unSubscribe$)
    );
     this.primaryMenu$.subscribe( res => {
      this.dashBoard = res;
      this.uri = this.dashBoard.filter( res => res.title === "Dashboard")
     });
     
  }
 
 
  activateLink(index: number) {
    this.activeLinkIndex = index;
  }
    
  
  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }    

}
