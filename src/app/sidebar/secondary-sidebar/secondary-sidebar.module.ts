import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { SecondarySidebarComponent } from './secondary-sidebar.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AutocompleteLibModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class SecondarySidebarModule { }
