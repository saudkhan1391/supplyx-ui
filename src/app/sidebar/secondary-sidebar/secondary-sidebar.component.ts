import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { ActiveRouteState, RouterQuery } from '@datorama/akita-ng-router-store';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { BrandsService } from 'src/app/state/market-place/brands.service';
import { MarketPlaceComponent } from 'src/app/buyer/market-place/market-place.component';
import { ProductService } from 'src/app/shared/services/api/product.service';
import { ProductsService } from 'src/app/state/products/product.service';
import { Product } from 'src/app/shared/models/product/product.model';

export interface MenuItem {
  title: string,
  url: string,
  disabled?: boolean
}
export interface SearchModel {
  name: string,
}

@Component({
  selector: 'app-secondary-sidebar',
  templateUrl: './secondary-sidebar.component.html',
  styleUrls: ['./secondary-sidebar.component.scss']
})


export class SecondarySidebarComponent implements OnInit {

  activeSideMenu: string = '';
  activeSideMenu$: Observable<MenuItem[]> = of([]);
  currentPage: string = "";
  // username:number = ;
  selectedRam: string = "0";
  MaxPrice: string = "100";
  brands: any[] = [];
  activeLinkIndex: number = 0;

  data: SearchModel[] = [];
  keyword = 'name';
  reservedProducts: Product[] = [];
  productMenu: MenuItem[] = [
    { title: 'Your Product', url: 'products' },
    { title: 'Add Product', url: 'products/add-product' },
    { title: 'Product Catalog', url: 'products/catalog' },
    { title: 'Product Cart', url: 'product-cart' },
  ]

  companyMenus: MenuItem[] = [
    { title: 'Company Profile', url: 'company/profile/about-us' },
    { title: 'Why Choose Us', url: 'company/profile/why-choose-us' },
    { title: 'Gallery', url: 'company/profile/gallery' },
    { title: 'Contact Us', url: 'company/profile/contact-us' }
  ]

  projectMenus: MenuItem[] = [
    { title: 'Projects', url: 'projects' },
    { title: 'Add Project', url: 'projects/add-project/add' },
  ]

  dashboardMenu: MenuItem[] = [
    { title: 'Dashboards', url: 'buyer-dashboard' },
    { title: 'Projects', url: 'projects' },
    { title: 'Market Place', url: 'market-place' },
    { title: 'Product Cart', url: 'product-cart' },
    { title: 'My Orders', url: 'projects/add-project/add' },
    { title: 'Profile', url: 'projects/add-project/add' },
  ]
  orderdMenu: MenuItem[] = [
    { title: 'Product Cart', url: 'product-cart' },
    { title: 'Submit Order', url: 'orders/submit-order' },
    { title: 'Shipping Info', url: 'shipping-information' },
    { title: 'Payment', url: 'orders/for-payment/payment' },
    { title: 'Thank You', url: 'orders/thankyou-submitted' },
    { title: 'My Orders', url: 'orders' },
    { title: 'For Confirmation', url: 'orders/for-confirmation' },
    { title: 'For Payments', url: 'orders/for-payment' },
    { title: 'Preparing Shipment', url: 'orders/preparing-shipment' },
    { title: 'Order in Transit', url: 'orders/order-in-transit' },
    { title: 'Completed', url: 'orders' },
    // { title: '', url: 'orders' },
  ]

  constructor(private _routerQuery: RouterQuery,
    private _brandsService: BrandsService,
    private _product: ProductService,
    private _productsService: ProductsService,
  ) {

    this._product.getAllProductsData().subscribe(res => {
      this.reservedProducts = res.data;
    })
  }

  ngOnInit(): void {
    this.activeSideMenu$ = this._routerQuery.select('state').pipe(
      map((activatedRoute: ActiveRouteState | null) => activatedRoute ? activatedRoute.url : ''),
      map(url => {
        // console.log(" url : ", url);
        this.currentPage = url;
        this.getBrands();

        if (url.includes('products')) {
          return this.productMenu
        }
        else if (url.includes('company')) {
          return this.companyMenus
        }
        else if (url.includes('buyer-dashboard')) {
          return this.dashboardMenu
        }
        else if (url.includes('orders') || url.includes("shipping-information")) {
          return this.orderdMenu
        }
        else if (url.includes('product-cart')) {
          return this.orderdMenu
        }
        else if (url.includes('projects')) {
          return this.projectMenus
        } else {
          return []
        }
      })
    )
    /* this.createSlider() */
  }

 /*  createSlider() {
    var slider = document.getElementById('Range-Multiple');
    noUiSlider.create(slider, {
      start: [20, 80],
      connect: true,
      range: {
          'min': 0,
          'max': 100
      }
  });
  } */

  getBrands() {
    if (this.currentPage == "/market-place") {
      this._brandsService.getBrands().subscribe(res => {
        // console.log("res : ", res);
        this.brands = res;
        this.brands.forEach((item: any) => { this.data.push({ name: item }) });
      })
    }
  }

  setRam(value: any) {
    this.selectedRam = value;
    this._product.getAllProductsData().subscribe(res => {
      let products = res.data.filter(item => (Number(item.price) > Number(this.selectedRam)) && (Number(item.price) < Number(this.MaxPrice)));
      this._productsService.setProducts(products);
    })
  }

  setMaxPrice(value: any) {
    this.MaxPrice = value;
    this._product.getAllProductsData().subscribe(res => {
      let products = res.data.filter(item => (Number(item.price) > Number(this.selectedRam)) && (Number(item.price) < Number(this.MaxPrice)));
      this._productsService.setProducts(products);
    })
  }

  selectEvent(item: any) {
    let value = item.name;
    this._product.getAllProductsByKey(value).subscribe((res) => {
      this._productsService.setProducts(res.data);
    });
  }

  onChangeSearch(value: string) {
  }

  onInputCleared(value: any) {
    this._productsService.setProducts(this.reservedProducts);
  }
  activateLink(index:number){
    this.activeLinkIndex=index
  }

  onFocused(e: any) {
    console.log('onFocused', e)
  }
}
